<?php  if (!defined('BASEPATH'))
    exit('No direct script access allowed');
Class Base extends CI_Controller {
    public function __construct() {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
    }
    
    public function index() {
        $data['show_banner'] = $this->core_base->getdata('settings_banner', null, Array('id_banner','ASC'), null);
        $data['show_product'] = $this->core_base->getrelasi('relasi_produk_kategori');
        $data['show_kategori_menu'] = $this->core_base->getdata('kategori', null, Array('nm_kategori','DESC'));
        $data['menu'] = 'menu';
        $data['module'] = 'home';
        $this->load->view('template', $data);
    }   
}