<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Data Laporan Pengiriman
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?=base_url()?>backend"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li class="active">Data Laporan Pengiriman</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Data Laporan Pengiriman</h3><br/><br/>
                    <div class="pull-right">
                    <form class="form-inline" method="post" action="<?= base_url() ?>excel/getexcel/laporan_pesanan" autocomplete="off">
                        <div class="form-group">
                            <label for="exampleInputName2">Dari Tanggal</label>
                            <input type="text" name="from_date" require class="form-control datepicker"  placeholder="Dari Tanggal">
                        </div> &nbsp;&nbsp;&nbsp;
                        <div class="form-group">
                            <label for="exampleInputEmail2">Sampai Tanggal</label>
                            <input type="text" name="to_date" require class="form-control datepicker"  placeholder="Sampai Tanggal">
                        </div> &nbsp;&nbsp;&nbsp;
                    <button type="submit" class="btn btn-success pull-right">EXPORT LAPORAN</button> 
                    </form>
                        <!-- <a href="<?= base_url() ?>excel/getexcel/laporan_pesanan" class="btn btn-success pull-right">EXPORT EXCEL</a> -->
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                     <table id="example1" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th class="text-center">Tanggan SJ</th>
                                <th class="text-center">No. Pesanan</th>
                                <th class="text-center">Nama Pelanggan</th>
                                <th class="text-center">Alamat Kirim</th>
                                <th class="text-center">Kode Surat Jalan</th>
                                <th class="text-center">Nama Kurir</th>
                                <th class="text-center">Nama Produk</th>
                                <th class="text-center">Jumlah</th>
                                <th class="text-center">Harga Satuan</th>
                                <th class="text-center">Total Harga</th>
                                
                                
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            $no = 1;
                            $grand_total = 0;
                                foreach($show_data as $val) {
                                $total = $val['qty'] * $val['harga_jual'];
                                $grand_total += $total;
                            ?>
                            <tr>
                                <td>
                                    <?=$no;?>
                                </td>
                                 <td class="text-center">
                                    <?=formatDateNumerik($val['tgl_sj'])?>
                                </td>
                                <td style="text-align:center;">                        
                                    <a href="<?=base_url()?>backend/edit/user_order/<?=$val['kd_pesanan_detail']?>"><?=$val['kd_pesanan_detail']?></a>
                                </td>
                                <td class="text-center">
                                    <?=$val['nm_plg']?>
                                </td>
                                <td class="text-center">
                                   <?=$val['alamat_kirim']?>
                                </td>
                                <td class="text-center">
                                   <?=$val['kd_sj']?>
                                </td>
                                <td class="text-center">
                                   <?=$val['nama_kurir']?>
                                </td>
                                <td class="text-center">
                                    <?=$val['nm_produk']?>
                                </td>
                                <td class="text-center">
                                     <?=$val['qty']?>
                                </td>
                                <td class="text-center">
                                    Rp. <?=rupiah($val['harga_jual'])?>
                                </td>
                                <td class="text-center">
                                    Rp. <?= rupiah($total)?>
                                </td> 
                                
                                <!-- <td class="text-center">
                                <?php
                                         if($val['status_pesanan'] == 0) {
                                             $status = 'alert-danger';
                                         } else if($val['status_pesanan'] == 1) {
                                             $status = 'alert-warning';
                                         } else if($val['status_pesanan'] == 2) {
                                             $status = 'alert-info';
                                         } else if($val['status_pesanan'] == 3) {
                                            $status = 'alert-info'; 
                                         } else if($val['status_pesanan'] == 4) {
                                            $status = 'alert-success'; 
                                         } else if($val['status_pesanan'] == 5) {
                                            $status = 'alert-danger'; 
                                         }
                                    ?>
                                     <div style="border-radius:2px;" class="<?=$status?>">
                                    <?=status_pesanan($val['status_pesanan'])?>
                                    </div>
                                </td> -->
                                
                            </tr>
                            <?php $no++; } ?>
                            
                        </tbody>
                        <tfoot>
                            <tr>
                                <td class="text-right" colspan="10">
                                    <b>Grand Total</b>
                                </td>
                                <td>
                                Rp. 
                                <?=rupiah($grand_total);?>
                                </td>
                            </tr>
                            </tfoot>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
