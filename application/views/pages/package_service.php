


    <!-- Start Page Banner -->
    <div class="page-banner" style="padding:40px 0; background: url(images/slide-02-bg.jpg) center #f9f9f9;">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <h2>Package Service</h2>
            <p>We Are Professional</p>
          </div>
          <div class="col-md-6">
            <ul class="breadcrumbs">
              <li><a href="<?=base_url()?>">Home</a></li>
              <li>Package Service</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- End Page Banner -->


    <!-- Start Content -->
    <div id="content">
      <div class="container">
        <div class="page-content">

            <br/><br/>
          <?php 
          $no = 1;
            foreach($show_package_service as $val) {
          ?>  
          <div class="row" data-animation="fadeIn" data-animation-delay="0<?=$no?>">

            <div class="col-md-5">
              <div>  
              <div class="item">
                   <?php
                    if ($val['service_pict'] == "") {
                        ?>
                        <img src="<?= base_url() ?>assets/admin/img/not_available.jpg"/>
                    <?php } else { ?>
                        <img src="<?= base_url() ?>repository/package_service/<?=$val['service_pict'] ?>"/>
                    <?php } ?> 
                
              </div>
              </div>
            </div>

            <div class="col-md-7">
                 <!-- Classic Heading -->
              <h4 class="classic-title"><span><?=$val['service_name']?></span></h4>
              <div class="product-detail-description">
                <?=$val['service_desc']?>
              </div> 
              <h3>Call : (021) 977147788</h3>
            </div>

          </div>
            <br/><br/>
            <?php $no++; } ?>
        </div>
      </div>
    </div>
    <!-- End content -->
