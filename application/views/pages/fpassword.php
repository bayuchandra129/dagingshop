<br/><br/>
<section id="register">
    <div id="content">
        <div class="container">
            <div class="page-content">
                <div class="row">
                    <!-- left form -->
                    <div class="col-sm-8">
                        <form enctype="multipart/form-data" id="form-ufpassword" method="POST" class="p-form">
                            <h3>Ubah Password</h3>
                            <p>Silahkan mengisi form berikut untuk ubah password baru anda</p>
                            <hr>
                            <label for="password">Password Reset</label>
                            <input type="password" required="required" id="password_lama" name="password_lama" autocomplete="off" placeholder="Masukkan Password Lama Lupa Password">
                            <label for="password">Password Baru</label>
                            <input type="password" required="required" id="password_baru" name="password_baru" autocomplete="off" placeholder="Masukkan Password Baru">
                            <label for="password">Password Baru Konfirmasi</label>
                            <input type="password" required="required" id="konfirmasi_password" name="konfirmasi_password" autocomplete="off" placeholder="Masukkan Password Baru Konfirmasi">
                            <input type="submit" id="SubmitUFPassword" class="btn-submit daftar" value="Ubah">
                            <input type="submit" onclick="document.location='<?=base_url()?>register/index'" class="btn-submit daftar" value="Kembali">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>