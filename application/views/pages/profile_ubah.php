<br/><br/>
<section id="ubah_profile">
    <div id="content">
        <div class="container">
            <div class="page-content">
                <div class="row">
                    <div class="col-sm-12">
                        <h3 class="title">MEMBER AREA<i class="fa fa-caret-right"></i><span>PROFILE</span></h3>
                        <div class="welcome">
                            <span class="hello">Halo, <?= $this->session->userdata('fullname'); ?><i></i></span>
                        </div>
                    </div>
                    <!-- left form -->
                    <div class="col-xs-2 col-sm-2 col-md-3 ">
                        <?php require_once($sidebar_dashboard . '.php'); ?>
                    </div>
                    <div class="col-xs-10  col-sm-10 col-md-9 content-wrap">
                        <!-- Profil -->
                        <div class="profil">

                            <h3 class="profil-title">Ubah Profil Member</h3><br/>

                            <div class="profil-wrap">
                                <form action="<?= base_url() ?>dashboard/ProccessProfile_Edit/ubah_profile" method="post" enctype="multipart/form-data">    
                                    <div class="form-wrap row">
                                        <div class="col-xs-12">
                                            <div class="col-xs-5"><label>Email</label></div>
                                            <div class="col-xs-7"><input type="text" readonly="" required="" value="<?= $profile['email_plg'] ?>" class="form-control" name="user_email" autocomplete="off"></div>
                                        </div><br/>
                                        <div class="col-xs-12">
                                            <div class="col-xs-5"><label>Foto Customer</label></div>
                                            <div class="col-xs-7"><input type="file"   name="user_photo" autocomplete="off">
                                                <br/>
                                                <?php
                                                if ($profile['photo_plg'] == "") {
                                                    ?>
                                                    <a class="lightbox"  href="<?= base_url() ?>assets/admin/img/not_available.jpg" data-lightbox-gallery="gallery2">
                                                        <img class="img img-thumbnail" src="<?= base_url() ?>assets/admin/img/not_available.jpg" width="100"/>
                                                    </a>
                                                <?php } else { ?>
                                                    <a class="lightbox" title="<?= $profile['nm_plg'] ?>" href="<?= base_url() ?>repository/user_profile/<?= $profile['photo_plg'] ?>" data-lightbox-gallery="gallery2">
                                                        <img width="100" style="border:2px solid #000" src="<?= base_url() ?>repository/user_profile/<?= $profile['photo_plg'] ?>"/>
                                                    </a>
                                                <?php } ?>
                                            </div>
                                        </div><br/>
                                        <div class="col-xs-12">
                                            <div class="col-xs-5"><label>Nama Customer</label></div>
                                            <div class="col-xs-7"><input type="text" required="" value="<?= $profile['nm_plg'] ?>" class="form-control" name="user_fullname" autocomplete="off"></div>
                                        </div><br/>
                                        <div class="col-xs-12">
                                            <div class="col-xs-5"><label>No. Handphone</label></div>
                                            <div class="col-xs-7"><input type="text" required="" value="<?= $profile['tlp_plg'] ?>" class="form-control" name="user_phone" autocomplete="off"></div>
                                        </div><br/>
                                        <div class="col-xs-12">
                                            <div class="col-xs-5"><label>Alamat Lengkap</label></div>
                                            <div class="col-xs-7"><input type="text" required="" value="<?= $profile['alamat'] ?>" class="form-control" name="user_address" autocomplete="off"></div>
                                        </div><br/>
                                        <div class="col-xs-12">
                                            <div class="col-xs-5"><label>Kota</label></div>
                                            <div class="col-xs-7"><input type="text" required="" value="<?= $profile['kota'] ?>" class="form-control" name="kota" autocomplete="off"></div>
                                        </div><br/>
                                        <div class="col-xs-12">
                                            <div class="col-xs-5"><label>Kode Pos</label></div>
                                            <div class="col-xs-7"><input type="text" required="" value="<?= $profile['kode_pos'] ?>" class="form-control" name="kode_pos" autocomplete="off"></div>
                                        </div><br/>
                                        <div class="col-xs-12">
                                            <div class="col-xs-5">
                                                <a href="<?= base_url() ?>dashboard/profile" class="btn-submit pull-left btn-link-dashboard">CANCEL</a>
                                            </div>
                                            <div class="col-xs-7"><input onclick="return confirm('Apakah anda yakin ingin mengubah data Anda ?')" style="padding:6px 16px;" type="submit" value="UBAH" class="btn-submit pull-right btn-profile-ubah"></div>
                                        </div>
                                    </div>
                                </form>

                            </div>

                        </div>						
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
