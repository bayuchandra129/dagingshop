

<!Doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">
    <head>
        <!-- Basic -->
        <title>PT. GLOBAL PRATAMA WIJAYA - Toko Daging #1 di Jakarta Utara</title>

        <!-- Define Charset -->
        <meta charset="utf-8">

        <!-- Responsive Metatag -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <!-- Page Description and Author -->
        <meta name="keyword" content="daging premium, daging super, daging supreme">
        <meta name="description" content="">
        <meta name="author" content="Solusi Multimedia Integratif">
        <link type="text/css" rel="shortcut icon" href="<?= base_url() ?>assets/images/favicon.png"/>
        <!-- Bootstrap CSS  -->
        <link rel="stylesheet" href="<?= base_url() ?>assets/admin/bootstrap.3.3.5/css/bootstrap.min.css" type="text/css" media="screen">

        <!-- Font Awesome CSS -->
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/font-awesome.min.css" type="text/css" media="screen">

        <!-- Bootstrap Datepicker CSS -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/bootstrap/bootstrap-calender/css/bootstrap-datepicker3.css" media="screen">

        <!-- Slicknav -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/slicknav.css" media="screen">

        <script src="<?= base_url() ?>assets/js/maps.js" type="text/javascript"></script>

        <!-- Margo CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/style.css" media="screen">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/style2.css" media="screen">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/js/sweetalert/sweetalert.css" />

        <!-- Responsive CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/responsive.css" media="screen">

        <!-- Css3 Transitions Styles  -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/animate.css" media="screen">
        
         <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/colors/red.css" title="red" media="screen" />
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/colors/jade.css" title="jade" media="screen" />
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/colors/green.css" title="green" media="screen" />
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/colors/blue.css" title="blue" media="screen" />
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/colors/beige.css" title="beige" media="screen" />
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/colors/cyan.css" title="cyan" media="screen" />
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/colors/orange.css" title="orange" media="screen" />
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/colors/peach.css" title="peach" media="screen" />
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/colors/pink.css" title="pink" media="screen" />
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/colors/purple.css" title="purple" media="screen" />
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/colors/sky-blue.css" title="sky-blue" media="screen" />
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/colors/yellow.css" title="yellow" media="screen" />
        
        <!-- Margo JS  -->
        <script type="text/javascript" src="<?= base_url() ?>assets/js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.migrate.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/modernizrr.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/admin/bootstrap.3.3.5/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.fitvids.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/owl.carousel.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/nivo-lightbox.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.isotope.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.appear.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/count-to.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.textillate.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.lettering.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.easypiechart.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.nicescroll.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.parallax.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/mediaelement-and-player.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.slicknav.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/sweetalert/sweetalert-dev.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/sweetalert/sweetalert.min.js"></script>
        <script type="text/javascript">
            var host = '<?= base_url() ?>';
        </script>
    </head>

    <body>
        <!-- Full Body Container -->
        <div id="container">
            <!-- Start Header Section -->
            <div class="hidden-header"></div>
            <header class="clearfix">

                <!-- Start Top Bar -->
                <div class="top-bar">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-7">
                                <!-- Start Contact Info -->
                                <ul class="contact-details">
                                    <li><a href="<?=base_url()?>informasi/contact">Kontak Kami</a>
                                    </li>
                                    <li><a href="<?=base_url()?>informasi/konfirmasipembayaran">Konfirmasi Pembayaran</a>
                                    </li>
                                    <!-- <li><a href="<?=base_url()?>informasi/testimoni">Testimoni</a>
                                    </li> -->
                                </ul>
                                <!-- End Contact Info -->
                            </div>
                            <!-- .col-md-6 -->
                            <div class="col-md-5">
                                
                                <?php
                                $getsession_login = $this->session->userdata('logged_plg');
                                if (empty($getsession_login)) {
                                    ?>
                                    <ul class="contact-details pull-right">
                                        <li><a href="<?= base_url() ?>register"><i class="fa fa-map-marker"></i> Sign In</a>
                                        </li>
                                        <li><a href="<?= base_url() ?>register"><i class="fa fa-registered"></i> Register</a>
                                        </li>
                                    </ul>
                                <?php } else { ?>
                                   <ul class="contact-details pull-right">
                                        <li><a href="<?= base_url() ?>dashboard/index"><i class="fa fa-map-marker"></i> Dashboard</a>
                                        </li>
                                        <li><a href="<?= base_url() ?>register/do_logout"><i class="fa fa-map-marker"></i> Logout</a>
                                        </li>
                                    </ul>     

                                <?php } ?>
                                <!-- End Social Links -->
                            </div>
                            <!-- .col-md-6 -->
                        </div>
                        <!-- .row -->
                    </div>
                    <!-- .container -->
                </div>
                <!-- .top-bar -->
                <!-- End Top Bar -->


                <?php require_once('pages/' . $menu . '.php'); ?>

            </header>
            <!-- End Header Section -->
            <?php require_once('pages/' . $module . '.php'); ?>
            <!-- Start Footer Section -->
            <footer>
                <div class="container">
                    <div class="col-xs-6 col-sm-6 col-md-4">
                        <h3 class="title-footer">Informasi</h3>
                        <ul class="list-footer">
                            <li><a href="<?=base_url()?>informasi/contact">Kontak Kami</a></li>
                            <li><a href="<?=base_url()?>informasi/carabelanja">Cara Belanja</a></li>
                            <li><a href="<?=base_url()?>informasi/bankaccount">Bank Acoount</a></li>
                            <li><a href="<?=base_url()?>informasi/tentangkami">Tentang Kami</a></li>
                            <li><a href="<?=base_url()?>informasi/konfirmasipembayaran">Konfirmasi Pembayaran</a></li>
                            <li><a href="<?=base_url()?>informasi/faq">FAQ</a></li>
                        </ul>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-4">
                        <h3 class="title-footer">IKUT KAMI</h3>
                        <ul class="list-footer">
                            <li><a target="_blank" href="https://www.facebook.com/anyalivingfurniture/?_rdc=1&_rdr">Facebook</a></li>
                            <li><a target="_blank" href="https://www.instagram.com/anyaliving/">Instagram</a></li>
                        </ul><br/><br/>
                    </div>
                    
                </div><br/>
            </footer>
            <footer>
                <!-- Start Copyright -->
                <div class="copyright-section">
                    <center>
                        <p>&copy; <?=date('Y')?> - PT. GLOBAL PRATAMA WIJAYA - All Rights Reserved </p>
                    </center>
                    <!-- col-md-6 -->
                    <!-- .row -->
                </div>
                <!-- End Copyright -->
        </div>
    </footer>
    <!-- End Footer Section -->
</div>
<!-- End Full Body Container -->
<!-- Go To Top Link -->
<a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>

<!-- <div id="loader">
    <div class="spinner">
        <div class="dot1"></div>
        <div class="dot2"></div>
    </div>
</div> -->

<!-- ==== Bootstrap Calender ==== -->
<script type="text/javascript" src="<?= base_url() ?>assets/bootstrap/bootstrap-calender/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/ajaxfileupload.js"></script>
<!-- Jquery Validation -->
<script type="text/javascript" src="<?= base_url() ?>assets/js/custom.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/script.js"></script>

</body>
</html>
<script>

var action;
$(".number-spinner button").mousedown(function () {
    btn = $(this);
    input = btn.closest('.number-spinner').find('input');
    btn.closest('.number-spinner').find('button').prop("disabled", false);

    if (btn.attr('data-dir') == 'up') {
        action = setInterval(function(){
            if ( input.attr('max') == undefined || parseInt(input.val()) < parseInt(input.attr('max')) ) {
                input.val(parseInt(input.val())+1);
            }else{
                btn.prop("disabled", true);
                clearInterval(action);
            }
        }, 50);
    } else {
        action = setInterval(function(){
            if ( input.attr('min') == undefined || parseInt(input.val()) > parseInt(input.attr('min')) ) {
                input.val(parseInt(input.val())-1);
            }else{
                btn.prop("disabled", true);
                clearInterval(action);
            }
        }, 50);
    }
}).mouseup(function(){
    clearInterval(action);
});


var host = '<?=base_url()?>';

$(document).ready(function(){      
      var postURL = host + "dashboard/proccessRetur";
      var i=1;  
    
      $('#add').click(function(){  
           i++;  
           $('#dynamic_field').append('<tr id="row'+i+'" class="dynamic-added">'+
            '<td> <input type="text" name="product[]" placeholder="Nama Produk"  class="form-control name_list numbersOnly" required="" /></td>'+
            '<td><input type="text" name="quantity[]" placeholder="Jumlah" maxlength="3" onkeypress="return isNumber(event);" class="form-control name_list numbersOnly" required="" /></td>'+
            '<td><input type="text" name="keterangan[]" placeholder="Keterangan Retur" class="form-control name_list" required="" /> </td>'+
            // '<td></td>'+
            // '<td></td>'+
            '<td><center><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></center></td></tr>');  
      });

      $(document).on('click', '.btn_remove', function(){  
           var button_id = $(this).attr("id");   
           $('#row'+button_id+'').remove();  
      });  

      
     
      


    });  

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
</script>


