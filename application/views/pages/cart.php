
<div class="content-cart">
    <div id="container">
        <br/>
        <!-- Start Page Banner -->
        <div class="page-banner" style="padding:40px 0; background: url(images/slide-02-bg.jpg) center #f9f9f9;">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h2>Keranjang Belanja</h2>
                        <p>We Are Professional</p>
                    </div>
                    <div class="col-md-6">
                        <ul class="breadcrumbs">
                            <li><a href="<?= base_url() ?>">Home</a></li>
                            <li>Keranjang Belanja</li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div><br/><br/>
    <div class="container">
        <div class="table-responsive">
            <table class="table" id="cart">
                <?php
                if ($cart = $this->cart->contents()) {
                    ?>
                    <thead>
                        <tr>
                            <th style="width:18%">Produk</th>
                            <th style="width:20%">Harga</th>
                            <th style="width:15%">Jumlah</th>
                            <th class="text-center" style="width:22%">Subtotal</th>
                            <th style="width:10%">Opsi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        echo form_open('product/update_cart');
                        ?>
                        <?php
                        foreach ($cart as $item) {
                            echo form_hidden('cart[' . $item['id'] . '][id]', $item['id']);
                            echo form_hidden('cart[' . $item['id'] . '][rowid]', $item['rowid']);
                            echo form_hidden('cart[' . $item['id'] . '][name]', $item['name']);
                            echo form_hidden('cart[' . $item['id'] . '][price]', $item['price']);
                            echo form_hidden('cart[' . $item['id'] . '][qty]', $item['qty']);
                            ?>
                            <tr>
                                <td>
                                    <span>
                                    <img width="120" class="img-responsive" alt="..." src="<?= base_url() ?>repository/product/<?= $item['img'] ?>"><br/>
                                    </span><h4 class="pull-left"><?= $item['name'] ?></h4> 
                                </td>
                                <td>Rp. <?= $this->cart->format_number($item['price']) ?></td>
                                <td>
                                    <input type="text" value="<?= $item['qty'] ?>" id="cartquantitiy" class="input-number" name="cart[<?= $item['id'] ?>][qty]">

                                </td>
                                <td>
                                    <?= $this->cart->format_number($item['price'] * $item['qty']) ?>
                                </td>
                                <td>
                                    <a href="<?= base_url() ?>product/removecart/<?= $item['rowid'] ?>" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                    <tfoot>
                        <tr></tr>
                    </tfoot>
                <?php } ?>
            </table>
        </div>

        <div class="col-md-12" style="padding:0px;">
            <div class="col-md-12" style="clear: both">
                <div class="pull-left"><a class="btn btn-warning" href="<?= base_url() ?>"><i class="fa fa-angle-left"></i> Continue Shopping</a></td>
                </div>
                <div class="pull-right">
                    <input type="submit" class ="btn btn-danger  btn-update-cart" value="Update Cart">

                    <?php echo form_close(); ?>
                </div>
            </div><br/><br/><br/><br/>
            <div class="col-md-5">
                <!-- <form method="post" action="<?=base_url()?>product/create_ongkir">
                <div class="col-md-12">
                    <h5>Tujuan Pengiriman</h5>
                    </div><br/>
                    <div class="col-md-12">
                        <div class="form-group">
                            <select class="form-control" required="" name="ongkir">
                                <option value="">--Pilih Lokasi</option>
                            <?php 
                                foreach($ongkir as $val_ongkir) {
                            ?>
                                <option value="<?=$val_ongkir['id_ongkir']?>"><?=$val_ongkir['nama_kota']?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="text" required="" name="alamat_lengkap" placeholder="Alamat Lengkap" class="form-control"/>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <h5>Catatan</h5>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <textarea name="catatan" class="form-control"></textarea>
                        </div>
                    </div>
                </form> -->
                <!-- <br/><br/> -->
                <br/>
                <div class="col-md-12">
                    <h5>Total Belanja</h5>
                </div><br/>
                <hr/>
                <div class="col-md-12">
                    <div class="col-md-6">
                        <h5>Total</h5>
                    </div>
                    <div class="col-md-6">
                        <h5><strong>Rp. <?= $this->cart->format_number($this->cart->total()) ?></strong></h5>
                    </div> <br/>
                    <?php
                    $getsession_email = $this->session->userdata('email');
                    if (!empty($getsession_email)) {
                        ?>
                        <input type="hidden" id="ongkir_val" value="<?= $this->cart->total() ?>"/>
                    <div class="col-md-6">
                        <h5>Ongkos Kirim</h5>
                    </div>
                    <div class="col-md-6">
                        <h5 id="txt_ongkos_kirim"><strong>Rp. 0</strong></h5>
                    </div> <br/>
                    <div class="col-md-6">
                        <h5>Grand Total</h5>
                    </div>
                    <div class="col-md-6">
                        <h5 id="txt_grand_total"><strong>Rp. <?= $this->cart->format_number($this->cart->total()) ?></strong></h5>
                    </div>

                    <?php } ?>
                </div><br/>
                <hr/>
                <div class="col-md-12" style="margin-top:15px;">
                    <?php
                    $getsession_email = $this->session->userdata('email');
                    if (!empty($getsession_email)) {
                        ?>
                        <form method="post" action="<?= base_url() ?>product/save_order">
                            <div class="col-md-12">
                                <h5>Tujuan Pengiriman</h5>
                                </div><br/>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <select id="ongkos_kirim" class="form-control" required="" name="ongkos_kirim">
                                            <option value="">--Pilih Lokasi</option>
                                            <?php 
                                            foreach($ongkir as $val_ongkir) {
                                        ?>
                                            <option value="<?=$val_ongkir['id_ongkir']?>"><?=$val_ongkir['nama_kota']?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" required="" name="alamat_pengiriman" placeholder="Alamat Pengiriman" class="form-control"/>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <h5>Catatan</h5>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <textarea name="catatan" class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="12">
                                <input type="submit" onClick="return confirm('Apakah barang belanja anda kami ingin proses?')" class="btn btn-success btn-block" value="Checkout">
                                </div>
                        </form>
                        <!-- <a class="btn btn-success btn-block" href="<?= base_url() ?>product/save_order">Checkout <i class="fa fa-angle-right"></i></a> -->
                    <?php } else { ?>
                        <a class="btn btn-success btn-block" href="<?= base_url() ?>register">Checkout <i class="fa fa-angle-right"></i></a>
                    <?php } ?>
                </div>
            </div>
            <div class="col-md-6">

            </div>
        </div>
        <!-- .container -->
    </div><br/><br/>
</div>