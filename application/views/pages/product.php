 <!-- Container -->
  <div id="container">
<br/> 
    <!-- Start Page Banner -->
    <div class="page-banner" style="padding:40px 0; background: url(images/slide-02-bg.jpg) center #f9f9f9;">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <h2>Kategori Produk</h2>
            <p>We Are Professional</p>
          </div>
          <div class="col-md-6">
            <ul class="breadcrumbs">
              <li><a href="<?=base_url()?>">Home</a></li>
              <li><a href="<?=base_url()?>product/index">Kategori Produk</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- End Page Banner -->

<br/><br/>
    <!-- Start Content -->
     <div id="content">
                <div class="container">
                    <div class="page-content">
                        <!-- Start Team Members -->
                        <div class="row">
                            <!-- Start Memebr 1 -->
                            <?php 
                            $no = 2;
                                foreach($show_cproduct as $val) {
                            ?>
                            <div class="col-md-3 col-sm-6 col-xs-6">
                                <div class="team-member modern portfolio-item item">
                                     <div style="text-align: center" class="member-info">
                                       <h1><?= $val['cproduct_name'] ?></h1>
                                    </div>
                                    <!-- Memebr Photo, Name & Position -->
                                    <div class="portfolio-thumb">
                                        <?php 
                                        if($val['cproduct_pict'] == "") {
                                    ?>
                                    <a  title="<?= $val['cproduct_name'] ?>" href="<?= base_url() ?>product/all/<?= $val['id_cproduct'] ?>" >
                                        <div class="thumb-overlay"><i class="fa fa-link"></i></div>
                                        <img class="img-thumbnail img-recent-portofolio" width="480px" height="332px" alt="" src="<?= base_url() ?>assets/admin/img/not_available.jpg"/>
                                    </a>
                                    <?php } else { ?>
                                    <a title="<?= $val['cproduct_name'] ?>" href="<?= base_url() ?>product/all/<?= $val['id_cproduct'] ?>">
                                       <div class="thumb-overlay"><i class="fa fa-link"></i></div>
                                        <img class="img-responsive img-thumbnail img-recent-portofolio img-recent-cproduct"  alt="" src="<?= base_url() ?>repository/category_product/<?= $val['cproduct_pict'] ?>" />
                                    </a>
                                    <?php } ?>
                                    </div>
                                    <!-- Memebr Words -->
                                   
                                </div>
                            </div>
                            <?php $no++; } ?>
                            <!-- End Memebr 1 -->

                        </div>
                        <!-- End Team Members -->

                    </div>
                    <!-- .container -->
                </div>
                <!-- End Team Member Section -->
            </div>
    <!-- End Content -->

  </div>
  <div style="clear:both"></div><br/><br/>
  