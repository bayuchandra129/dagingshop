<br/><br/>
<div id="content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="big-title text-center">
                    <h1>Tentang Kami</h1>
                </div>
                <div style="font-size:16px;">
                <p class="text-left" style="margin-bottom:10px;font-size:16px;">
                Menjadi importir daging yang bertanggung jawab secara sosial yang memperoleh produk berkualitas unggul dari produsen pertanian berkelanjutan <br/><br/>

Didirikan pada tahun 2009, PT. Kekuatan Global Pratama Wijaya adalah menyediakan produk daging merah berkualitas unggul untuk layanan makanan
perusahaan di seluruh Indonesia.<br/><br/>

Semua produsen dipilih dengan cermat untuk terdiri dari peternakan terkemuka berlisensi dan bersertifikat dengan pengalaman yang solid.<br/><br/>

Menempatkan kepercayaan dan kepuasan pelanggan pada alas, Global Pratama Wijaya telah bekerja tanpa henti untuk memberikan<br/><br/>
kualitas dan layanan yang konsisten di depan pintu mereka.<br/>

Global Pratama Wijaya adalah cerminan dari perusahaan yang berkembang dengan semangat dan ketekunan yang luar biasa untuk berusaha menjadi yang terbaik.
                </p>
                </div>
                
            </div>
        </div>
    </div>         
</div>
