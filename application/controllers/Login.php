<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');
Class Login extends CI_Controller {

    public function __construct() {
        parent::__construct();
       
        date_default_timezone_set("Asia/Jakarta");        
    }

    
    public function index() {
        $this->load->view('login');
    }

    /* ====== PROSES ========== */

    function prosesLogin() {
        
        $email = $this->input->post('email');
        $password = sha1(md5($this->input->post('password')));

        $cek_akun = Array();
        // pemanggilan model
        $cek_akun = $this->core_admin->ceklogin($email, $password);
        
        if ($cek_akun == TRUE) {
            
            $sess_array = Array(
                'id_admin' => $cek_akun[0]['id_admin'],
                'nm_admin' => $cek_akun[0]['nm_admin'],
                'email_admin' => $cek_akun[0]['email_admin'],
                'level' => $cek_akun[0]['level'],
                'logged_in' => 'Sudah Loggin'
            );
            $session = $this->session->set_userdata($sess_array);
          
            redirect(base_url() . 'backend/panel');
        } else {
            echo 'Login Gagal';
        }
    }

    function prosesLogout() {
        $this->session->unset_userdata('id_admin');
        $this->session->unset_userdata('nm_admin');
        $this->session->unset_userdata('email_admin');
        $this->session->unset_userdata('level');
        $this->session->unset_userdata('logged_in');
        $this->session->sess_destroy();
     
        redirect(base_url() . 'login');
    }

}