<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

Class Product extends CI_Controller {

    public function __construct() {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
        
    }

    // public function index($params = null) {

    //     $data['show_contact'] = $this->core_base->getdata('settings_contact', Array('id_settings_contact' => 1));
    //     $data['params'] = $params;
    //     $data['show_cproduct'] = $this->core_base->getdata('category_product', null, Array('cproduct_name', 'ASC'), null);
    //     $data['menu'] = 'menu';
    //     $data['module'] = 'product';
    //     $this->load->view('template', $data);
    // }

    // public function index($id = null, $params = null) {
        public function index($id = null) {
            if (!empty($id)) {
                // if($params == 'product') {
                $data['params'] = $id;
                $data['show_cproduct'] = 'all';
                $data['show_product'] = $this->core_base->getrelasi('relasi_produk_kategori',$id);
                $data['show_kategori_menu'] = $this->core_base->getdata('kategori', null, Array('nm_kategori','DESC'));
                $data['show_cproduct'] = $this->core_base->getdata('kategori', Array('kd_kategori' => $id), null, null);
                $data['nm_kategori'] = $data['show_cproduct']['nm_kategori'];
                $data['kd_kategori'] = $data['show_cproduct']['kd_kategori'];
            } else {
                $search = $this->input->get('query_search');
                $data['search'] = $search;
                $data['show_cproduct'] = null;
                $data['show_product'] = $this->core_base->get_search($search);
            }
            
        
            $data['menu'] = 'menu';
            $data['module'] = 'product_all';
            $this->load->view('template', $data);
        }

    public function detail($id) {
        
        $data['show_kategori_menu'] = $this->core_base->getdata('kategori', null, Array('nm_kategori','DESC'));
        $data['show_product'] = $this->core_base->getrelasi('relasi_produk_detail', $id);
        $data['show_recommend'] = $this->core_base->getrelasi('relasi_produk_recommendasi', $data['show_product'][0]['kd_kategori']);

        $data['params'] = $id;
        $data['menu'] = 'menu';
        $data['module'] = 'product_detail';
        $this->load->view('template', $data);
    }

    public function search($id = null) {
        if (!empty($id)) {
            // if($params == 'product') {
            $data['params'] = $id;
            $data['show_cproduct'] = 'all';
            $data['show_product'] = $this->core_base->getrelasi('relasi_produk_kategori',$id);
            $data['show_kategori_menu'] = $this->core_base->getdata('kategori', null, Array('nm_kategori','DESC'));
            $data['show_cproduct'] = $this->core_base->getdata('kategori', Array('kd_kategori' => $id), null, null);
            $data['nm_kategori'] = $data['show_cproduct']['nm_kategori'];
            $data['kd_kategori'] = $data['show_cproduct']['kd_kategori'];
        } else {
            $search = $this->input->get('query_search');
            $data['search'] = $search;
             $data['show_kategori_menu'] = $this->core_base->getdata('kategori', null, Array('nm_kategori','DESC'));
            $data['show_cproduct'] = null;
            $data['show_product'] = $this->core_base->get_search($search);
        }
        

        $data['menu'] = 'menu';
        $data['module'] = 'product_all';
        $this->load->view('template', $data);
    }

    public function proccess_search() {
        
        $search = $this->input->get('s');

        redirect(base_url() . 'product_search/?query_search=' . $search);
    }

    public function cart() {
        $data['show_kategori_menu'] = $this->core_base->getdata('kategori', null, Array('nm_kategori','DESC'));
        $data['ongkir'] = $this->core_base->getdata('ongkir', null, Array('nama_kota','ASC'));
        $data['menu'] = 'menu';
        $data['module'] = 'cart';
        $this->load->view('template', $data);
    }

    /* === Proccess Add To Cart ==== */

    function addcart() {
        $insert_data = array(
            'id' => $this->input->post('id_product'),
            'name' => $this->input->post('product_name'),
            'price' => $this->input->post('product_price'),
            'qty' => $this->input->post('product_qty'),
            'img' => $this->input->post('product_pict')
        );


        $cart_insert = $this->cart->insert($insert_data);

        redirect(base_url() . 'product/cart');
    }

    function update_cart() {
        // Recieve post values,calcute them and update
        $cart_info = $_POST['cart'];
        foreach ($cart_info as $id => $cart) {
            $rowid = $cart['rowid'];
            $price = $cart['price'];
            $amount = $price * $cart['qty'];
            $qty = $cart['qty'];

            $data = array(
                'rowid' => $rowid,
                'price' => $price,
                'amount' => $amount,
                'qty' => $qty
            );

            $this->cart->update($data);
        }
        redirect(base_url() . 'product/cart');
    }

    function removecart($rowid) {
        // Check rowid value.
        if ($rowid === "all") {
            // Destroy data which store in  session.
            $this->cart->destroy();
        } else {
            // Destroy selected rowid in session.
            $data = array(
                'rowid' => $rowid,
                'qty' => 0
            );
            // Update cart data, after cancle.
            $this->cart->update($data);
        }

        // This will show cancle data in cart.
        redirect(base_url() . 'product/cart');
    }

    public function save_order() {
        $getsession_id = $this->session->userdata('id');
        if (empty($getsession_id)) {
            exit();
        }
        $no_order = 'NO-' . acakangka(5) . '-AL';
        /** === Pengiriman === **/
        $ongkos_kirim = $this->input->post('ongkos_kirim');
        $alamat_pengiriman = $this->input->post('alamat_pengiriman');
        $catatan = $this->input->post('catatan');

        $order = array(
            'kd_plg' => $getsession_id,
            'kd_pesanan' => $no_order,
            'ongkir_id' => $ongkos_kirim,
            'alamat_kirim' => $alamat_pengiriman,
            'catatan' => $catatan,
            'tgl_pesanan' => date('Y-m-d H:i:s'),
            'status_pesanan' => 1
        );

        $ord_id = $this->core_base->insertdata('pesanan', $order);

        if ($ord_id) {
            $get_id_purchase = $this->core_base->getdata('pesanan', Array('kd_pesanan' => $no_order));
            if ($cart = $this->cart->contents()):
                foreach ($cart as $item):
                    $order_detail = array(
                        'kd_pesanan' => $get_id_purchase['kd_pesanan'],
                        'kd_produk' => $item['id'],
                        'qty' => $item['qty'],
                        'harga_jual' => $item['price']
                    );

                     // Pengurangan Stok
                    $get_produk = $this->core_base->getdata('produk', Array('kd_produk'=>$item['id']));
                    
                    $stok_old = $get_produk['stok'];
                    $stok_now = $stok_old - $item['qty'];
                    $data_stok_produk = Array(
                                        'stok' => $stok_now
                                        );
                    $update_stok_produk = $this->core_base->updatedata('produk', 
                                                            $data_stok_produk,
                                                            Array('kd_produk'=> $item['id']));
 

                    // Insert product imformation with order detail, store in cart also store in database. 
                    $cust_id = $this->core_base->insertdata('detil_pesanan', $order_detail);
                    
                endforeach;
                $this->cart->destroy();
                redirect(base_url() . 'dashboard/index');
            endif;
        }
    }

}
