<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Data Testimonial
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= base_url() ?>backend"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li class="active">Data Testimonial</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Data Master Testimonial</h3><br/><br/>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th class="text-center">Kode Pesanan</th>
                                <th class="text-center">Fullname</th>
                                <th class="text-center">Isi Testimoni</th>
                                <th class="text-center">Tanggal</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            foreach ($show_data as $val) {
                                ?>
                                <tr>
                                    <td>
                                        <?= $no; ?>
                                    </td>
                                    <td style="text-align:center;">                        
                                    <a href="<?=base_url()?>backend/edit/user_order/<?=$val['kd_pesanan']?>"><?=$val['kd_pesanan']?></a>
                                </td>
                                    <td class="text-center">
                                       <?=$val['nm_plg']?>
                                    </td>
                                    <td class="text-center">
                                        <?=$val['isi_testi']?>
                                    </td>
                                    <td class="text-center">
                                        <?= formatDate($val['tgl_testi']) ?>
                                    </td>
                                </tr>
                                <?php $no++;
                            } ?>
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
