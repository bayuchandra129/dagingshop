<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Data Retur
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?=base_url()?>backend"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li class="active">Data Retur</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Data Retur</h3><br/><br/>
                </div><!-- /.box-header -->
                <div class="box-body">
                     <table id="example1" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th class="text-center">Opsi</th>
                                <th class="text-center">No. Retur</th>
                                <th class="text-center">No. Pesanan</th>
                                <th class="text-center">Foto Retur</th>
                                <th class="text-center">Tanggan Retur</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            $no = 1;
                                foreach($show_data as $val) {
                            ?>
                            <tr>
                                <td>
                                    <?=$no;?>
                                </td>
                                <td style="text-align:center;">     
                                <a href="<?=base_url()?>backend/view/retur/<?=$val['kd_retur']?>">
                                        <span title="View" aria-hidden="true" class="glyphicon glyphicon-eye-open"></span> &nbsp;&nbsp;&nbsp;
                                    </a>
                                    <a href="<?=base_url()?>pdf/getcetak/retur/<?=$val['kd_retur']?>">
                                        <span title="Cetak Retur" aria-hidden="true" class="glyphicon glyphicon-print"></span>  &nbsp;&nbsp;&nbsp;
                                    </a>
                                </td>
                                <td style="text-align:center;">                        
                                    <?=$val['kd_retur']?>
                                </td>
                                <td style="text-align:center;">                        
                                    <a href="<?=base_url()?>backend/edit/user_order/<?=$val['kd_pesanan']?>"><?=$val['kd_pesanan']?></a>
                                </td>
                                
                                <td>
                                        <?php
                                        if ($val['gambar_retur'] == "") {
                                            ?>
                                        <a class="fancybox" data-fancybox-group="gallery" href="<?= base_url() ?>assets/admin/img/not_available.jpg" >
                                            <img width="100" style="border:2px solid #000" src="<?= base_url() ?>assets/admin/img/not_available.jpg"/>
                                            </a>
                                        <?php } else { ?>
                                        <a class="fancybox" data-fancybox-group="gallery"  title="No. Pesanan : <?= $val['kd_pesanan'] ?>" href="<?= base_url() ?>repository/retur/<?= $val['gambar_retur'] ?>" >
                                            <img width="100" style="border:2px solid #000" src="<?= base_url() ?>repository/retur/<?= $val['gambar_retur'] ?>"/>
                                            </a>
                                        <?php } ?>
                                    </td>
                                 <td class="text-center">
                                    <?=formatDate($val['tgl_retur'])?>
                                </td>
                            </tr>
                            <?php $no++; } ?>
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
