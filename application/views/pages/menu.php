<!-- Start  Logo & Naviagtion  -->
<div class="navbar navbar-default navbar-top">
    <div class="container">
        <div class="navbar-header">
            <!-- Stat Toggle Nav Link For Mobiles -->
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <i class="fa fa-bars"></i>
            </button>
            <!-- End Toggle Nav Link For Mobiles -->
            <a class="navbar-brand "  href="<?=base_url()?>">
                <img alt="" width="80" style="position:relative;top:-30px;" src="<?= base_url() ?>assets/images/logo.jpeg">
            </a>
        </div>
        <div class="navbar-collapse collapse">
            <!-- Stat Search -->
            <div class="search-side">
                <a class="show-search"><i class="fa fa-search"></i></a>
                <div class="search-form">
                    <form autocomplete="off" role="search" method="get" class="searchform" action="<?=base_url()?>product/proccess_search">
                        <input type="text" value="" name="s" id="s" placeholder="Search Product...">
                    </form>
                </div>
            </div>
            <!-- End Search -->
            <!-- Start Navigation List -->
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a class="<?=($module == 'home' ? 'active' : '')?>" href="<?=base_url()?>">Home</a>
                </li>
                <?php 
                    foreach($show_kategori_menu as $val) {
                ?>
                    <li>
                        <a href="<?=base_url()?>product_category/<?=$val['kd_kategori']?>"><?=$val['nm_kategori']?></a>
                    </li>
                <?php } ?>

               
                <?php
                $getsession_login = $this->session->userdata('logged_in');
                    if($getsession_login == 1) {
                ?>
               <li>
                    <a class='<?=($module == 'contact' || $module == 'profile' || $module == 'dashboard' || $module == 'keranjang_dashboard' ? 'active' : '')?>' href="<?=base_url()?>dashboard/index"><i class="fa fa-bars"></i>&nbsp;&nbsp; Welcome, <?=$this->session->userdata('fullname')?></a>
                    <ul class="dropdown">
                       <li>
                            <a class='<?=($module == 'dashboard' ? 'active' : '')?>' href="<?=base_url()?>dashboard/index">Panel Akun</a>
                        </li>
                       <li>
                            <a  href="<?=base_url()?>register/do_logout">Sign Out</a>
                        </li>
                    </ul>
                </li>
                
                <?php }  ?>
            </ul>
            <!-- End Navigation List -->
        </div>
    </div>

    <!-- Mobile Menu Start -->
    <ul class="wpb-mobile-menu">
        <?php
                    if($getsession_login == 1) {
                ?>
                <li>
                    <a class='<?=($module == 'dashboard' ? 'active' : '')?>' href="<?=base_url()?>dashboard/index"><i class="fa fa-bars"></i>&nbsp;&nbsp; Welcome, <?=$this->session->userdata('fullname')?></a>
                </li>
                <?php } ?> 
        <?php
            $getsession_login = $this->session->userdata('logged_in');
            if(empty($getsession_login)) {
        ?>
                <li>
                    <a class="<?=($module == 'register' ? 'active' : '')?>" href="<?=base_url()?>register">Sign In</a>
                </li>
                <li>
                    <a class="<?=($module == 'register' ? 'active' : '')?>" href="<?=base_url()?>register">Register</a>
                </li>
            <?php  } ?>         
                <li>
                    <a class="<?=($module == 'home' ? 'active' : '')?>" href="<?=base_url()?>">Home</a>
                </li>
                <li>
                    <a class="<?=($module == 'product' || $module == 'product_all' || $module == 'product_detail' ? 'active' : '')?>" href="<?=base_url()?>product/index">Product</a>
                </li>
                <li>
                    <a class="<?=($module == 'package_service' ? 'active' : '')?>" href="<?=base_url()?>package_service/index">Package Service</a>
                </li>
                <li>
                    <a class='<?=($module == 'portofolio' ? 'active' : '')?>' href="<?=base_url()?>portofolio/index">Portofolio</a>
                </li>
               <li>
                    <a class='<?=($module == 'gallery' ? 'active' : '')?>' href="<?=base_url()?>gallery/index">Gallery</a>
                </li>
               <li>
                    <a class='<?=($module == 'testimony' ? 'active' : '')?>' href="<?=base_url()?>testimony/index">Testimonial</a>
                </li>
               <li>
                    <a class='<?=($module == 'contact' ? 'active' : '')?>' href="javascript:void(0)">Contact Us</a>
                    <ul class="dropdown">
                       <li>
                            <a href="<?=base_url()?>contact/index/help">Contact Help</a>
                        </li>
                       <li>
                            <a href="<?=base_url()?>contact/index/complain">Contact Complain</a>
                        </li>
                    </ul>
                </li>
               <?php
                    if($getsession_login == 1) {
                ?>
                <li>
                    <a class='<?=($module == 'testimony' ? 'active' : '')?>' href="<?=base_url()?>register/do_logout">Sign Out</a>
                </li>
                <?php } ?> 
    </ul>
    <!-- Mobile Menu End -->

</div>
<!-- End Header Logo & Naviagtion -->