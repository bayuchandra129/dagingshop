<?php  if (!defined('BASEPATH'))
    exit('No direct script access allowed');
Class Informasi extends CI_Controller {
    public function __construct() {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
    }
    
    public function contact() {

        $data['show_kategori_menu'] = $this->core_base->getdata('kategori', null, Array('nm_kategori','DESC'));
        $data['menu'] = 'menu';
        $data['module'] = 'contact';
        $this->load->view('template', $data);
    }   

    public function carabelanja() {
        $data['show_kategori_menu'] = $this->core_base->getdata('kategori', null, Array('nm_kategori','DESC'));
        $data['menu'] = 'menu';
        $data['module'] = 'info_carabelanja';
        $this->load->view('template', $data);
    }   

    public function tentangkami() {
        $data['show_kategori_menu'] = $this->core_base->getdata('kategori', null, Array('nm_kategori','DESC'));
        $data['menu'] = 'menu';
        $data['module'] = 'info_tentangkami';
        $this->load->view('template', $data);
    }

    public function bankaccount() {
        $data['show_kategori_menu'] = $this->core_base->getdata('kategori', null, Array('nm_kategori','DESC'));
        $data['menu'] = 'menu';
        $data['module'] = 'info_bankaccount';
        $this->load->view('template', $data);
    }

    public function faq() {
        $data['show_kategori_menu'] = $this->core_base->getdata('kategori', null, Array('nm_kategori','DESC'));
        $data['menu'] = 'menu';
        $data['module'] = 'info_faq';
        $this->load->view('template', $data);
    }

    public function konfirmasipembayaran() {
        
        $data['show_kategori_menu'] = $this->core_base->getdata('kategori', null, Array('nm_kategori','DESC'));
        $data['menu'] = 'menu';
        $data['module'] = 'info_konfirmasipembayaran';
        $this->load->view('template', $data);
    }

    public function processContact($params = null) {
        $getsession_id = $this->session->userdata('id');
        $getsession_logged_in = $this->session->userdata('logged_in');
        
        $status = null;
        $msg = null;
        
        $name = $this->input->post('name');
        $email = $this->input->post('email');
        $subject = $this->input->post('subject');
        $message = $this->input->post('message');
        $date_add = date('Y-m-d H:i:s');


        $table = 'kontak';
        $data = Array(
            'nama' => $name,
            'email' => $email,
            'subjek' => $subject,
            'isi_pesan' => $message,
            'date_add' => $date_add
        );
        $insert = $this->db->insert($table, $data);

        if ($insert) {
            echo 1;
        } else {
            echo 0;
        }
    
    }

    public function proccessConfirmPayment() {
        // jika no order tidak valid
        $status = null;
        $msg = null;
        $kode_pembayaran = $this->core_base->kodepembayaran();
        $kode_pesanan = $this->input->post('kode_pesanan');
        $tgl_bayar = $this->input->post('tgl_bayar');
        $nama_pembank = $this->input->post('nama_pembank');
        $no_rek = $this->input->post('no_rek');
        $nm_bank = $this->input->post('nm_bank');
        $total_bayar = $this->input->post('total_bayar');
        

        $cek_no = $this->core_base->countdata('pesanan', Array('kd_pesanan' => $kode_pesanan));

        if ($cek_no < 1) {
            $status = 'not_valid_noorder';
            $msg = 'Kode Pesanan yang anda masukan tidak valid';
        } else {
            if (isset($_FILES["foto_bukti"]) && $_FILES["foto_bukti"]["error"] == UPLOAD_ERR_OK) {
                $UploadDirectory = './repository/konfirmasi_pembayaran/';

                //Is file size is less than allowed size.
                if ($_FILES["foto_bukti"]["size"] > 2042880) {
                    $status = 'failed';
                    $msg = 'File size is too big!';
                }

                //allowed file type Server side check
                switch (strtolower($_FILES['foto_bukti']['type'])) {
                    //allowed file types
                    case 'image/png':
                    case 'image/jpeg':
                    case 'image/pjpeg':
                        break;
                    default:
                        $status = 'failed';
                        $msg = 'Unsupported File!';
                }

                $File_Name = strtolower($_FILES['foto_bukti']['name']);
                $File_Ext = substr($File_Name, strrpos($File_Name, '.')); //get file extention
                $Random_Number = rand(0, 9999); //Random number to be added to name.
                $NewFileName = $Random_Number . $File_Ext; //new file name
                // jika user upload lebih dari 1, maka gagal upload

                $move = move_uploaded_file($_FILES['foto_bukti']['tmp_name'], $UploadDirectory . $NewFileName);
                if ($move) {
                    $update_purchase_confirmation = $this->core_base->insertdata('pembayaran', Array(
                        'kd_pembayaran' => $kode_pembayaran,
                        'kd_pesanan' => $kode_pesanan,
                        'tgl_pembayaran' => $tgl_bayar,
                        'nm_pembank' => $nama_pembank,
                        'no_rek' => $no_rek,
                        'nm_bank' => $nm_bank,
                        'total_bayar' => $total_bayar,
                        'foto_bukti' => $NewFileName,
                        'status_pembayaran' => 0,
                        'pem_date_add' => date('Y-m-d H:i:s')
                    ));
                            
                    if ($update_purchase_confirmation) {
                        $status = 'success';
                        $msg = 'Terimakasih sudah konfirmasi, kami segera proses';
                    }
                }
            } else {
                $status = 'failed';
                $msg = 'Something wrong with upload! Is "upload_max_filesize" set correctly?';
            }
        }

        echo json_encode(Array('status' => $status, 'msg' => $msg));

    }
}
