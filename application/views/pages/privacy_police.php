<br/><br/>
<div id="content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="big-title text-center">
                    <h1><?= $show_data['sw_title'] ?></h1>
                </div>
                <p class="text-left" style="margin-bottom:20px;">
                    <?= $show_data['sw_description'] ?>
                </p>
            </div>
        </div>
    </div>         
</div>
