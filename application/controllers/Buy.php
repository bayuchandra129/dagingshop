<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');

Class Buy extends CI_Controller {

    public function __construct() {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
    }

    public function how_to_buy() {
         $data['show_contact'] = $this->core_base->getdata('settings_contact', Array('id_settings_contact' => 1));
        $data['show_data'] = $this->core_base->getdata('settings_web_footer', Array('id_settings_wf' => 1));
        $data['menu'] = 'menu';
        $data['module'] = 'how_to_buy';
        $this->load->view('template', $data);
    }
    
    public function payment() {
         $data['show_contact'] = $this->core_base->getdata('settings_contact', Array('id_settings_contact' => 1));
        $data['show_data'] = $this->core_base->getdata('settings_web_footer', Array('id_settings_wf' => 2));
        $data['menu'] = 'menu';
        $data['module'] = 'payment';
        $this->load->view('template', $data);
    }
}
