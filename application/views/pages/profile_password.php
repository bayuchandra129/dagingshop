<section>
    <div class="content-cart">
        <div id="content">

            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h3 class="title">MEMBER AREA<i class="fa fa-caret-right"></i><span>Profile</span></h3>
                        <div class="welcome">
                            <span class="hello">Halo, <?= $this->session->userdata('fullname'); ?><i></i></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!-- left form -->
                    <div class="col-xs-2 col-sm-2 col-md-3 ">
                        <?php require_once($sidebar_dashboard . '.php'); ?>
                    </div>
                    <div class="col-xs-10  col-sm-10 col-md-9 content-wrap">
                        <!-- Profil -->
                        <div class="profil">

                            <h3 class="profil-title">Ubah Password Member</h3><br/>

                            <div class="profil-wrap">

                                <h6 class="profil-title">Data Diri</h6>
                                <form id="Form-ChangePasswordId" method="post" enctype="multipart/form-data">    
                                <div class="form-wrap row">
                                    <div class="col-xs-12">
                                        <div class="col-xs-5"><label>Password Lama</label></div>
                                        <div class="col-xs-7"><input type="password" required="" class="form-control" name="password_lama" placeholder="Password Lama" autocomplete="off"></div>
                                    </div><br/>
                                    <div class="col-xs-12">
                                        <div class="col-xs-5"><label>Password Baru</label></div>
                                        <div class="col-xs-7"><input type="password" required="" class="form-control" name="password_baru" placeholder="Password Baru" autocomplete="off"></div>
                                    </div><br/>
                                    <div class="col-xs-12">
                                        <div class="col-xs-5"><label>Password Baru Konfirmasi</label></div>
                                        <div class="col-xs-7">
                                            <input type="password" required="" class="form-control" placeholder="Konfirmasi Password" value="" id="konfirmasi_password" name="konfirmasi_password" autocomplete="off">
                                    </div>
                                    </div><br/>
                                    <div class="col-xs-12">
                                        <div class="col-xs-5">
                                            <a href="<?=base_url()?>dashboard/profile" class="btn-submit pull-left btn-link-dashboard">CANCEL</a>
                                        </div>
                                        <div class="col-xs-7"><input onclick="return confirm('Apakah anda yakin ingin mengubah password Anda ?')" type="submit" value="UBAH" class="btn-submit"></div>
                                    </div>
                                </div>
                            </form>

                            </div>

                        </div>						</div>
                </div>

            </div>
        </div>
    </div>    
</section>
