<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');

Class Help extends CI_Controller {

    public function __construct() {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
    }

    public function terms_and_condition() {
         $data['show_contact'] = $this->core_base->getdata('settings_contact', Array('id_settings_contact' => 1));
        $data['show_data'] = $this->core_base->getdata('settings_web_footer', Array('id_settings_wf' => 3));
        $data['menu'] = 'menu';
        $data['module'] = 'terms_and_condition';
        $this->load->view('template', $data);
    }
    
    public function privacy_police() {
         $data['show_contact'] = $this->core_base->getdata('settings_contact', Array('id_settings_contact' => 1));
        $data['show_data'] = $this->core_base->getdata('settings_web_footer', Array('id_settings_wf' => 4));
        $data['menu'] = 'menu';
        $data['module'] = 'privacy_police';
        $this->load->view('template', $data);
    }
}
