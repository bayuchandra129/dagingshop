<br/><br/>
<div id="content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="big-title text-center">
                    <h1>Cara Belanja di Anya Living</h1>
                </div>
                <div style="font-size:16px;">
                <p class="text-left" style="margin-bottom:10px;font-size:16px;">
                Berikut ini adalah cara berbelanja di toko online kami:
                </p>
                <ol type="1">
                    <li>1. Pilih Barang</li>
                    <li>2. Check Out</li>
                    <li>3. Transfer pembayaran</li>
                    <li>4. Konfirmasi Pembayaran</li>
                    <li>5. Barang dikirim. Selesai</li>
                </ol>
                </div>
                
            </div>
        </div>
    </div>         
</div>
