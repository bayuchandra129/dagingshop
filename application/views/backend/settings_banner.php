<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Data Konfigurasi Banner
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?=base_url()?>backend"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li class="active">Data Konfigurasi Banner</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Data Master Konfigurasi Banner</h3><br/><br/>
                    <div class="col-md-3">
                        <a href="<?=base_url()?>backend/add/settings_banner" class="btn btn-block btn-primary">Tambah Konfigurasi Banner</a>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                     <table id="example1" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th class="text-center">Opsi</th>
                                <th class="text-center">Nama Staff</th>
                                <th class="text-center">Banner</th>
                                <th class="text-center">Banner Title</th>
                                <th class="text-center">Banner Deskripsi</th>
                                <th class="text-center">Date Add</th>
                                <th class="text-center">Date Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            $no = 1;
                                foreach($show_data as $val) {
                            ?>
                            <tr>
                                <td>
                                    <?=$no;?>
                                </td>
                                <td class="text-center">
                                    <a href="<?=base_url()?>backend/view/settings_banner/<?=$val['id_banner']?>">
                                        <span title="View" aria-hidden="true" class="glyphicon glyphicon-eye-open"></span> &nbsp;&nbsp;&nbsp;
                                    </a>
                                    
                                    <a href="<?=base_url()?>backend/edit/settings_banner/<?=$val['id_banner']?>">
                                        <span title="Edit" aria-hidden="true" class="glyphicon glyphicon-edit"></span> 
                                    </a>
                                </td>
                                 <td class="text-center">
                                    <?=$val['staff_fullname']?>
                                </td>
                                <td class="text-center">
                                    <?php
                                        if ($val['banner_pict'] == "") {
                                            ?> 
                                    <a class="fancybox" data-fancybox-group="gallery" href="<?= base_url() ?>assets/admin/img/not_available.jpg" >
                                            <img width="100" style="border:2px solid #000" src="<?= base_url() ?>assets/admin/img/not_available.jpg"/>
                                            </a>
                                        <?php } else { ?>
                                    <a class="fancybox" data-fancybox-group="gallery" title="<?=$val['banner_title']?>" href="<?= base_url() ?>repository/banner/<?= $val['banner_pict'] ?>" >
                                            <img width="100" style="border:2px solid #000" src="<?= base_url() ?>repository/banner/<?= $val['banner_pict'] ?>"/>
                                            </a>
                                        <?php } ?>
                                </td>
                                <td class="text-center">
                                    <?=$val['banner_title']?>
                                </td>
                                <td class="text-center">
                                    <?=$val['banner_desc']?>
                                </td>
                               
                                <td class="text-center">
                                    <?=$val['date_add']?>
                                </td>
                                <td class="text-center">
                                    <?=$val['date_edit']?>
                                </td>
                            </tr>
                            <?php $no++; } ?>
                        </tbody>
<!--                        <tfoot>
                            <tr>
                                <th>No</th>
                                <th class="text-center">Opsi</th>
                                <th class="text-center">About Name</th>
                                <th class="text-center">Date Add</th>
                                <th class="text-center">Date Edit</th>
                            </tr>
                        </tfoot>-->
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
