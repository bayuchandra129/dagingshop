
<section class="content-header">
    <h1>
        Dashboard
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= base_url() ?>backend/panel"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li class="active">Home</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3>Laporan Penjualan</h3>
                </div>
            </div><!-- /.box -->
        </div><!-- /.col -->
        <div class="col-xs-12">
            <div class="panel panel-default">
                <canvas id="myChart" width="1000" height="280"></canvas>
            </div>
        </div>    
    </div><!-- /.row -->
</section><!-- /.content -->
<script src="<?=base_url()?>assets/js/chart.min.js"></script>
<script type="text/javascript">
 var ctx = document.getElementById("myChart").getContext("2d");
	var chart = new Chart(ctx, {
		// The type of chart we want to create
		type: "line",

		// The data for our dataset
		data: {
			labels: <?=json_encode($bulan)?>,
			datasets: [
				{
					label: "Jumlah Pesanan",
					backgroundColor: "rgb(255, 99, 132)",
					borderColor: "rgb(255, 99, 132)",
					data: <?=json_encode($jumlah_bulanan)?>
				}
			]
		},

		// Configuration options go here
		options: {}
    });
</script>
