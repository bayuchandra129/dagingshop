<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Data Pesanan Pelanggan
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?=base_url()?>backend"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li class="active">Data Pesanan Pelanggan</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                
                <div class="box-header">
                    <h3 class="box-title">Data Pesanan Pelanggan</h3><br/><br/>
                    Total Cancel : <span class="badge bg-light-blue" title="" data-toggle="tooltip" data-original-title="<?=$count_cancel?>"><?=$count_cancel?></span> &nbsp;
                    Total Pending : <span class="badge bg-light-blue" title="" data-toggle="tooltip" data-original-title="<?=$count_pending?>"><?=$count_pending?></span> &nbsp;
                    Total Proccessing : <span class="badge bg-light-blue" title="" data-toggle="tooltip" data-original-title="<?=$count_proccessing?>"><?=$count_proccessing?></span> &nbsp;
                    Total Delivery : <span class="badge bg-light-blue" title="" data-toggle="tooltip" data-original-title="<?=$count_delivery?>"><?=$count_delivery?></span> &nbsp;
                    Total Complete : <span class="badge bg-light-blue" title="" data-toggle="tooltip" data-original-title="<?=$count_complete?>"><?=$count_complete?></span> <br><br>
                    Total Retur : <span class="badge bg-light-blue" title="" data-toggle="tooltip" data-original-title="<?=$count_complete?>"><?=$count_complete?></span> <br><br>
                    
                    <div class="pull-right">
                        <!-- <form action="<?= base_url() ?>excel/getexcel/user_order" id="formReport_<?=$module?>" method="post">
                            <span>Sort By : </span>
                            FROM &nbsp;&nbsp; <input type="text" required="required" name="tanggal1" class="datepicker form-sortby" placeholder="From Tanggal" value="" id="tanggal"  autocomplete="off"> 
                            &nbsp;TO &nbsp;&nbsp; <input type="text" required="required" name="tanggal2" class="datepicker form-sortby" placeholder="To Tanggal" value="" id="tanggal" autocomplete="off">
                            <input type="submit" class="btn btn-primary" value="Export Excel"/>
                        </form> -->
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                     <table id="example1" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th class="text-center">Kode Pesanan</th>
                                <th class="text-center">Nama Lengkap</th>
                                <th class="text-center">Email</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Tanggal Pesan</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            $no = 1;
                                foreach($show_data as $val) {
                            ?>
                            <tr>
                                <td>
                                    <?=$no;?>
                                </td>
                                
                                <td style="text-align:center;">                        
                                    <a href="<?=base_url()?>backend/edit/user_order/<?=$val['pesanan_code']?>"><?=$val['pesanan_code']?></a>
                                </td>
                                <td><?=$val['nm_plg']?></td>
                                <td><?=$val['email_plg']?></td>
                                <td style="text-align: center">
                                    <?php
                                         if($val['status_pesanan'] == 0) {
                                             $status = 'alert-danger';
                                         } else if($val['status_pesanan'] == 1) {
                                             $status = 'alert-warning';
                                         } else if($val['status_pesanan'] == 2) {
                                             $status = 'alert-info';
                                         } else if($val['status_pesanan'] == 3) {
                                            $status = 'alert-info'; 
                                         } else if($val['status_pesanan'] == 4) {
                                            $status = 'alert-success'; 
                                         } else if($val['status_pesanan'] == 5) {
                                            $status = 'alert-danger'; 
                                         }
                                    ?>
                                    <div style="border-radius:2px;" class="<?=$status?>">
                                    <?=status_pesanan($val['status_pesanan'])?>
                                    </div>
                                </td>
                                <td style="text-align: center"><?=formatDate($val['tgl_pesanan'])?></td>
                            </tr>
                            <?php $no++; } ?>
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
                
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
