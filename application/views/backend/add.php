<?php
if ($type == 'product') {
    ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Data Produk
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url() ?>backend"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li><a href="<?= base_url() ?>backend/product"><i class="fa fa-dashboard"></i>Data Produk</a></li>
            <li class="active">Tambah Produk</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Tambah Data Master Produk</h3>
                    </div><!-- /.box-header -->
                    <form action="<?= base_url() ?>proccess_backend/processadd/product" method="post" enctype="multipart/form-data" required="required" autocomplete="off">
                        <div class="box-body">
                            <div class="form-group">
                            <label>Kategori Produk</label>
                            <select name="product_category" required="required" class="select2 form-control" style="width: 100%;">
                                <option value="">-- Pilih Kategori --</option>
                                <?php foreach($show_kategory as $val_kategori) { ?>
                                <option value="<?=$val_kategori['kd_kategori']?>"><?=$val_kategori['nm_kategori']?></option>
                                <?php } ?>
                            </select>
                            </div>
                            <div class="form-group">
                                <label>Nama Produk : </label>
                                <input type="text" name="product_name"  class="form-control" placeholder="Enter Nama Produk" required="required">
                            </div>
                            
                            <div class="form-group">
                                <label>Stok  : </label>
                                <input type="text" onkeypress="return isNumber(event);" name="product_qty"  class="form-control" placeholder="Enter Total Produk" required="required">
                            </div>
                             
                            <div class="form-group">
                                <label for="exampleInputFile">Picture</label>
                                <input type="file" name="photo" id="exampleInputFile">
                                <p class="help-block">Max photo 1MB, Format (JPG, PNG).</p>
                            </div>
                            <div class="form-group">
                                <label>Diskon : </label>
                                <input type="text" name="product_diskon" onkeypress="return isNumber(event);"  class="form-control" placeholder="Enter Diskon Produk" required="required">
                            </div>
                            <div class="form-group">
                                <label>Harga Produk : </label>
                                <input type="text" name="product_price" onkeypress="return isNumber(event);"  class="form-control" placeholder="Enter Harga Produk" required="required">
                            </div>
                            <div class="form-group">
                                <label>Deskripsi Produk :</label><br/>
                                <textarea id="editor1" name="product_desc" rows="10" cols="80">
                                </textarea>
                            </div>
                        </div><!-- /.box-body -->
                        <div class="box-footer">
                            <a href="<?= base_url() ?>backend/product" class="btn btn-default">Cancel</a>
                            <button class="btn btn-primary pull-right" type="submit">Tambah Data</button>
                        </div>
                    </form>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
<?php } else if ($type == 'category_product') {
    ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Data Kategori
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url() ?>backend"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li><a href="<?= base_url() ?>backend/product"><i class="fa fa-dashboard"></i>Data Kategori</a></li>
            <li class="active">Tambah Kategori Produk</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Add Data Master Kategori</h3>
                    </div><!-- /.box-header -->
                    <form action="<?= base_url() ?>proccess_backend/processadd/category_product" method="post" enctype="multipart/form-data" required="required">
                        <div class="box-body">
                            
                            <div class="form-group">
                                <label>Nama Kategori Produk : </label>
                                <input type="text" name="category_product_name"  class="form-control" placeholder="Enter Nama Kategori" required="required">
                            </div>
                        </div><!-- /.box-body -->
                        <div class="box-footer">
                            <a href="<?= base_url() ?>backend/category_product" class="btn btn-default">Cancel</a>
                            <button class="btn btn-primary pull-right" type="submit">Tambah Data</button>
                        </div>
                    </form>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
<?php } else if ($type == 'ongkir') {
    ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Data Ongkir
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url() ?>backend"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li><a href="<?= base_url() ?>backend/product"><i class="fa fa-dashboard"></i>Data Ongkir</a></li>
            <li class="active">Tambah Ongkir</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Add Data Master Ongkir</h3>
                    </div><!-- /.box-header -->
                    <form action="<?= base_url() ?>proccess_backend/processadd/ongkir" method="post" enctype="multipart/form-data" required="required">
                        <div class="box-body">
                            
                            <div class="form-group">
                                <label>Nama  : </label>
                                <input type="text" name="nama"  class="form-control" placeholder="Enter Nama" required="required">
                            </div>
                            <div class="form-group">
                                <label>Harga  : </label>
                                <input type="text" name="harga" onkeypress="return isNumber(event);" class="form-control" placeholder="Enter Harga Ongkir" required="required">
                            </div>
                        </div><!-- /.box-body -->
                        <div class="box-footer">
                            <a href="<?= base_url() ?>backend/category_product" class="btn btn-default">Cancel</a>
                            <button class="btn btn-primary pull-right" type="submit">Tambah Data</button>
                        </div>
                    </form>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
<?php }  else if ($type == 'pengiriman') {
    ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Data Surat Jalan
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url() ?>backend"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li><a href="<?= base_url() ?>backend/pengiriman"><i class="fa fa-dashboard"></i>Data Surat Jalan</a></li>
            <li class="active">Tambah Surat Jalan</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Add Data Master Surat Jalan</h3>
                    </div><!-- /.box-header -->
                    <div class="content-error" style="margin:10px;"> 
                        <?php
                        if (!empty($_GET['info']) && $_GET['info'] == 'kodepesan-notfound') { ?>
                            <div class="alert alert-danger">
                                <strong>Sorry!</strong> Kode Pesan yang Anda masukan salah!
                            </div>
                        <?php } else if (!empty($_GET['info']) && $_GET['info'] == 'sj-available') { ?>
                            <div class="alert alert-danger">
                                <strong>Sorry!</strong> Kode Pesanan sudah pernah dibuatkan Surat Jalan
                            </div>
                        <?php } ?>
                    </div>
                    <form action="<?= base_url() ?>proccess_backend/processadd/pengiriman" method="post" enctype="multipart/form-data" required="required" autocomplete="off">
                        <div class="box-body">
                            <div class="form-group">
                                <label>Kode Pesanan : </label>
                                <input type="text" name="kd_pesanan"  class="form-control" placeholder="Enter Kode Pesanan" required="required">
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label>Nama Kurir : </label>
                                <select  name="nama_kurir"  class="form-control"  required="required">
                                        <option value="">-- Pilih Kurir --</option>
                                        <?php 
                                            foreach($kurir as $val) {
                                        ?>
                                        <option value="<?=$val['id_kurir']?>"><?=$val['nama_kurir']?></option>
                                        <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label>Tanggal Kirim : </label>
                                <input type="text" name="tgl_kirim"  class="form-control datepicker" placeholder="Enter Tanggan Kirim" required="required">
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label>Status Kirim : </label>
                                <select  name="status_kirim"  class="form-control"  required="required">
                                        <option value="">-- Pilih Status --</option>
                                        <option value="1">Dalam Perjalanan</option>
                                        <option value="2">Sudah Selesai</option>
                                </select>
                            </div>
                        </div>
                        <div class="box-footer">
                            <a href="<?= base_url() ?>backend/pengiriman" class="btn btn-default">Cancel</a>
                            <button class="btn btn-primary pull-right" type="submit">Tambah Data</button>
                        </div>
                    </form>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
<?php } else if ($type == 'kurir') {
    ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Data Kurir
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url() ?>backend"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li><a href="<?= base_url() ?>backend/kurir"><i class="fa fa-dashboard"></i>Data Kurir</a></li>
            <li class="active">Tambah Kurir</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Add Data Master Kurir</h3>
                    </div>
                    <form action="<?= base_url() ?>proccess_backend/processadd/kurir" method="post" enctype="multipart/form-data" required="required" autocomplete="off">
                        <div class="box-body">
                            <div class="form-group">
                                <label>Nama Lengkap : </label>
                                <input type="text" name="nama_kurir"  class="form-control" placeholder="Enter Nama Lengkap" required="required">
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label>Alamat  : </label>
                                <input type="text" name="alamat_kurir"  class="form-control" placeholder="Enter Alamat" required="required">
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label>No. HP  : </label>
                                <input type="text" name="no_hp_kurir" onkeypress="return isNumber(event);"  class="form-control" placeholder="Enter No. HP" required="required">
                            </div>
                        </div>
                        <div class="box-footer">
                            <a href="<?= base_url() ?>backend/kurir" class="btn btn-default">Cancel</a>
                            <button class="btn btn-primary pull-right" type="submit">Tambah Data</button>
                        </div>
                    </form>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
<?php }  else if ($type == 'staff') {
    ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Data Staff
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url() ?>backend"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li><a href="<?= base_url() ?>backend/staff"><i class="fa fa-dashboard"></i>Data Staff</a></li>
            <li class="active">Tambah Staff</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Tambah Data Master Staff</h3>
                    </div><!-- /.box-header -->
                    <form action="<?= base_url() ?>proccess_backend/processadd/staff" method="post" enctype="multipart/form-data" required="required">
                        <div class="box-body">
                         <div class="form-group">
                            <label>Hak Akses</label>
                            <select name="staff_hak_akses" required="required" class="select2 form-control" style="width: 100%;">
                              <option value="">-- Pilih Hak Akses --</option>
                              <?php foreach($show_hak_akses as $val_hak_akses) { ?>
                              <option value="<?=$val_hak_akses['id_privilege']?>"><?=$val_hak_akses['privilege_type']?></option>
                              <?php } ?>
                            </select>
                          </div>
                            <div class="form-group">
                                <label>Fullname : </label>
                                <input type="text" name="staff_fullname"  class="form-control" placeholder="Enter Fullname" required="required">
                            </div>
                              
                            <div class="form-group">
                                <label for="exampleInputFile">Photo</label>
                                <input type="file" name="photo" id="exampleInputFile">
                                <p class="help-block">Max photo 1MB, Format (JPG, PNG).</p>
                            </div>
                            <div class="form-group">
                                <label>Email : </label>
                                <input type="text" name="staff_email"  class="form-control" placeholder="Enter Email" required="required">
                            </div>
                            <div class="form-group">
                                <label>Phone : </label>
                                <input type="text" name="staff_phone"  class="form-control" placeholder="Enter Phone" required="required">
                            </div>
                            <div class="form-group">
                                <label>Address : </label>
                                <input type="text" name="staff_address"  class="form-control" placeholder="Enter Adress" required="required">
                            </div>
                            <div class="form-group">
                                <label>
                              Gender :
                            </label>
                            <label>
                                <input type="radio" name="staff_gender" class="minimal" value="L"> &nbsp; L
                            </label>
                            <label>
                                <input type="radio" name="staff_gender" class="minimal" value="P"> &nbsp; P
                            </label>
                            
                          </div>
                        </div>
                        <div class="box-body">
                            <h3>Akun Login</h3>
                            <div class="form-group">
                                <label>Username : </label>
                                <input type="text" name="username"  class="form-control" placeholder="Enter username" required="required">
                            </div>
                            <div class="form-group">
                                <label>Password : </label>
                                <input type="password" name="password"  class="form-control" placeholder="Enter password" required="required">
                            </div>
                        </div><!-- /.box-body -->
                        <div class="box-footer">
                            <a href="<?= base_url() ?>backend/product" class="btn btn-default">Cancel</a>
                            <button class="btn btn-primary pull-right" type="submit">Tambah Data</button>
                        </div>
                    </form>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
<?php } else if ($type == 'settings_banner') {
    ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Data Konfigurasi Banner
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url() ?>backend"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li><a href="<?= base_url() ?>backend/galery"><i class="fa fa-dashboard"></i>Data Konfigurasi Banner</a></li>
            <li class="active">Add Konfigurasi Banner</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Add Data Master Konfigurasi Banner</h3>
                    </div><!-- /.box-header -->
                    <form action="<?= base_url() ?>proccess_backend/processadd/settings_banner" method="post" enctype="multipart/form-data" required="required">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleInputFile">Gambar Banner</label>
                                <input type="file" name="photo" id="exampleInputFile">
                                <p class="help-block">Max photo 1MB, Format (JPG, PNG).</p>
                            </div>
                            <div class="form-group">
                                <label>Judul Banner : </label>
                                <input type="text" name="banner_title"  class="form-control" placeholder="Enter Judul Banner" required="required">
                            </div>
                            <div class="form-group">
                                <label>Deskripsi Banner : </label>
                                <input type="text" name="banner_desc"  class="form-control" placeholder="Enter Description Banner" required="required">
                            </div>
                        </div><!-- /.box-body -->
                        <div class="box-footer">
                            <a href="<?= base_url() ?>backend/settings_banner" class="btn btn-default">Cancel</a>
                            <button class="btn btn-primary pull-right" type="submit">Add</button>
                        </div>
                    </form>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
<?php } ?>