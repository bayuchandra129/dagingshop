<?php 
Class Pdf extends CI_Controller{
    
    function __construct() {
        parent::__construct();

    }
    
    function getcetak($param, $no_order){
        
        if($param == 'surat_jalan') {
            $pdf = new FPDF('P','mm','A4');
        } else if($param == 'invoice') {
            $pdf = new FPDF('P','mm','A4');
        } else if($param == 'retur') {
            $pdf = new FPDF('l','mm','A5');
        }
        // membuat halaman baru
        $pdf->AddPage();
        if($param == 'surat_jalan') {
            $getsj =  $this->core_admin->getrelasi('relasi_pengiriman', $no_order);
            
            $pdf->Image(base_url().'assets/images/logo.jpeg',10,5,30);
            // setting jenis font yang akan digunakan
            $pdf->SetFont('Arial','BU',12);
            // mencetak string 
            $pdf->Cell(190,17,'Bukti Tanda Terima',0,1,'C');
            
            // Memberikan space kebawah agar tidak terlalu rapat
            
            $pdf->Cell(10,7,'',0,1);
            $pdf->SetFont('Arial','',10);
            $pdf->Cell(40,6,'No. Surat Jalan',0,0,'L');
            $pdf->Cell(65,6,$getsj[0]['kd_sj'],0,0,'L');
            $pdf->Cell(30,6,'Pengiriman Ke',1,1,'L');
            
            $pdf->Cell(40,6,'No. Pesanan',0,0,'L');
            $pdf->Cell(65,6,$getsj[0]['kd_pesanan'],0,0,'L');
            $pdf->Cell(30,6,'Nama',0,0,'L');
            $pdf->Cell(55,6,$getsj[0]['nm_plg'],0,1,'L');

            $pdf->Cell(40,6,'PT. Anya Living',0,0,'L');
            $pdf->Cell(65,6, '02135760354',0,0,'L');
            $pdf->Cell(30,6,'Kota',0,0,'L');
            $pdf->Cell(55,6,$getsj[0]['nama_kota'],0,1,'L');

            $pdf->Cell(40,6,'Tanggal Pengiriman',0,0,'L');
            $pdf->Cell(65,6,formatDates($getsj[0]['tgl_sj']),0,0,'L');
            $pdf->Cell(30,6,'Alamat Kirim',0,0,'L');
            $pdf->Cell(55,6,$getsj[0]['alamat_kirim'],0,1,'L');

            
            $pdf->Cell(40,6,'Nama Kurir',0,0,'L');
            $pdf->Cell(65,6,$getsj[0]['nama_kurir'],0,0,'L');
            $pdf->Cell(30,6,'Phone',0,0,'L');
            $pdf->Cell(55,6,$getsj[0]['tlp_plg'],0,1,'L');

            $pdf->Cell(10,12,'',0,1);
            $pdf->Cell(105,6,'Nama Produk',1,0,'C');
            $pdf->Cell(85,6,'Qty',1,1,'C');

            
            
            $pdf->SetFont('Arial','',10);
      
            $getorder = $this->core_admin->getrelasi('relasi_cetak_suratjalan',  $getsj[0]['kd_pesanan']);

    

            foreach ($getorder as $row){
                $pdf->Cell(105,6,$row['nm_produk'],1,0,'C');
                $pdf->Cell(85,6,$row['qty'],1,1,'C');
            }

            $pdf->Cell(10,20,'',0,1);
            $pdf->SetFont('Arial','',10);
            $pdf->Cell(40,6,'Catatan',0,0,'L');
            $pdf->Cell(65,6,$getsj[0]['catatan'],0,'L');
            
            $pdf->Cell(10,20,'',0,1);
            $pdf->SetFont('Arial','',10);
            $pdf->Cell(40,36,'Hand Over By',0,0,'L');
            $pdf->Cell(65,6,'',0,'L');
            $pdf->Cell(30,36,'Reiceved By',0,1,'L');
            $pdf->Cell(55,6,'',0,1,'L');
            
            $pdf->SetFont('Arial','',10);
            $pdf->Cell(40,6,'Date',0,0,'L');
            $pdf->Cell(65,6,'',0,'L');
            $pdf->Cell(30,6,'Date',0,0,'L');
            $pdf->Cell(55,6,'',0,1,'L');

        } else if($param == 'invoice') {
            $data['show_data'] = $this->core_admin->getrelasi('user_order', $no_order);
            $data['payment'] = $this->core_admin->getdata('pembayaran', Array('kd_pesanan' => $no_order));
            $data['pesanan_saya'] = $this->core_base->getrelasi('relasi_purchase', $no_order);

            $pdf->Image(base_url().'assets/images/logo.jpeg',10,5,30);
            // setting jenis font yang akan digunakan
            $pdf->SetFont('Arial','BU',12);
            // mencetak string 
            $pdf->Cell(190,17,'Invoice',0,1,'C');
            
            // Memberikan space kebawah agar tidak terlalu rapat
            $pdf->Cell(10,7,'',0,1);
            $pdf->SetFont('Arial','U',10);
            $pdf->Cell(105,6,'Invoice To',0,0,'L');
            $pdf->Cell(30,6,'No. Invoice.',0,0,'L');
            $pdf->Cell(40,6, $data['payment']['kd_pembayaran'],0,1,'L');
            $pdf->SetFont('Arial','',10);
            $pdf->Cell(40,6,'Nama',0,0,'L');
            $pdf->Cell(65,6, $data['show_data'][0]['nm_plg'],0,0,'L');
            $pdf->Cell(30,6,'No. Pesanan.',0,0,'L');
            $pdf->Cell(40,6, $data['show_data'][0]['pesanan_code'],0,1,'L');

            $pdf->Cell(40,6,'Alamat',0,0,'L');
            $pdf->Cell(65,6, $data['show_data'][0]['alamat'],0,0,'L');
            $pdf->Cell(30,6,'Date',0,0,'L');
            $pdf->Cell(55,6, formatDate($data['show_data'][0]['tgl_pesanan']),0,1,'L');

            $pdf->Cell(40,26,'',0,0,'L');
            $pdf->Cell(65,26, '',0,0,'L');
            $pdf->Cell(30,26,'Payment Terms',0,0,'L');
            $pdf->Cell(55,26, 'Bank Transfer',0,1,'L');

            $pdf->Cell(40,6,'Contact',0,0,'L');
            $pdf->Cell(65,6, $data['show_data'][0]['nm_plg'],0,0,'L');
            $pdf->Cell(30,6,'',0,0,'L');
            $pdf->Cell(55,6,' ',0,1,'L');

            $pdf->Cell(40,6,'Phone',0,0,'L');
            $pdf->Cell(65,6, $data['show_data'][0]['tlp_plg'],0,0,'L');
            $pdf->Cell(30,6,'',0,0,'L');
            $pdf->Cell(55,6, '',0,1,'L');

            $pdf->Cell(40,10,'Email',0,0,'L');
            $pdf->Cell(65,10,$data['show_data'][0]['email_plg'],0,0,'L');
            $pdf->Cell(30,10,'',0,0,'L');
            $pdf->Cell(55,10, '',0,1,'L');

            // $pdf->Cell(40,8,'',0,0,'L');
            // $pdf->Cell(65,8,'',0,0,'L');
            // $pdf->Cell(85,8,'Note.',1,1,'L');

            // $pdf->Cell(40,6,'',0,0,'L');
            // $pdf->Cell(65,6,'',0,0,'L');
            // $pdf->Cell(85,26,'.',1,1,'L');

            $pdf->Cell(10,7,'',0,1);
            $pdf->Cell(95,6,'Product',1,0,'C');
            $pdf->Cell(40,6,'Unite Price',1,0,'C');
            $pdf->Cell(15,6,'Qty',1,0,'C');
            $pdf->Cell(35,6,'Amount',1,1,'C');

            $sub_total = 0;
           foreach ($data['pesanan_saya'] as $row){
            $sub_total += $row['harga_jual'] * $row['qty'];

            $pdf->SetFont('Arial','',8);
            $pdf->Cell(95,6, $row['nm_produk'],1,0,'C');
            $pdf->Cell(40,6,'Rp. '.rupiah($row['harga_jual']),1,0,'C');
            $pdf->Cell(15,6,$row['qty'],1,0,'C');
            $pdf->Cell(35,6, 'Rp. '.rupiah($row['harga_jual'] * $row['qty']),1,1,'C');
           } 

            $pdf->Cell(60,10,'',0,1);
            $pdf->Cell(40,8,'',0,0,'L');
            $pdf->Cell(65,8,'',0,0,'L');
            $pdf->Cell(35,8,'Sub Total.',0,0,'L');
            $pdf->Cell(15,8,':',0,0,'L');
            $pdf->Cell(35,8, 'Rp. '.rupiah($sub_total),0,1,'L');

            $pdf->Cell(40,8,'',0,0,'L');
            $pdf->Cell(65,8,'',0,0,'L');
            $pdf->Cell(35,8,'Shipment.',0,0,'L');
            $pdf->Cell(15,8,':',0,0,'L');
            $pdf->Cell(35,8,'Rp. '.rupiah($data['show_data'][0]['harga_ongkir']),0,1,'L');
            // $pdf->Cell(40,8,'',0,0,'L');
            // $pdf->Cell(65,8,'',0,0,'L');
            // $pdf->Cell(35,8,'Diskon.',0,0,'L');
            // $pdf->Cell(15,8,':',0,0,'L');
            // $pdf->Cell(35,8,'',0,1,'L');

            $pdf->Cell(107,1,'',0,0);
            $pdf->Cell(85,1,'',1,1);

            $pdf->Cell(40,8,'',0,0,'L');
            $pdf->Cell(65,8,'',0,0,'L');
            $pdf->Cell(35,8,'Total.',0,0,'L');
            $pdf->Cell(15,8,':',0,0,'L');
            $pdf->Cell(35,8,'Rp. '.rupiah($sub_total + $data['show_data'][0]['harga_ongkir']),0,1,'L');

            $pdf->SetFont('Arial','',10);
      

            $pdf->Cell(60,20,'',0,1);
            $pdf->SetFont('Arial','',10);
            $pdf->Cell(60,6,'Seller Signature',0,0,'L');
            $pdf->Cell(65,6,'',0,'L');
            $pdf->Cell(30,6,'Buyer Signature',0,1,'L');
            $pdf->Cell(55,6,'',0,1,'L');
            
            $pdf->SetFont('Arial','',10);
            $pdf->Cell(60,36,'Oki Sulistyo',0,0,'L');
            $pdf->Cell(65,6,'',0,'L');
            $pdf->Cell(30,36,$data['show_data'][0]['nm_plg'],0,0,'L');
            $pdf->Cell(55,6,'',0,1,'L');

        } else if($param == 'retur') {
            $data['show_data'] = $this->core_admin->getrelasi('cetak_retur', $no_order);
            $data['pesanan_saya'] = $this->core_admin->getrelasi('cetak_detil_retur', $no_order);

            $pdf->Image(base_url().'assets/images/logo.jpeg',10,5,30);
            // setting jenis font yang akan digunakan
            $pdf->SetFont('Arial','BU',12);
            // mencetak string 
            $pdf->Cell(190,17,'RETUR',0,1,'C');
            
            // Memberikan space kebawah agar tidak terlalu rapat
            $pdf->Cell(10,4,'',0,1);
            $pdf->SetFont('Arial','B',12);
            $pdf->Cell(30,6,'No. Pesanan',0,0,'L');
            $pdf->Cell(40,6,$data['show_data'][0]['kd_pesanan'],0,0,'L');

            $pdf->Cell(10,7,'',0,1);
            $pdf->SetFont('Arial','',10);
            $pdf->Cell(40,6,'',0,0,'L');
            $pdf->Cell(65,6, '',0,0,'L');
            $pdf->Cell(30,6,'Nomor Retur.',0,0,'L');
            $pdf->Cell(40,6, $data['show_data'][0]['kd_retur'],0,1,'L');

            $pdf->Cell(10,3,'',0,1);
            $pdf->SetFont('Arial','',10);
            $pdf->Cell(40,3,'Kepada',0,0,'L');
            $pdf->Cell(65,3, $data['show_data'][0]['nm_plg'],0,0,'L');
            $pdf->Cell(30,3,'Tanggal Retur',0,0,'L');
            $pdf->Cell(40,3, formatDate($data['show_data'][0]['tgl_retur']),0,1,'L');

            $pdf->Cell(10,5,'',0,1);
            $pdf->SetFont('Arial','',10);
            $pdf->Cell(40,3,'Alamat',0,0,'L');
            $pdf->Cell(65,3, $data['show_data'][0]['alamat'],0,0,'L');
            // $pdf->Cell(30,3,'No. Surat Jalan',0,0,'L');
            // $pdf->Cell(40,3, '',0,1,'L');

            $pdf->Cell(10,7,'',0,1);
            $pdf->SetFont('Arial','',10);
            $pdf->Cell(40,3,'Telp',0,0,'L');
            $pdf->Cell(65,3, $data['show_data'][0]['tlp_plg'],0,0,'L');
            $pdf->Cell(30,3,'',0,0,'L');
            $pdf->Cell(40,3, '',0,1,'L');

            $pdf->Cell(10,3,'',0,1);
            $pdf->SetFont('Arial','',10);
            $pdf->Cell(40,3,'Email',0,0,'L');
            $pdf->Cell(65,3, $data['show_data'][0]['email_plg'],0,0,'L');
            $pdf->Cell(30,3,'',0,0,'L');
            $pdf->Cell(40,3, '',0,1,'L');

           $pdf->SetLeftMargin(45);
            $pdf->Cell(10,7,'',0,1);
            $pdf->Cell(10,6,'No.',1,0,'C');
            $pdf->Cell(40,6,'Nama Produk',1,0,'C');
            $pdf->Cell(15,6,'Qty',1,0,'C');
            $pdf->Cell(65,6,'Keterangan Retur',1,1,'C');

            $i = 1; 
            foreach ($data['pesanan_saya'] as $row){
                $pdf->SetFont('Arial','',8);
                $pdf->Cell(10,6,$i,1,0,'C');
                $pdf->Cell(40,6,  $row['nama_produk'],1,0,'C');
                $pdf->Cell(15,6, $row['quantity'],1,0,'C');
                $pdf->Cell(65,6, $row['keterangan'],1,1,'C');
           $i++; }

            $pdf->SetLeftMargin(5);
            $pdf->Cell(10,10,'',0,1);
            $pdf->SetFont('Arial','',10);
            $pdf->Cell(40,3,'',0,0,'L');
            $pdf->Cell(115,3, '',0,0,'L');
            $pdf->Cell(30,3,'Staff Admin,',0,0,'L');
            $pdf->Cell(40,3, '',0,1,'L');

            $pdf->Cell(10,16,'',0,1);
            $pdf->SetFont('Arial','',10);
            $pdf->Cell(40,3,'',0,0,'L');
            $pdf->Cell(115,3, '',0,0,'L');
            $pdf->Cell(30,3,'(                 )',0,0,'L');
            $pdf->Cell(40,3, '',0,1,'L');

           

        }

        $pdf->Output();
    }
}