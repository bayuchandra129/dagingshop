<?php

Class Fpassword extends CI_Controller {

    public function __construct() {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
    }

    public function index() {
        $data['menu'] = 'menu';
        $data['module'] = 'fpassword';
        $this->load->view('template', $data);
    }

    public function do_ubahfpassword($params = null) {
        $password_lama = $this->input->post('password_lama');
        $password_baru = sha1(md5($this->input->post('password_baru')));
        $konfirmasi_password = sha1(md5($this->input->post('konfirmasi_password')));

        // Cek password lama salah si user lupa password
        $cek_pass_lama = $this->core_base->countdata('user', Array('user_forgot_password' => $password_lama));
        
        // Jika di count tidak sama atau data kurang lebih dari 1
        if ($cek_pass_lama < 1) {
            $status = 'password_tak_sama';
            $msg = 'Password lama anda tidak valid';
        } else { // Jika datanya lebih dari 0
            // cek password baru dan konfirmasi password
            if ($password_baru != $konfirmasi_password) {
                $status = 'password_konfirmasi_tak_sama';
                $msg = 'Password baru dan konfirmasi pasword tidak sama';
            } else { // jika password baru dan password konfirmasi sama, dia update
                $data = Array('password' => $password_baru);
                $update_password = $this->core_base->updatedata('user', $data, Array('user_forgot_password' => $params));
                if ($update_password) {
                    $status = 'berhasil';
                    $msg = 'Password baru anda berhasil di ubah';
                }
            }
        }

        echo json_encode(Array('status' => $status, 'msg' => $msg));
    }

}
