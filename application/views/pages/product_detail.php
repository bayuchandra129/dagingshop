<!-- Container -->
<div id="container">
    <br/> 
    <!-- Start Page Banner -->
    <div class="page-banner" style="padding:40px 0; background: url(images/slide-02-bg.jpg) center #f9f9f9;">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>Keterangan Produk</h2>
                </div>
                <div class="col-md-6">
                    <ul class="breadcrumbs">
                        <li><a href="<?= base_url() ?>">Home</a></li>
                        <li><a href="javascript:void(0)">Keterangan Product</a></li>
                        <li><?= $show_product[0]['nm_produk'] ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Banner -->

    <br/><br/>
    <!-- Start Content -->
    <div id="content">
        <div class="container">
            <div class="page-content">
                <!-- Start Team Members -->
                <div class="row">
                    <!-- Start Memebr 1 -->

                    <div class="" data-animation="fadeIn" data-animation-delay="01">

                        <div class="col-md-5">
                            <div>  
                                <div class="item">
                                    <center>
                                        <?php
                                        if ($show_product[0]['gambar_produk'] == "") {
                                            ?>
                                            <a class="lightbox" title="<?= $show_product[0]['nm_produk'] ?>" href="<?= base_url() ?>assets/admin/img/not_available.jpg" data-lightbox-gallery="gallery2">
                                                <img class=" img-recent-portofolio" width="480px" height="500px" alt="" src="<?= base_url() ?>assets/admin/img/not_available.jpg"/>
                                            </a>
                                        <?php } else { ?>
                                            <a class="lightbox" title="<?= $show_product[0]['nm_produk'] ?>" href="<?= base_url() . "repository/product/" . $show_product[0]['gambar_produk']; ?>" data-lightbox-gallery="gallery2">
                                                <img class=" img-recent-portofolio" width="480px" height="500px" alt="" src="<?= base_url() ?>repository/product/<?= $show_product[0]['gambar_produk'] ?>" />
                                            </a>
                                        <?php } ?>
                                    </center>
                                </div><br/>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <?php 
                                if($show_product[0]['diskon'] > 0) {
                                    $diskon = ($show_product[0]['harga'] / 100) * $show_product[0]['diskon'];
                                    $harga = $show_product[0]['harga'] - $diskon;
                                } else {
                                    $harga = $show_product[0]['harga'];
                                }
                            ?>
                            <form action="<?= base_url() ?>product/addcart" method="post">
                                <input type="hidden" name="id_product" value="<?= $show_product[0]['kd_produk'] ?>"/>
                                <input type="hidden" name="product_name" value="<?= $show_product[0]['nm_produk'] ?>"/>
                                <input type="hidden" name="product_price" value="<?= $harga ?>"/>
                                <input type="hidden" name="product_pict" value="<?= $show_product[0]['gambar_produk'] ?>"/>
                                <!-- Classic Heading -->
                                <h2 class="classic-title"><span><?= $show_product[0]['nm_produk'] ?></span></h2>
                                <div class="product-detail-description">
                                    <?php 
                                        $diskon = ($show_product[0]['harga'] / 100) * $show_product[0]['diskon'];
                                        $harga_diskon = $show_product[0]['harga'] - $diskon;
                                    ?>
                                    <h4>Harga : 
                                    <?php 
                                        if( $show_product[0]['diskon'] > 0) {
                                    ?>
                                    <label class="coret color-red-bold"><?=convert_to_rupiah($show_product[0]['harga'])?> </label>    
                                    <?=convert_to_rupiah($harga_diskon)?>
                                    <?php } else { ?>
                                    <?=convert_to_rupiah($show_product[0]['harga'])?>
                                    <?php } ?>
                                    </h4>
                                    <label style="color:#ff0000;font-weight:bold;padding-top:10px;">Stok Tersedia : <?= $show_product[0]['stok'] ?></label> <br/>
                                    <?= $show_product[0]['deskripsi'] ?>
                                </div>
                                <div class="classic-title"><span></span></div>

                                <div class="input-group number-spinner pull-right">
                                    <span class="input-group-btn data-dwn">
                                        <button type="button" class="btn btn-default btn-info" data-dir="dwn"><span class="glyphicon glyphicon-minus"></span></button>
                                    </span>
                                    <input type="text" class="form-control text-center" name="product_qty" value="1" min="0" max="<?= $show_product[0]['stok'] ?>">
                                    <span class="input-group-btn data-up">
                                        <button type="button" class="btn btn-default btn-info" data-dir="up"><span class="glyphicon glyphicon-plus"></span></button>
                                    </span>
                                </div>                     
                                <div class="pull-right">
                                        <input type="submit" onclick="return confirm('Apakah anda yakin ingin memesan ?')" id="SubmitRegisterId" class="btn-submit daftar" value="ADD TO CART">
                                </div>
                            </form>    
                        </div>

                    </div><br/><br/>
                    <div style="clear:both"></div>
                    <!-- End Memebr 1 -->
                    
                    <div style="padding-top: 30px">
                         <h4 class="classic-title"><span>Rekomendasi Produk</span></h4>
                        <div class="row">
                            <?php
                            if (empty($show_product)) {
                                ?>
                                <div class="col-md-12 ">
                                    <div class="bg-warning" style="padding:20px;border-radius:5px;">
                                        <h4 style="color:#ff0000">Ups, Stok produk tidak tersedia!</h4>
                                    </div>
                                </div>
                            <?php
                            } else {
                                $no = 2;
                                foreach ($show_recommend as $val) {
                                     $diskon = ($val['harga'] / 100) * $val['diskon'];
                                     $harga_diskon = $val['harga'] - $diskon;
                                    ?>
                                    <div class="col-sm-4 col-md-3">
                                        <div class="thumbnail" >
                                            <?php
                                                if ($val['gambar_produk'] == "") {
                                                ?>
                                                <a title="<?= $val['nm_produk'] ?>" href="<?= base_url() ?>product/detail/<?= $val['kd_produk'] ?>" >
                                                    <div class="thumb-overlay"><i class="fa fa-link"></i></div>
                                                    <img class="img-thumbnail img-responsive img-recent-produkall" width="480px" height="332px" alt="" src="<?= base_url() ?>assets/admin/img/not_available.jpg"/>
                                                </a>
                                            <?php } else { ?>
                                                <a  title="<?= $val['nm_produk'] ?>" href="<?= base_url() ?>product/detail/<?= $val['kd_produk'] ?>" >
                                                    <img class="img-thumbnail img-responsive img-recent-produkall" width="480" alt="" src="<?= base_url() ?>repository/product/<?= $val['gambar_produk'] ?>" />
                                                </a>
                                            <?php } ?>
                                            <div class="caption">
                                                <div class="row">
                                                    <div class="col-md-12 price ">
                                                        <?php 
                                                if($val['diskon'] > 0) {
                                            ?>
                                            <label class="color-red-bold text-product-price  pull-right"><?= convert_to_rupiah($harga_diskon) ?></label>  
                                            <label class="color-red-bold text-product-price coret  pull-right" style="padding-right:10px;"><?= convert_to_rupiah($val['harga']) ?></label> &nbsp;&nbsp;&nbsp;
                                                <?php } else { ?>
                                                    <label class="color-red-bold text-product-price  pull-right" style="padding-right:10px;"><?= convert_to_rupiah($val['harga']) ?></label> &nbsp;&nbsp;&nbsp;
                                                <?php } ?>
                                                    </div>
                                                    <div class="col-md-12" style="clear:both;">
                                                        <h3><?= $val['nm_produk'] ?></h3>
                                                    </div>
                                                    
                                                </div> <br/>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <a href="<?= base_url() ?>product/detail/<?= $val['kd_produk'] ?>" class="btn btn-primary btn-product" style="width:100%;"><span class="glyphicon glyphicon-shopping-cart"></span> Lihat Detil</a>
                                                    </div>
                                                </div>

                                                <p> </p>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    $no++;
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <!-- End Team Members -->

            </div>
            <!-- .container -->
        </div>
        <!-- End Team Member Section -->
    </div>
    <!-- End Content -->

</div>
<div style="clear:both"></div><br/><br/>
