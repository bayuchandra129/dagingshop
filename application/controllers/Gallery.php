<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');
Class Gallery extends CI_Controller {
    public function __construct() {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
    }
    
    public function index($params = null) {
        
        if($params == "") {
            
        } else {
            $data['show_gallery'] = $this->core_base->getdata('gallery', Array('id_gallery' => $params), null, null);
        }
         $data['show_contact'] = $this->core_base->getdata('settings_contact', Array('id_settings_contact' => 1));
        $data['params'] = $params;
        $data['show_recent_gallery'] = $this->core_base->getdata('gallery', null, Array('date_add','DESC'), null);
        $data['menu'] = 'menu';
        $data['module'] = 'gallery';
        $this->load->view('template', $data);
    }
}