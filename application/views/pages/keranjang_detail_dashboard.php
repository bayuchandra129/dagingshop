<section>
    <div class="content-cart">
        <div id="content">

            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h3 class="title">MEMBER AREA<i class="fa fa-caret-right"></i><span>Keranjang Belanja</span></h3>
                        <div class="welcome">
                            <span class="hello">Halo, <?= $this->session->userdata('fullname'); ?><i></i></span>
                            <span class="userid"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!-- left form -->
                    <div class="col-xs-2 col-sm-2 col-md-3 ">
                        <?php require_once($sidebar_dashboard . '.php'); ?>
                    </div>
                    <div class="col-xs-10  col-sm-10 col-md-9 content-wrap">
                        <!-- Profil -->
                        <div class="profil">

                            <h3 class="profil-title">DETAIL PESANAN SAYA</h3>
                            <span class="pull-right" style="font-size:14px;">No. Order : <b><?= $no_order ?></b></span><br/>
                            <br/>
                            <dl class="dl-horizontal">
                                <dt style="text-align:left">Kota</dt>
                                <dd><?=$kota?></dd><br>
                                <dt style="text-align:left">Alamat Pengiriman</dt>
                                <dd><?=$alamat_kirim?></dd><br>
                                <dt style="text-align:left">Catatan</dt>
                                <dd><?=$catatan?></dd><br>
                                <dt style="text-align:left">Tanggal Pesanan</dt>
                                <dd><?=formatDate($tgl_pesanan)?></dd><br>
                                
                            </dl>
                            <div class="profil-wrap">
                                <div class="table-responsive">          
                                    <table class="table table-hover table-condensed" id="cart">

                                        <thead>
                                            <tr>
                                                <th style="text-align:center">Foto Produk</th>
                                                <th style="text-align:center">Produk</th>
                                                <th style="text-align:center">Jumlah</th>
                                                <th style="text-align:center">Subtotal</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $total = 0;
                                            foreach ($pesanan_saya as $item) {
                                                $total += $item['harga_jual'] * $item['qty'];
                                                ?>
                                                <tr>
                                                    <td align="center" data-th="Product">
                                                        <img width="100" class="img-responsive" alt="..." src="<?= base_url() ?>repository/product/<?= $item['gambar_produk'] ?>">

                                                    </td>
                                                    <td align="center">
                                                        <h4 class="nomargin"><?= $item['nm_produk'] ?></h4>
                                                    </td>
                                                    <td align="center" data-th="Price"><?= $item['qty'] ?></td>
                                                    <td align="center" class="text-center" data-th="Subtotal">
                                                        Rp. <?= $this->cart->format_number($item['harga_jual'] * $item['qty']) ?>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="3"> <h5>Total :</h5></td>
                                                <td align="center"><b>Rp. <?= $this->cart->format_number($total) ?></b></td>
                                            </tr>
                                            <tr>
                                                <td colspan="3"> <h5>Ongkos Kirim :</h5></td>
                                                <td align="center"><b>Rp. <?= $this->cart->format_number($harga_ongkir) ?></b></td>
                                            </tr>
                                            <tr>
                                                <td colspan="3"> <h4>Grand Total :</h4></td>
                                                <td align="center"><b>Rp. <?= $this->cart->format_number($total + $harga_ongkir) ?></b></td>
                                            </tr>
                                        </tfoot>   
                                    </table>
                                </div>
                                <div style="font-size:16px;min-height: 200px;">
                                    <h3 class="profil-title">DETAIL KONFIRMASI PEMBAYARAN</h3>
                                    <?php
                                    if (empty($kd_pembayaran)) {
                                        ?>
                                        <br/><p style="font-size: 16px;">Anda belum melakukan konfirmasi pembayaran!</p>      
                                    <?php } else {
                                        ?>
                                        <div class="col-xs-12">
                                            <div class="col-xs-4">Bukti Transfer</div>
                                            <div class="col-xs-8">
                                                <?php
                                                if ($kd_pembayaran == "") {
                                                    ?>
                                                    <a class="lightbox"  href="<?= base_url() ?>assets/admin/img/not_available.jpg" data-lightbox-gallery="gallery2">
                                                        <img width="100" style="border:2px solid #000" src="<?= base_url() ?>assets/admin/img/not_available.jpg"/>
                                                    </a>
                                                <?php } else { ?>
                                                    <a class="lightbox" title="Rp. <?= $total_bayar ?>" href="<?= base_url() ?>repository/konfirmasi_pembayaran/<?= $foto_butki ?>" data-lightbox-gallery="gallery2">
                                                        <img width="100" style="border:2px solid #000" src="<?= base_url() ?>repository/konfirmasi_pembayaran/<?= $foto_butki ?>"/>    
                                                    </a>
                                                <?php } ?>
                                            </div> 
                                            <div class="col-xs-4" style="position:relative;top:10px;">Status Pembayaran</div>
                                            <div class="col-xs-8" style="position:relative;top:10px;">
                                                <?=status_payment($status_pembayaran)?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div> 
                                <div class="profil-wrap mt-2">
                                <h3 class="profil-title">DETAIL PENGIRIMAN</h3>
                                    <?php
                                    if (empty($status_kirim)) {
                                        ?>
                                        <br/><p style="font-size: 16px;">Pengiriman Anda Belum di Proses!</p>      
                                    <?php } else {
                                        ?>
                                        <p style="font-size: 16px;">Selamat Pesanan Anda sudah diKirimkan dengan Kurir <b><?=$nama_kurir?></b> </p><br/>
                                        <dt>Status Pengiriman</dt>
                                        <dd><?= status_surat_jalan($status_kirim) ?></dd> 
                                        <dt>Alamat Pengiriman</dt>
                                        <dd><?= $alamat_pengiriman ?></dd> 
                                    <?php } ?>
                                </div> 
                                <!-- <div style="font-size:16px;min-height: 200px;">
                                   <h3 class="profil-title">Testimoni Produk</h3>  
                                   <form id="formTestimoni" method="post" enctype="multipart/form-data" required="required">
                                    <div class="box-body"> 
                                         <div class="form-group">
                                            <label>Isi Testimoni : </label>
                                            <textarea type="text" name="isi_testimoni" class="form-control" placeholder="Isi Testimoni" rows="5" required="required"></textarea>
                                        </div>
                                        <input type="hidden" name="kd_pesanan" value="<?=$no_order?>"/>
                                         <button type="submit" style="float: right;margin-bottom:30px;" id="form-confirmid" class="btn-system btn-large pull-right">TESTIMONI</button>
                                    </div>
                                   </form>                  
                                </div> -->
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
</section>
<div style="clear:both"></div>
