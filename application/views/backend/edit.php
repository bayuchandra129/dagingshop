<?php
if ($type == 'user_order') {
    ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Data Detail Pesanan Pelanggan 
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url() ?>backend"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li><a href="<?= base_url() ?>backend/aboutus"><i class="fa fa-dashboard"></i>Data Detail Pesanan Pelanggan </a></li>
            <li class="active">Edit Detail Pesanan Pelanggan </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <form action="<?= base_url() ?>proccess_backend/processupdate/<?= $type ?>/<?= $id ?>" method="post" enctype="multipart/form-data" autocomplete="off">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Order <b><?= $show_data[0]['pesanan_code'] ?></b> Details</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <div class="col-md-4">
                                <h3><b>General Details</b></h3>
                                <dl>
                                    <dt>Order Date</dt>
                                    <dd><?= formatDate($show_data[0]['tgl_pesanan']) ?></dd><br/>
                                    <dt>Order Status</dt>
                                    <dd>
                                        <?php
                                        if ($show_data[0]['status_pesanan'] == 0) {
                                            $selected5 = '';
                                            $selected = 'selected';
                                            $selected1 = '';
                                            $selected2 = '';
                                            $selected3 = '';
                                            $selected4 = '';
                                        } else if ($show_data[0]['status_pesanan'] == 1) {
                                            $selected5 = '';
                                            $selected1 = 'selected';
                                            $selected = '';
                                            $selected2 = '';
                                            $selected3 = '';
                                            $selected4 = '';
                                        } else if ($show_data[0]['status_pesanan'] == 2) {
                                            $selected5 = '';
                                            $selected4 = '';
                                            $selected2 = 'selected';
                                            $selected = '';
                                            $selected1 = '';
                                            $selected3 = '';
                                        } else if ($show_data[0]['status_pesanan'] == 3) {
                                            $selected5 = '';
                                            $selected4 = '';
                                            $selected3 = 'selected';
                                            $selected = '';
                                            $selected2 = '';
                                            $selected1 = '';
                                        } else if ($show_data[0]['status_pesanan'] == 4) {
                                            $selected5 = '';
                                            $selected4 = 'selected';
                                            $selected3 = '';
                                            $selected = '';
                                            $selected2 = '';
                                            $selected1 = '';
                                        } else if ($show_data[0]['status_pesanan'] == 5) {
                                            $selected5 = 'selected';
                                            $selected4 = '';
                                            $selected3 = '';
                                            $selected = '';
                                            $selected2 = '';
                                            $selected1 = '';
                                        }
                                        ?>
                                        <select class="form-control" name="purchase_status">
                                            <option value="0" <?=$selected?>>Cancel</option>
                                            <option value="1" <?=$selected1?>>Pending</option>
                                            <option value="2" <?=$selected2?>>Proccessing</option>
                                            <option value="3" <?=$selected3?>>Delivery</option>
                                            <option value="4" <?=$selected4?>>Complete</option>
                                            <option value="5" <?=$selected5?>>Retur</option>
                                        </select>
                                    </dd><br/>
                                    <dt>Customer</dt>
                                    <dd>Guest</dd><br/>
                                    <dt>Catatan</dt>
                                    <dd><?= $show_data[0]['catatan'] ?></dd><br/>
                                </dl>
                            </div>
                            <div class="col-md-4">
                                <h3><b>Keterangan Penagihan</b></h3>
                                <dl>
                                    <dt>Alamat</dt>
                                    <dd>
                                        <?= $show_data[0]['alamat'] ?>
                                    </dd><br/>
                                    <dt>Nama Lengkap</dt>
                                    <dd><?= $show_data[0]['nm_plg'] ?></dd><br/>
                                    <dt>Email</dt>
                                    <dd><?= $show_data[0]['email_plg'] ?></dd><br/>
                                    <dt>Phone</dt>
                                    <dd><?= $show_data[0]['tlp_plg'] ?></dd><br/>
                                    
                                </dl>
                            </div>
                            <div class="col-md-4">
                                <h3><b>Detail Pengiriman</b></h3>
                                
                                <div>
                                    <dt>Nama Kota</dt>
                                    <dd><?= $show_data[0]['nama_kota'] ?></dd><br/>
                                    <dt>Alamat Pengiriman</dt>
                                    <dd><?= $show_data[0]['alamat_kirim'] ?></dd><br/>
                                   
                                </div>
                                <h3><b>Detail Pembayaran</b></h3>
                                <?php 
                                    if($kdpembayaran == null) {
                                ?>
                                <p class="color-red">Pelanggan Belum Melakukan Pembayaran</p>
                                    <?php } else { ?>
                                <div>
                                    <dt>Tanggal Pembayaran</dt>
                                    <dd><?=$tglpembayaran?></dd><br/>
                                </div>
                                <div>
                                    <dt>Bank Tujuan Transfer</dt>
                                    <dd><?=$nm_pembank?></dd><br/>
                                </div>
                                <div>
                                    <dt>No. Rekening</dt>
                                    <dd><?=$no_rek?></dd><br/>
                                </div>
                                <div>
                                    <dt>Bank Pelanggan</dt>
                                    <dd><?=$nm_bank?></dd><br/>
                                </div>
                                <div>
                                    <dt>Total Bayar</dt>
                                    <dd><?=$total_bayar?></dd><br/>
                                </div>
                                <div>
                                    <dt>Bukti Transfer</dt>
                                    <dd>
                                         <?php
                                        if ($foto_bukti == "") {
                                            ?>
                                        <a class="fancybox" data-fancybox-group="gallery" href="<?= base_url() ?>assets/admin/img/not_available.jpg" >
                                            <img width="100" style="border:2px solid #000" src="<?= base_url() ?>assets/admin/img/not_available.jpg"/>
                                            </a>
                                        <?php } else { ?>
                                        <a class="fancybox" data-fancybox-group="gallery"   href="<?= base_url() ?>repository/konfirmasi_pembayaran/<?= $foto_bukti ?>" >
                                            <img width="100" style="border:2px solid #000" src="<?= base_url() ?>repository/konfirmasi_pembayaran/<?= $foto_bukti ?>"/>
                                            </a>
                                        <?php } ?>
                                    </dd><br/>
                                </div>
                                <div>
                                    <dt>Status Pembayaran</dt>
                                    <dd><?=status_payment($status_pembayaran)?></dd><br/>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div><!-- /.box -->
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Order <b><?= $count_items ?></b> Items</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <table  class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th style="text-align:center">Foto Product</th>
                                        <th style="text-align:center">Produk</th>
                                        <th style="text-align:center">Harga Jual</th>
                                        <th style="text-align:center">Jumlah</th>
                                        <th style="text-align:center">Subtotal</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $total = 0;
                                    $sub_total = 0;
                                    foreach ($pesanan_saya as $item) {
                                        $sub_total += $item['harga_jual'] * $item['qty'];
                                        ?>
                                        <tr>
                                            <td align="center" data-th="Product">
                                                <a class="fancybox" data-fancybox-group="gallery"  title="<?= $item['nm_produk'] ?>" href="<?= base_url() ?>repository/product/<?= $item['gambar_produk'] ?>" >
                                                <img width="100" class="img-responsive" alt="..." src="<?= base_url() ?>repository/product/<?= $item['gambar_produk'] ?>">
                                                </a>
                                            </td>
                                            <td align="center">
                                                <h5 class="nomargin"><?= $item['nm_produk'] ?></h5>
                                            </td>
                                            <td align="center" data-th="Price">Rp. <?= $this->cart->format_number($item['harga_jual']) ?></td>
                                            <td align="center" data-th="Quantity">
                                                <div class="input-group">
                                                    <?= $item['qty'] ?>
                                                </div>
                                            </td>
                                            <td align="center" class="text-center" data-th="Subtotal">
                                                Rp. <?= $this->cart->format_number($item['harga_jual'] * $item['qty']) ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                         <td colspan="4" class="text-right"><b>Total Order : </b></td>
                                        <td class="text-center">Rp. <?=$this->cart->format_number($sub_total)?></td>
                                    </tr>
                                    <tr>
                                         <td colspan="4" class="text-right"><b>Ongkos Kirim : </b></td>
                                        <td class="text-center">Rp. <?=$this->cart->format_number($show_data[0]['harga_ongkir'])?></td>
                                    </tr>
                                    <tr>
                                         <td colspan="4" class="text-right"><b>Grand Total Order : </b></td>
                                        <td class="text-center">Rp. <?=$this->cart->format_number($sub_total + $show_data[0]['harga_ongkir'])?></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <div class="box-footer">
                           
                            <button class="btn btn-primary pull-right" type="submit">Update</button>
                        </div>
                    </div><!-- /.box -->
                    
                </div><!-- /.col -->
            </form>
        </div><!-- /.row -->
    </section><!-- /.content -->
<?php }  else if ($type == 'product') {
    ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Data Produk
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url() ?>backend"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li><a href="<?= base_url() ?>backend/product"><i class="fa fa-dashboard"></i>Data Produk</a></li>
            <li class="active">Edit Produk</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Edit Data Master Produk</h3>
                    </div><!-- /.box-header -->
                    <form action="<?= base_url() ?>proccess_backend/processupdate/<?= $type ?>/<?= $id ?>" method="post" enctype="multipart/form-data" required="required" autocomplete="off">
                        <div class="box-body">
                            <div class="form-group">
                                <label>Kategori Produk</label>
                                <select name="product_category" required="required" class="select2 form-control" style="width: 100%;">
                                    <option value="">-- Pilih Kategori --</option>
                                    <?php foreach ($show_kategory as $val_kategori) { ?>
                                        <?php
                                        if ($show_data[0]['kd_kategori'] == $val_kategori['kd_kategori']) {
                                            $selected1 = 'selected';
                                        } else {
                                            $selected1 = '';
                                        }
                                        ?>
                                        <option value="<?= $val_kategori['kd_kategori'] ?>" <?= $selected1 ?>><?= $val_kategori['nm_kategori'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Nama Produk : </label>
                                <input type="text" name="product_name"  class="form-control" value="<?= $show_data[0]['nm_produk'] ?>" placeholder="Enter Nama Produk" required="required">
                            </div>
                            <div class="form-group">
                                <label>Stok : </label>
                                <input type="text" onkeypress="return isNumber(event);" name="product_qty"  value="<?= $show_data[0]['stok'] ?>" class="form-control" placeholder="Enter Total Produk" required="required">
                            </div>
                            
                            
                            <div class="form-group">
                                <label for="exampleInputFile">Picture</label>
                                <input type="file" name="photo" id="exampleInputFile">
                                <p class="help-block">Max photo 1MB, Format (JPG, PNG).</p> <br/>
                                 <?php 
                                        if($show_data[0]['gambar_produk'] == "") {
                                    ?>
                                    <a class="fancybox" data-fancybox-group="gallery" href="<?= base_url() ?>assets/admin/img/not_available.jpg" >
                                    <img width="100" style="border:2px solid #000" src="<?=base_url()?>assets/admin/img/not_available.jpg"/>
                                    </a>
                                    <?php } else { ?>
                                    <a class="fancybox" data-fancybox-group="gallery"  title="<?=$show_data[0]['nm_produk']?>" href="<?=base_url()?>repository/product/<?=$show_data[0]['gambar_produk']?>" >
                                    <img width="100" style="border:2px solid #000" src="<?=base_url()?>repository/product/<?=$show_data[0]['gambar_produk']?>"/>
                                    </a>
                                    <?php } ?>
                            </div>
                            <div class="form-group">
                                <label>Diskon : </label>
                                <input type="text" name="product_diskon"  class="form-control" value="<?= $show_data[0]['diskon'] ?>" placeholder="Enter Diskon" required="required">
                            </div>
                            <div class="form-group">
                                <label>Harga Produk : </label>
                                <input type="text" name="product_price"  class="form-control" value="<?= $show_data[0]['harga'] ?>" placeholder="Enter Harga Produk" required="required">
                            </div>
                            <div class="form-group">
                                <label>Deskripsi Produk :</label><br/>
                                <textarea id="editor1" name="product_desc" rows="10" cols="80">
                                    <?= $show_data[0]['deskripsi'] ?>
                                </textarea>
                            </div>
                        </div><!-- /.box-body -->
                        <div class="box-footer">
                            <a href="<?= base_url() ?>backend/product" class="btn btn-default">Cancel</a>
                            <button class="btn btn-primary pull-right" type="submit">Edit Data</button>
                        </div>
                    </form>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
<?php } else if ($type == 'category_product') {
    ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Data Kategori Produk
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url() ?>backend"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li><a href="<?= base_url() ?>backend/category_product"><i class="fa fa-dashboard"></i>Data Kategori</a></li>
            <li class="active">Edit Kategori</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Edit Data Kategori </h3>
                    </div><!-- /.box-header -->
                    <form action="<?= base_url() ?>proccess_backend/processupdate/<?= $type ?>/<?= $id ?>" method="post" enctype="multipart/form-data" required="required" autocomplete="off">
                        <div class="box-body">
                            <div class="form-group">
                                <label>Nama Kategori Produk : </label>
                                <input type="text" name="category_product_name" value="<?= $show_data['nm_kategori'] ?>"  class="form-control" placeholder="Enter Nama Kategori" required="required">
                            </div>
                        </div><!-- /.box-body -->
                        <div class="box-footer">
                            <a href="<?= base_url() ?>backend/category_product" class="btn btn-default">Cancel</a>
                            <button class="btn btn-primary pull-right" type="submit">Edit Data</button>
                        </div>
                    </form>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
<?php } else if ($type == 'kurir') {
    ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Data Kurir
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url() ?>backend"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li><a href="<?= base_url() ?>backend/kurir"><i class="fa fa-dashboard"></i>Data Kurir</a></li>
            <li class="active">Edit Kurir</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Edit Data Kurir </h3>
                    </div><!-- /.box-header -->
                    <form action="<?= base_url() ?>proccess_backend/processupdate/<?= $type ?>/<?= $id ?>" method="post" enctype="multipart/form-data" required="required" autocomplete="off">
                        <div class="box-body">
                            <div class="form-group">
                                <label>Nama  : </label>
                                <input type="text" name="nama" value="<?= $show_data['nama_kurir'] ?>"  class="form-control" placeholder="Enter Nama Kurir" required="required">
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label>Alamat  : </label>
                                <input type="text" name="alamat" value="<?= $show_data['alamat_kurir'] ?>"  class="form-control" placeholder="Enter Alamat Kurir" required="required">
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label>No. Telp  : </label>
                                <input type="text" name="no_telp" value="<?= $show_data['no_telp_kurir'] ?>"  class="form-control" placeholder="Enter Telp Kurir" required="required">
                            </div>
                        </div>
                        <div class="box-footer">
                            <a href="<?= base_url() ?>backend/kurir" class="btn btn-default">Cancel</a>
                            <button class="btn btn-primary pull-right" type="submit">Edit Data</button>
                        </div>
                    </form>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
<?php } else if ($type == 'ongkir') {
    ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Data Ongkos Kirim
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url() ?>backend"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li><a href="<?= base_url() ?>backend/kurir"><i class="fa fa-dashboard"></i>Data Ongkos Kirim</a></li>
            <li class="active">Edit Ongkos Kirim</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Edit Data Ongkos Kirim </h3>
                    </div><!-- /.box-header -->
                    <form action="<?= base_url() ?>proccess_backend/processupdate/<?= $type ?>/<?= $id ?>" method="post" enctype="multipart/form-data" required="required" autocomplete="off">
                        <div class="box-body">
                            <div class="form-group">
                                <label>Nama Kota : </label>
                                <input type="text" name="nama" value="<?= $show_data['nama_kota'] ?>"  class="form-control" placeholder="Enter Nama Kota" required="required">
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label>Harga Ongkir  : </label>
                                <input type="text" name="harga" value="<?= $show_data['harga_ongkir'] ?>"  class="form-control" placeholder="Enter Harga Ongkir" required="required">
                            </div>
                        </div>
                        <div class="box-footer">
                            <a href="<?= base_url() ?>backend/ongkir" class="btn btn-default">Cancel</a>
                            <button class="btn btn-primary pull-right" type="submit">Edit Data</button>
                        </div>
                    </form>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
<?php } else if ($type == 'pengiriman') {
    ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Data Surat Jalan
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url() ?>backend"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li><a href="<?= base_url() ?>backend/pengiriman"><i class="fa fa-dashboard"></i>Data Surat Jalan</a></li>
            <li class="active">Edit Surat Jalan</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Edit Data Master Surat Jalan</h3>
                    </div><!-- /.box-header -->
                    <div class="content-error" style="margin:10px;"> 
                        <?php
                        if (!empty($_GET['info']) && $_GET['info'] == 'kodepesan-notfound') { ?>
                            <div class="alert alert-danger">
                                <strong>Sorry!</strong> Kode Pesan yang Anda masukan salah!
                            </div>
                        <?php } ?>
                    </div>
                    <form action="<?= base_url() ?>proccess_backend/processupdate/<?= $type ?>/<?= $show_data['kd_sj'] ?>" method="post" enctype="multipart/form-data" required="required" autocomplete="off">
                        <div class="box-body">
                            <div class="form-group">
                                <label>No. Surat Jalan : </label>
                                <input type="text" name="kd_sj"  class="form-control" value="<?= $show_data['kd_sj'] ?>" readonly placeholder="Enter Surat Jalan" required="required">
                            </div>
                            <div class="form-group">
                                <label>No. Pesanan : </label>
                                <input type="text" name="kd_pesanan" readonly  class="form-control" value="<?= $show_data['kd_pesanan'] ?>" placeholder="Enter Kode Pesanan" required="required">
                            </div>
                            <div class="box-body">
                            <div class="form-group">
                                <label>Nama Kurir : </label>
                                <select  name="nama_kurir"  class="form-control"  required="required">
                                        <option value="">-- Pilih Kurir --</option>
                                        <?php 
                                            foreach($kurir as $val) {
                                                if($kurir == $val['id_kurir']) {
                                                    $selected = 'selected';
                                                } else {
                                                    $selected = 'selected';
                                                }
                                        ?>
                                        <option value="<?=$val['id_kurir']?>" <?=$selected?>><?=$val['nama_kurir']?></option>
                                        <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label>Tanggal Kirim : </label>
                                <input type="text" name="tgl_kirim"  class="form-control datepicker" value="<?= $show_data['tgl_sj'] ?>" placeholder="Enter Tanggan Kirim" required="required">
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label>Status Kirim : </label>
                                <select  name="status_kirim"  class="form-control"  required="required">
                                <?php 
                                    if($show_data['status_kirim'] == 1) {
                                        $selected1 = 'selected';
                                        $selected2 = '';
                                    } else if($show_data['status_kirim'] == 2) { 
                                        $selected1 = '';
                                        $selected2 = 'selected';
                                    } else {
                                        $selected1 = '';
                                        $selected2 = '';
                                    }
                                ?>
                                        <option value="">-- Pilih Status --</option>
                                        <option value="1" <?=$selected1?>>Dalam Perjalanan</option>
                                        <option value="2" <?=$selected2?>>Sudah Selesai</option>
                                </select>
                            </div>
                        </div>
                        </div><!-- /.box-body -->
                        <div class="box-footer">
                            <a href="<?= base_url() ?>backend/pengiriman" class="btn btn-default">Cancel</a>
                            <button class="btn btn-primary pull-right" type="submit">Edit</button>
                        </div>
                    </form>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
<?php } else if ($type == 'pembayaran') {
    ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Data Pembayaran
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url() ?>backend"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li><a href="<?= base_url() ?>backend/pengiriman"><i class="fa fa-dashboard"></i>Data Pembayaran</a></li>
            <li class="active">Edit Pembayaran</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Edit Data Pembayaran</h3>
                    </div><!-- /.box-header -->
                    <form action="<?= base_url() ?>proccess_backend/processupdate/<?= $type ?>/<?= $show_data['kd_pembayaran'] ?>" method="post" enctype="multipart/form-data" required="required" autocomplete="off">
                        <div class="box-body">
                            <div class="form-group">
                                <label>Kode Pembayaran : </label>
                                <input type="text" name="kd_pembayaran"  class="form-control" value="<?= $show_data['kd_pembayaran'] ?>" readonly placeholder="Enter Kode Pembayaran" required="required">
                            </div>
                            <div class="form-group">
                                <label>Kode Pesanan : </label>
                                <input type="text" name="kd_pesanan"  class="form-control" value="<?= $show_data['kd_pesanan'] ?>" readonly placeholder="Enter Kode Pesanan" required="required">
                            </div>
                            <div class="form-group">
                                <label>Status Pembayaran : </label>
                                <select  name="status_pembayaran"  class="form-control" required="required">
                                    <option value="">-- Status Pembayaran -- </option>
                                    <?php 
                                        if($show_data['status_pembayaran'] == 1) {
                                            $selected2 = 'selected';
                                            $selected1 = '';
                                        } else if($show_data['status_pembayaran'] == 0) {
                                            $selected2 = '';
                                            $selected1 = 'selected';
                                        } else {
                                            $selected1 = '';
                                            $selected2 = '';
                                        }
                                    ?>
                                    <option value="0" <?=$selected1?>>Sedang di Proses</option>
                                    <option value="1" <?=$selected2?>>Lunas</option>
                                </select>
                            </div>
                        </div><!-- /.box-body -->
                        <div class="box-footer">
                            <a href="<?= base_url() ?>backend/user_konfirmasi_pembayaran" class="btn btn-default">Cancel</a>
                            <button class="btn btn-primary pull-right" type="submit">Edit</button>
                        </div>
                    </form>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
<?php } else if ($type == 'staff') {
    ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Data Staff
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url() ?>backend"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li><a href="<?= base_url() ?>backend/staff"><i class="fa fa-dashboard"></i>Data Staff</a></li>
            <li class="active">Edit Staff</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Edit Data Master Staff</h3>
                    </div><!-- /.box-header -->
                    <form action="<?= base_url() ?>proccess_backend/processupdate/staff/<?= $show_data[0]['id_staff'] ?>" method="post" enctype="multipart/form-data" required="required">
                        <div class="box-body">
                            <div class="form-group">
                                <label>Hak Akses</label>
                                <select name="id_privilege" required="required" class="select2 form-control" style="width: 100%;">
                                    <option value="">-- Pilih Hak Akses --</option>
                                    <?php
                                    foreach ($show_hak_akses as $val_hak_akses) {
                                        if ($show_data[0]['id_privilege'] == $val_hak_akses['id_privilege']) {
                                            $selected = 'selected';
                                        } else {
                                            $selected = "";
                                        }
                                        ?>
                                        <option value="<?= $val_hak_akses['id_privilege'] ?>" <?= $selected ?>><?= $val_hak_akses['privilege_type'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Fullname : </label>
                                <input type="text" name="staff_fullname"  class="form-control" value="<?= $show_data[0]['staff_fullname'] ?>" placeholder="Enter Fullname" required="required">
                            </div>

                            <div class="form-group">
                                <label for="exampleInputFile">Photo</label>
                                <input type="file" name="photo" id="exampleInputFile">
                                <p class="help-block">Max photo 1MB, Format (JPG, PNG).</p>
                            </div>
                            <div class="form-group">
                                <label>Email : </label>
                                <input type="text" name="staff_email"  class="form-control" value="<?= $show_data[0]['staff_email'] ?>" placeholder="Enter Email" required="required">
                            </div>
                            <div class="form-group">
                                <label>Phone : </label>
                                <input type="text" name="staff_phone"  class="form-control" value="<?= $show_data[0]['staff_phone'] ?>" placeholder="Enter Phone" required="required">
                            </div>
                            <div class="form-group">
                                <label>Address : </label>
                                <input type="text" name="staff_address"  class="form-control" value="<?= $show_data[0]['staff_address'] ?>" placeholder="Enter Adress" required="required">
                            </div>
                            <div class="form-group">
                                <label>
                                    Gender :
                                </label>
                                <?php
                                if ($show_data[0]['staff_gender'] == "L") {
                                    $checked1 = 'checked';
                                    $checked2 = '';
                                } else if ($show_data[0]['staff_gender'] == "P") {
                                    $checked1 = '';
                                    $checked2 = 'checked';
                                }
                                ?>
                                <label>
                                    <input type="radio" name="staff_gender" <?= $checked1 ?> class="minimal" value="L"> &nbsp; L
                                </label>
                                <label>
                                    <input type="radio" name="staff_gender" <?= $checked2 ?> class="minimal" value="P"> &nbsp; P
                                </label>

                            </div>
                        </div><!-- /.box-body -->
                        <div class="box-footer">
                            <a href="<?= base_url() ?>backend/staff" class="btn btn-default">Cancel</a>
                            <button class="btn btn-primary pull-right" type="submit">Edit Data</button>
                        </div>
                    </form>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
<?php } else if ($type == 'hak_akses') {
    ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Data Hak Akses
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url() ?>backend"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li><a href="<?= base_url() ?>backend/vendor"><i class="fa fa-dashboard"></i>Data Hak Akses</a></li>
            <li class="active">Edit Hak Akses</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Edit Data Master Hak Akses</h3>
                    </div><!-- /.box-header -->
                    <form action="<?= base_url() ?>proccess_backend/processupdate/<?= $type ?>/<?= $id ?>" method="post" enctype="multipart/form-data" required="required">
                        <div class="box-body">
                            <div class="form-group">
                                <label>Nama Vendor : </label>
                                <input type="text" name="vendor_name" value="<?= $show_data['vendor_name'] ?>" class="form-control" placeholder="Enter Nama Vendor" required="required">
                            </div>
                        </div><!-- /.box-body -->
                        <div class="box-footer">
                            <a href="<?= base_url() ?>backend/vendor" class="btn btn-default">Cancel</a>
                            <button class="btn btn-primary pull-right" type="submit">Edit Data</button>
                        </div>
                    </form>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
<?php } else if ($type == 'settings_banner') {
    ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Data Konfigurasi Banner
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url() ?>backend"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li><a href="<?= base_url() ?>backend/galery"><i class="fa fa-dashboard"></i>Data Konfigurasi Banner</a></li>
            <li class="active">Edit Konfigurasi Banner</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Edit Data Master Konfigurasi Banner</h3>
                    </div><!-- /.box-header -->
                    <form action="<?= base_url() ?>proccess_backend/processupdate/<?= $type ?>/<?= $id ?>" method="post" enctype="multipart/form-data" required="required">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleInputFile">Gambar Banner</label>
                                <input type="file" name="photo" id="exampleInputFile">
                                <p class="help-block">Max photo 1MB, Format (JPG, PNG).</p>
                            </div>
                            <div class="form-group">
                                <label>Judul Banner : </label>
                                <input type="text" name="banner_title" value="<?= $show_data[0]['banner_title'] ?>"  class="form-control" placeholder="Enter Judul Banner" required="required">
                            </div>
                            <div class="form-group">
                                <label>Deskripsi Banner : </label>
                                <input type="text" name="banner_desc" value="<?= $show_data[0]['banner_desc'] ?>"  class="form-control" placeholder="Enter Description Banner" required="required">
                            </div>
                        </div><!-- /.box-body -->
                        <div class="box-footer">
                            <a href="<?= base_url() ?>backend/settings_banner" class="btn btn-default">Cancel</a>
                            <button class="btn btn-primary pull-right" type="submit">Edit</button>
                        </div>
                    </form>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
<?php }  ?>
