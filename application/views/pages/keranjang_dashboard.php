<section>
    <div class="content-cart">
        <div id="content">

            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h3 class="title">MEMBER AREA<i class="fa fa-caret-right"></i><span>PESANAN BELANJA</span></h3>
                        <div class="welcome">
                            <span class="hello">Halo, <?= $this->session->userdata('fullname'); ?><i></i></span>
                            <span class="userid"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!-- left form -->
                    <div class="col-xs-2 col-sm-2 col-md-3 ">
                        <?php require_once($sidebar_dashboard . '.php'); ?>
                    </div>
                    <div class="col-xs-10  col-sm-10 col-md-9 content-wrap">
                        <!-- Profil -->
                        <div class="profil">

                            <h3 class="profil-title">PESANAN SAYA</h3><br/>

                            <div class="profil-wrap">
                                <div class="table-responsive">          
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <!-- <th>Opsi</th> -->
                                                <th>No. Order</th>
                                                <th>Tanggal Belanja</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $no = 1;
                                            foreach ($pesanan_saya as $val) {
                                                ?>
                                                <tr>
                                                    <td><?= $no ?></td>
                                                    <!-- <td style="text-align:center;">     
                                                        <?php if($val['status_pesanan'] == 4) {  ?>                   
                                                            <a href="<?=base_url()?>pdf/getcetak/invoice/<?=$val['kd_pesanan']?>">
                                                                <span title="Cetak Invoice" aria-hidden="true" class="glyphicon glyphicon-print"></span>  &nbsp;&nbsp;&nbsp;
                                                            </a>
                                                        <?php } ?>
                                                    </td> -->
                                                    <td>
                                                        <a href="<?= base_url() ?>dashboard/keranjang_detail/<?= $val['kd_pesanan'] ?>">
                                                            <?= $val['kd_pesanan'] ?>
                                                        </a>
                                                    </td>
                                                    <td>
                                                        <?= formatDate($val['tgl_pesanan']) ?>
                                                    </td>
                                                    <td>
                                                        <?= status_pesanan($val['status_pesanan']) ?>
                                                    </td>
                                                </tr>
                                                <?php $no++;
                                            } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>						</div>
                </div>

            </div>
        </div>
    </div>    
</section>
