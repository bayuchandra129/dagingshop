<div class="page-banner no-subtitle" style="padding:40px 0; background: url(images/slide-02-bg.jpg) center #f9f9f9;">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h2>Testimonial</h2>
                <p>We Are Professional</p>
            </div>
            <div class="col-md-6">
                <ul class="breadcrumbs">
                    <li><a href="<?= base_url() ?>">Home</a></li>
                    <li>Testimonial</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- End Page Banner -->
<br/><br/>
<!-- Start Content -->
<div id="content">
    <div class="container">
        <div class="row sidebar-page">

            <!-- Page Content -->
            <div class="col-md-8 page-content">

                <div class="tabs-section">

                    <!-- Nav Tabs -->
                    <ul class="nav nav-tabs" data-tabs="tabs">
                        <li class="active"><a href="#tab-1" data-toggle="tab"><i class="fa fa-desktop"></i>ALL</a></li>
                        <li><a href="#tab-2" data-toggle="tab"><i class="fa fa-leaf"></i>Product</a></li>
                        <li><a href="#tab-3" data-toggle="tab"><i class="fa fa-rocket"></i>Package Service</a></li>
                    </ul>

                    <!-- Tab panels -->
                    <div class="tab-content">
                        <!-- Tab Content 1 -->
                        <div class="tab-pane fade in active" id="tab-1"><br/>
                            <?php
                            foreach ($show_portofolio as $val) {
                                ?>
                                <div class="content">
                                    <div class="col-md-4">
                                        <center>
                                            <?php
                                            if ($val['user_photo'] == "") {
                                                ?>
                                            <a class="lightbox" title="<?=$val['user_fullname']?>" href="<?= base_url() ?>assets/images/member-01.jpg" data-lightbox-gallery="gallery2">
                                                <img class="img-circle" src="<?= base_url() ?>assets/images/member-01.jpg"/>
                                            </a>
                                            <?php } else { ?>
                                            <a class="lightbox" title="<?=$val['testimonial_message']?>" href="<?= base_url() ?>repository/user_profile/<?= $val['user_photo'] ?>" data-lightbox-gallery="gallery2">
                                            <img class="img-circle" src="<?= base_url() ?>repository/user_profile/<?= $val['user_photo'] ?>"/>   
                                            </a>
                                            <?php } ?>
                                                </center><br/>
                                            </div>
                                            <div class="col-md-8">
                                                <p>
                                                    <span><b><?= formatDate($val['tgl_add_testimonial']) ?></b></span><br/>    
                                                    "<?= $val['testimonial_message'] ?>"    
                                                </p>
                                                <p>
                                                    <span><?= $val['product_name'] ?></span> 
                                                    <strong class="accent-color pull-right">- <?= $val['user_fullname'] ?> -</strong> </p>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <br/><br/>
                                    <?php } ?>
                                </div><br/>
                                <!-- Tab Content 2 -->
                                <div class="tab-pane fade" id="tab-2">
                                    <?php
                                    foreach ($show_portofolio_product as $val) {
                                        ?>
                                        <div class="content">
                                            <div class="col-md-4">
                                                <center>
                                                    <img class="img-circle" src="<?= base_url() ?>assets/images/member-01.jpg"/>
                                                </center><br/>
                                            </div>
                                            <div class="col-md-8">
                                                <p>
                                                    <span><b><?= formatDate($val['tgl_add_testimonial']) ?></b></span><br/>    
                                                    "<?= $val['testimonial_message'] ?>"    
                                                </p>
                                                <p> <span><?= $val['product_name'] ?></span> 
                                                    <strong class="accent-color pull-right"> - <?= $val['user_fullname'] ?> -</strong> </p>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <br/><br/>
                                    <?php } ?>
                                </div>
                                <!-- Tab Content 3 -->
                                <div class="tab-pane fade" id="tab-3">
                                    <?php
                                    foreach ($show_portofolio_package_service as $val_ps) {
                                        ?>
                                        <div class="content">
                                            <div class="col-md-4">
                                                <center>

                                                    <img class="img-circle" src="<?= base_url() ?>assets/images/member-01.jpg"/>
                                                </center><br/>
                                            </div>
                                            <div class="col-md-8">
                                                <p>
                                                    <span><b><?= formatDate($val['tgl_add_testimonial']) ?></b></span><br/>    
                                                    "<?= $val_ps['testimonial_message'] ?>"    
                                                </p>
                                                <p><strong class="accent-color">- <?= $val_ps['user_fullname'] ?> -</strong> </p>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <br/><br/>
                                    <?php } ?>
                                </div>
                            </div>
                            <!-- End Tab Panels -->

                        </div>

                        <!-- Divider -->
                        <div class="hr5" style="margin-top:45px;margin-bottom:45px;"></div>

                        <br/>

                    </div>
                    <!-- End Page Content -->


                    <!--Sidebar-->
                    <div class="col-md-4 sidebar right-sidebar">

                        <!-- Popular Posts widget -->
                        <div class="widget widget-popular-posts">
                            <h4>News Product <span class="head-line"></span></h4>
                            <ul>
                                <?php
                                foreach ($product_new as $val) {
                                    ?>
                                    <li>
                                        <div class="widget-thumb">
                                            <?php
                                            if ($val['product_pict'] == "") {
                                                ?>
                                                <a class="lightbox" title="<?= $val['product_name'] ?>" href="<?= base_url() ?>assets/admin/img/not_available.jpg" data-lightbox-gallery="gallery2">
                                                    <img class="img-thumbnail img-recent-portofolio" alt="" src="<?= base_url() ?>assets/admin/img/not_available.jpg"/>
                                                </a>
                                            <?php } else { ?>
                                                <a class="lightbox" title="<?= $val['product_name'] ?>" href="<?= base_url() . "repository/product/" . $val['product_pict']; ?>" data-lightbox-gallery="gallery2">
                                                    <img class="img-thumbnail img-recent-portofolio" alt="" src="<?= base_url() ?>repository/product/<?= $val['product_pict'] ?>" />
                                                </a>
                                            <?php } ?>
                                        </div>
                                        <div class="widget-content" >
                                            <h5 style="color:#333;font-weight: bold;"><a href="#"><?= $val['product_name'] ?></a></h5>
                                            <span style="color:#333;">Rp. <?= rupiah($val['product_price']) ?></span><br/>
                                            <span style="color:#333;"><?= strip_tags(potong_kata($val['product_desc'], 0, 50)); ?></span><br/>
                                            <a href="<?= base_url() ?>product/detail/<?= $val['id_product'] ?>"><strong class="accent-color">Read More</strong></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </li>
                                <?php } ?>
                    </ul>
                </div>
                <!--End sidebar-->
            </div>
        </div>
    </div>
    <!-- End Content -->
