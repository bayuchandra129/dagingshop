-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 16, 2022 at 06:15 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.2.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dagingshop`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL,
  `nm_admin` varchar(30) NOT NULL,
  `email_admin` varchar(30) NOT NULL,
  `pass_admin` varchar(50) NOT NULL,
  `level` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `nm_admin`, `email_admin`, `pass_admin`, `level`) VALUES
(1, '630200436.jpg', 'admin@gmail.com', '90b9aa7e25f80cf4f64e990b78a9fc5ebd6cecad', 1),
(2, '.png', 'owner@gmail.com', '082213192276', 2);

-- --------------------------------------------------------

--
-- Table structure for table `detil_pesanan`
--

CREATE TABLE `detil_pesanan` (
  `detil_pesanan_id` int(11) NOT NULL,
  `kd_pesanan` varchar(20) NOT NULL,
  `kd_produk` varchar(20) NOT NULL,
  `qty` int(11) NOT NULL,
  `harga_jual` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detil_pesanan`
--

INSERT INTO `detil_pesanan` (`detil_pesanan_id`, `kd_pesanan`, `kd_produk`, `qty`, `harga_jual`) VALUES
(1, 'NO-76249-AL', 'PAL-CORG-0006', 4, 169000),
(2, 'NO-76249-AL', 'PAL-XBMP-0005', 3, 4477000),
(3, 'NO-76249-AL', 'PAL-ZECF-0004', 1, 230000),
(4, 'NO-17285-AL', 'PAL-XBMP-0005', 5, 4477000),
(5, 'NO-14913-AL', 'PAL-KING-0018', 1, 259900);

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `kd_kategori` int(11) NOT NULL,
  `nm_kategori` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`kd_kategori`, `nm_kategori`) VALUES
(1, 'Daging Super'),
(2, 'Daging Premium'),
(3, 'Daging Supreme');

-- --------------------------------------------------------

--
-- Table structure for table `kurir`
--

CREATE TABLE `kurir` (
  `id_kurir` int(11) NOT NULL,
  `nama_kurir` varchar(50) NOT NULL,
  `alamat_kurir` text NOT NULL,
  `no_telp_kurir` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kurir`
--

INSERT INTO `kurir` (`id_kurir`, `nama_kurir`, `alamat_kurir`, `no_telp_kurir`) VALUES
(1, 'CANDRA', 'KEBON BESAR TANGERANG', '08936472813'),
(2, 'ROMI', 'BATU CEPER', '0896773546472'),
(3, 'FAZRI', 'KEBON NANAS', '081264748595'),
(4, 'AHMAD', 'PASAR KEMIS', '085726278192'),
(5, 'JOJO', 'THAMRIN SEGNEG', '08135272811221');

-- --------------------------------------------------------

--
-- Table structure for table `ongkir`
--

CREATE TABLE `ongkir` (
  `id_ongkir` int(11) NOT NULL,
  `nama_kota` varchar(20) NOT NULL,
  `harga_ongkir` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ongkir`
--

INSERT INTO `ongkir` (`id_ongkir`, `nama_kota`, `harga_ongkir`) VALUES
(1, 'JAKARTA', 20000),
(2, 'BOGOR', 33000),
(3, 'DEPOK', 35000),
(4, 'TANGERANG', 30000),
(5, 'BEKASI', 25000);

-- --------------------------------------------------------

--
-- Table structure for table `pelanggan`
--

CREATE TABLE `pelanggan` (
  `id_plg` int(11) NOT NULL,
  `pass_plg` varchar(50) NOT NULL,
  `nm_plg` varchar(30) NOT NULL,
  `photo_plg` varchar(100) NOT NULL,
  `email_plg` varchar(50) NOT NULL,
  `tlp_plg` varchar(20) NOT NULL,
  `alamat` text NOT NULL,
  `kota` varchar(15) NOT NULL,
  `kode_pos` varchar(10) NOT NULL,
  `date_register` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pelanggan`
--

INSERT INTO `pelanggan` (`id_plg`, `pass_plg`, `nm_plg`, `photo_plg`, `email_plg`, `tlp_plg`, `alamat`, `kota`, `kode_pos`, `date_register`) VALUES
(1, 'fe703d258c7ef5f50b71e06565a65aa07194907f', 'ELISA NENDES', '', 'elisanendes@gmail.com', '082973362', 'jl. abadi kebon besar 33', 'Banten', '15122', '2019-01-16 21:32:20'),
(2, 'fe703d258c7ef5f50b71e06565a65aa07194907f', 'pelanggan baru', '', 'pelangganbaru@gmail.com', '082213192276', 'Jl. H. Kasam No. 91', 'DKI Jakarta', '11650', '2019-01-20 10:45:03'),
(3, 'fe703d258c7ef5f50b71e06565a65aa07194907f', 'bayu', '', 'bayu@gmail.com', '082213192276', 'jl sudirman', 'DKI Jakarta', '11650', '2021-12-29 15:05:50');

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran`
--

CREATE TABLE `pembayaran` (
  `kd_pembayaran` varchar(20) NOT NULL,
  `kd_pesanan` varchar(20) NOT NULL,
  `tgl_pembayaran` date NOT NULL,
  `nm_pembank` varchar(50) NOT NULL,
  `no_rek` int(11) NOT NULL,
  `nm_bank` varchar(20) NOT NULL,
  `total_bayar` int(11) NOT NULL,
  `foto_bukti` varchar(100) NOT NULL,
  `status_pembayaran` tinyint(4) NOT NULL,
  `pem_date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembayaran`
--

INSERT INTO `pembayaran` (`kd_pembayaran`, `kd_pesanan`, `tgl_pembayaran`, `nm_pembank`, `no_rek`, `nm_bank`, `total_bayar`, `foto_bukti`, `status_pembayaran`, `pem_date_add`) VALUES
('PEMAL-CKZT-0002', 'NO-76249-AL', '2019-01-24', 'BCA', 123123, 'BCA', 123121231, '9518.jpg', 0, '2019-01-20 10:59:08'),
('PEMAL-HBFI-0003', 'NO-14913-AL', '2021-12-29', 'Bayu', 2147483647, 'BCA', 279900, '6256.jpeg', 1, '2021-12-29 15:07:52'),
('PEMAL-XUZU-0001', 'NO-17285-AL', '2019-01-24', 'BNI', 2131231, 'BCA', 11111, '2450.jpg', 0, '2019-01-20 10:58:50');

-- --------------------------------------------------------

--
-- Table structure for table `pesanan`
--

CREATE TABLE `pesanan` (
  `kd_pesanan` varchar(20) NOT NULL,
  `kd_plg` int(11) NOT NULL,
  `ongkir_id` int(11) NOT NULL,
  `alamat_kirim` text NOT NULL,
  `catatan` text NOT NULL,
  `status_pesanan` tinyint(4) NOT NULL COMMENT '0 = cancel, 1 = pending, 2 = proccessing, 3 =  delivery, 4 = complete',
  `tgl_pesanan` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pesanan`
--

INSERT INTO `pesanan` (`kd_pesanan`, `kd_plg`, `ongkir_id`, `alamat_kirim`, `catatan`, `status_pesanan`, `tgl_pesanan`) VALUES
('NO-14913-AL', 3, 1, 'jl. sudirman', 'packing yang rapih', 4, '2021-12-29 15:05:51'),
('NO-17285-AL', 1, 5, 'Kemabangan', 'Kembangan', 1, '2019-01-20 08:47:23'),
('NO-76249-AL', 1, 3, 'margocity no 13', '', 4, '2019-01-16 21:34:46');

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `kd_produk` varchar(20) NOT NULL,
  `kd_kategori` int(11) NOT NULL,
  `nm_produk` varchar(30) NOT NULL,
  `stok` int(11) NOT NULL,
  `gambar_produk` varchar(50) NOT NULL,
  `diskon` int(20) NOT NULL,
  `harga` bigint(25) NOT NULL,
  `gram` int(11) NOT NULL,
  `deskripsi` text NOT NULL,
  `date_add` datetime NOT NULL,
  `date_edit` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`kd_produk`, `kd_kategori`, `nm_produk`, `stok`, `gambar_produk`, `diskon`, `harga`, `gram`, `deskripsi`, `date_add`, `date_edit`) VALUES
('PAL-AJNN-0020', 3, 'SOUTHLAND DARLING TENDERLOIN', 1000, '166.jpeg', 0, 279900, 500, '<p>-</p>\r\n', '2021-12-26 13:33:47', '0000-00-00 00:00:00'),
('PAL-BEHS-0012', 2, 'JAPAN BEEF OMI HIME SHABU SLIC', 10000, '9468.jpeg', 0, 179900, 200, '<p>-</p>\r\n', '2021-12-26 13:29:06', '0000-00-00 00:00:00'),
('PAL-BFGA-0005', 1, 'AUSTRALIAN GRASSFED SHORT RIB ', 10000, '680.jpeg', 0, 91000, 500, '<p>-</p>\r\n', '2021-12-26 13:24:30', '0000-00-00 00:00:00'),
('PAL-BOEW-0013', 2, 'BLACK ANGUS SHORT RIBS BONE-IN', 10000, '5936.jpeg', 0, 199900, 200, '<p>-</p>\r\n', '2021-12-26 13:29:41', '0000-00-00 00:00:00'),
('PAL-CNKV-0017', 3, 'SOUTHLAND DARLING CHUCK EYE RO', 10000, '4396.jpeg', 0, 289900, 500, '<p>-</p>\r\n', '2021-12-26 13:32:17', '0000-00-00 00:00:00'),
('PAL-FRGC-0002', 1, 'AUSSIE TONGUE', 10000, '7861.jpeg', 0, 93500, 300, '<p>-</p>\r\n', '2021-12-26 11:24:54', '0000-00-00 00:00:00'),
('PAL-HPSF-0011', 2, 'BLACK ANGUS RUMP PREMIUM 200+ ', 10000, '2715.jpeg', 0, 367900, 200, '<p>-</p>\r\n', '2021-12-26 13:28:37', '0000-00-00 00:00:00'),
('PAL-KING-0018', 3, 'SOUTHLAND DARLING RIBEYE', 9999, '6127.jpeg', 0, 259900, 500, '<p>-</p>\r\n', '2021-12-26 13:32:45', '0000-00-00 00:00:00'),
('PAL-MHAR-0022', 3, 'WAGYU 600+ DAYS JAPANESE OMI H', 10000, '746.jpeg', 0, 219900, 500, '<p>-</p>\r\n', '2021-12-26 13:34:39', '0000-00-00 00:00:00'),
('PAL-NDRI-0004', 1, 'LAMB RACK', 10000, '5180.jpeg', 0, 124800, 500, '<p>-</p>\r\n', '2021-12-26 13:23:49', '0000-00-00 00:00:00'),
('PAL-PCQD-0021', 3, 'WAGYU (OYSTER BLADE) 600+ DAYS', 10000, '164.jpeg', 0, 269900, 500, '<p>-</p>\r\n', '2021-12-26 13:34:14', '0000-00-00 00:00:00'),
('PAL-RBNV-0006', 1, 'AUSTRALIAN GRASSFED STRIPLOIN', 10000, '4506.jpeg', 0, 245900, 500, '', '2021-12-26 13:25:08', '0000-00-00 00:00:00'),
('PAL-TADE-0010', 2, 'BLACK ANGUS SHORT RIBS BONE-IN', 10000, '9516.jpeg', 0, 159900, 200, '', '2021-12-26 13:27:45', '0000-00-00 00:00:00'),
('PAL-TJRU-0007', 1, 'AUSTRALIAN GRASSFED TENDERLOIN', 10000, '9287.jpeg', 0, 199900, 200, '<p>-</p>\r\n', '2021-12-26 13:25:38', '0000-00-00 00:00:00'),
('PAL-VFAJ-0016', 3, 'SHER WAGYU CHUCK TAIL FLAP MB ', 10000, '7719.jpeg', 0, 189900, 500, '<p>-</p>\r\n', '2021-12-26 13:31:48', '0000-00-00 00:00:00'),
('PAL-VJHA-0015', 2, 'BLACK ANGUS TENDERLOIN PREMIUM', 10000, '7402.jpeg', 0, 279900, 200, '<p>-</p>\r\n', '2021-12-26 13:31:16', '0000-00-00 00:00:00'),
('PAL-VOWZ-0001', 1, 'AUSTRALIAN GRASSFED OXTAIL END', 10000, '3171.jpeg', 0, 239900, 500, '<p>-</p>\r\n', '2021-12-26 10:15:26', '2021-12-26 11:12:30'),
('PAL-VZBB-0014', 2, 'BLACK ANGUS STRIPLOIN PREMIUM ', 10000, '4268.jpeg', 0, 297900, 200, '<p>-</p>\r\n', '2021-12-26 13:30:19', '0000-00-00 00:00:00'),
('PAL-WHQV-0009', 2, 'BLACK ANGUS RIBEYE PREMIUM 200', 10000, '4588.jpeg', 0, 235900, 0, '<p>-</p>\r\n', '2021-12-26 13:27:15', '0000-00-00 00:00:00'),
('PAL-WYLX-0023', 3, 'WAGYU RAN ICHI (D RUMP) 600+ D', 10000, '6819.jpeg', 0, 339900, 500, '<p>-</p>\r\n', '2021-12-26 13:35:04', '0000-00-00 00:00:00'),
('PAL-ZDRN-0008', 2, 'BLACK ANGUS RIBEYE PREMIUM 150', 10000, '8541.jpeg', 0, 299900, 200, '<p>-</p>\r\n', '2021-12-26 13:26:36', '0000-00-00 00:00:00'),
('PAL-ZHIH-0019', 3, 'SOUTHLAND DARLING STRIPLOIN', 10000, '6784.jpeg', 0, 219900, 500, '<p>-</p>\r\n', '2021-12-26 13:33:17', '0000-00-00 00:00:00'),
('PAL-ZNQO-0003', 1, 'AUSTRALIAN GRASSFED RIBEYE', 10000, '7947.jpeg', 0, 400000, 200, '<p>-</p>\r\n', '2021-12-26 13:23:08', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `settings_banner`
--

CREATE TABLE `settings_banner` (
  `id_banner` int(11) NOT NULL,
  `id_staff` int(11) NOT NULL,
  `banner_pict` varchar(50) NOT NULL,
  `banner_title` varchar(50) NOT NULL,
  `banner_desc` varchar(100) NOT NULL,
  `date_add` datetime NOT NULL,
  `date_edit` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings_banner`
--

INSERT INTO `settings_banner` (`id_banner`, `id_staff`, `banner_pict`, `banner_title`, `banner_desc`, `date_add`, `date_edit`) VALUES
(1, 1, 'banner1.jpg', '  ', '  ', '2015-12-18 12:41:56', '2016-01-05 12:08:58'),
(2, 1, 'banner2.jpg', '', '', '2015-12-18 12:53:26', '2015-12-24 19:53:42'),
(3, 1, 'banner3.jpg', '', '', '2015-12-18 12:53:56', '2015-12-24 19:54:38');

-- --------------------------------------------------------

--
-- Table structure for table `surat_jalan`
--

CREATE TABLE `surat_jalan` (
  `kd_sj` varchar(20) NOT NULL,
  `kd_pesanan` varchar(20) NOT NULL,
  `kurir_id` int(11) NOT NULL,
  `tgl_sj` date NOT NULL,
  `status_kirim` tinyint(4) NOT NULL COMMENT '1 = Dalam Perjalanan, 2 = Sudah Selesai',
  `sj_date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `surat_jalan`
--

INSERT INTO `surat_jalan` (`kd_sj`, `kd_pesanan`, `kurir_id`, `tgl_sj`, `status_kirim`, `sj_date_add`) VALUES
('SJAL-IHKX-0001', 'NO-17285-AL', 3, '2019-01-24', 1, '2019-01-20 11:17:28'),
('SJAL-PXYQ-0002', 'NO-76249-AL', 1, '2019-01-17', 1, '2019-01-20 11:18:59'),
('SJAL-XSSL-0003', 'NO-14913-AL', 2, '2021-12-29', 2, '2021-12-29 15:09:02');

-- --------------------------------------------------------

--
-- Table structure for table `ulasan`
--

CREATE TABLE `ulasan` (
  `id` int(11) NOT NULL,
  `kd_pesanan` varchar(50) NOT NULL,
  `id_plg` int(11) NOT NULL,
  `ulasan_isi` text NOT NULL,
  `ulasan_tgl` datetime NOT NULL,
  `ulasan_status` tinyint(4) NOT NULL COMMENT '0 = tidak aktif, 1 = aktif'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `detil_pesanan`
--
ALTER TABLE `detil_pesanan`
  ADD PRIMARY KEY (`detil_pesanan_id`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`kd_kategori`);

--
-- Indexes for table `kurir`
--
ALTER TABLE `kurir`
  ADD PRIMARY KEY (`id_kurir`);

--
-- Indexes for table `ongkir`
--
ALTER TABLE `ongkir`
  ADD PRIMARY KEY (`id_ongkir`);

--
-- Indexes for table `pelanggan`
--
ALTER TABLE `pelanggan`
  ADD PRIMARY KEY (`id_plg`);

--
-- Indexes for table `pembayaran`
--
ALTER TABLE `pembayaran`
  ADD PRIMARY KEY (`kd_pembayaran`);

--
-- Indexes for table `pesanan`
--
ALTER TABLE `pesanan`
  ADD PRIMARY KEY (`kd_pesanan`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`kd_produk`);

--
-- Indexes for table `settings_banner`
--
ALTER TABLE `settings_banner`
  ADD PRIMARY KEY (`id_banner`);

--
-- Indexes for table `ulasan`
--
ALTER TABLE `ulasan`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `detil_pesanan`
--
ALTER TABLE `detil_pesanan`
  MODIFY `detil_pesanan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `kd_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `kurir`
--
ALTER TABLE `kurir`
  MODIFY `id_kurir` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `ongkir`
--
ALTER TABLE `ongkir`
  MODIFY `id_ongkir` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `pelanggan`
--
ALTER TABLE `pelanggan`
  MODIFY `id_plg` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `settings_banner`
--
ALTER TABLE `settings_banner`
  MODIFY `id_banner` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `ulasan`
--
ALTER TABLE `ulasan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
