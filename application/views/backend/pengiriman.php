<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Data Surat Jalan
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?=base_url()?>backend"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li class="active">Data Surat Jalan</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Data  Pengiriman</h3><br/><br/>
                    <div class="col-md-2">
                        <a href="<?=base_url()?>backend/add/pengiriman" class="btn btn-block btn-primary">Tambah Surat Jalan</a>
                    </div>
                </div><!-- /.box-header -->
                
                <div class="box-body">
                     <table id="example1" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th class="text-center">Opsi</th>
                                <th class="text-center">No. Surat Jalan</th>
                                <th class="text-center">No. Pesanan</th>
                                <th class="text-center">Nama Kurir</th>
                                <th class="text-center">Kota</th>
                                <th class="text-center">Alamat Kirim</th>
                                <th class="text-center">Tanggal Surat Jalan</th>
                                <th class="text-center">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            $no = 1;
                                foreach($show_data as $val) {
                            ?>
                            <tr>
                                <td>
                                    <?=$no;?>
                                </td>
                                <td class="text-center">
                                    <a href="<?=base_url()?>backend/view/pengiriman/<?=$val['kd_sj']?>">
                                        <span title="View" aria-hidden="true" class="glyphicon glyphicon-eye-open"></span> &nbsp;&nbsp;&nbsp;
                                    </a>
                                    <a href="<?=base_url()?>backend/edit/pengiriman/<?=$val['kd_sj']?>">
                                        <span title="Edit" aria-hidden="true" class="glyphicon glyphicon-edit"></span>  &nbsp;&nbsp;&nbsp;
                                    </a>
                                    <a href="<?=base_url()?>pdf/getcetak/surat_jalan/<?=$val['kd_sj']?>">
                                        <span title="Cetak" aria-hidden="true" class="glyphicon glyphicon-print"></span>  &nbsp;&nbsp;&nbsp;
                                    </a>
                                    <a onclick="return confirm('Apakah anda yakin ingin menghapus data ini ? ')" href="<?=base_url()?>proccess_backend/processremove/pengiriman/<?=$val['kd_sj']?>">
                                        <span title="Remove" aria-hidden="true" class="fa fa-trash"></span> 
                                    </a>
                                </td>
                                <td style="text-align:center;">                        
                                    <?=$val['kd_sj']?>
                                </td>
                                <td style="text-align:center;">                        
                                    <a href="<?=base_url()?>backend/edit/user_order/<?=$val['kd_pesanan']?>"><?=$val['kd_pesanan']?></a>
                                </td>
                                <td class="text-center">
                                    <?=$val['nama_kurir']?>
                                </td>
                                <td class="text-center">
                                    <?=$val['nama_kota']?>
                                </td>
                                <td class="text-center">
                                    <?=$val['alamat_kirim']?>
                                </td>
                                 <td class="text-center">
                                    <?=formatDates($val['tgl_sj'])?>
                                </td>
                                <td class="text-center">
                                    <?=status_surat_jalan($val['status_kirim'])?>
                                </td>
                            </tr>
                            <?php $no++; } ?>
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
