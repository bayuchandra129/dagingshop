<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

Class Proccess_backend extends CI_Controller {

    var $original_path;
    var $resized_path;
    var $thumbs_path;

    public function __construct() {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
        $this->original_path = base_url() . 'repository/product/original';
        $this->resized_path = base_url() . 'repository/product/resized';
        $this->thumbs_path = base_url() . 'repository/product/thumbs';
    }

    public function processadd($type = null) {
        $date_add = date('Y-m-d H:i:s');
        $date_edit = date('Y-m-d H:i:s');
        $id_staff = $this->session->userdata('id_staff');

        if ($type == 'product') {
            $product_kd = $this->core_admin->kodeproduk();
            $product_category = $this->input->post('product_category');
            $product_name = $this->input->post('product_name');
            $product_qty = $this->input->post('product_qty');
            $product_diskon = $this->input->post('product_diskon');
            $product_price = $this->input->post('product_price');
            $product_desc = $this->input->post('product_desc');



            if (isset($_FILES["photo"]) && $_FILES["photo"]["error"] == UPLOAD_ERR_OK) {
                $UploadDirectory = './repository/product/';

                //Is file size is less than allowed size.
                if ($_FILES["photo"]["size"] > 1042880) {
                    die("File size is too big!");
                }

                //allowed file type Server side check
                switch (strtolower($_FILES['photo']['type'])) {
                    //allowed file types
                    case 'image/png':
                    case 'image/jpeg':
                    case 'image/pjpeg':
                        break;
                    default:
                        die('Unsupported File!'); //output error
                }

                $File_Name = strtolower($_FILES['photo']['name']);
                $File_Ext = substr($File_Name, strrpos($File_Name, '.')); //get file extention
                $Random_Number = rand(0, 9999); //Random number to be added to name.
                $NewFileName = $Random_Number . $File_Ext; //new file name
                // jika user upload lebih dari 1, maka gagal upload

                $move = move_uploaded_file($_FILES['photo']['tmp_name'], $UploadDirectory . $NewFileName);
                if ($move) {
                    $table = 'produk';
                    $data = Array(
                        'kd_produk' => $product_kd,
                        'kd_kategori' => $product_category,
                        'nm_produk' => $product_name,
                        'stok' => $product_qty,
                        'gambar_produk' => $NewFileName,
                        'diskon' => $product_diskon,
                        'harga' => $product_price,
                        'deskripsi' => $product_desc,
                        'date_add' => $date_add
                    );



                    $insert = $this->core_base->insertdata($table, $data);


                    if ($insert) {
                        redirect(base_url('backend/' . $type . '?info=add-success'));
                    } else {
                        redirect(base_url('backend/add/' . $type . '?info=add-failed'));
                    }
                }
            } else {
                die('Something wrong with upload! Is "upload_max_filesize" set correctly?');
            }
        } else if ($type == 'category_product') {
          
            $category_product_name = $this->input->post('category_product_name');
            $data = Array(
                'nm_kategori' => $category_product_name
            );

            $insert = $this->core_base->insertdata('kategori', $data);

            if ($insert) {
                redirect(base_url('backend/category_product/' . $type . '?info=add-success'));
            } else {
                redirect(base_url('backend/add/' . $type . '?info=add-failed'));
            }
        } else if ($type == 'kurir') {
            $nama_kurir = $this->input->post('nama_kurir');
            $alamat_kurir = $this->input->post('alamat_kurir');
            $no_hp_kurir = $this->input->post('no_hp_kurir');
            
            
            $data = Array(
                'nama_kurir' => $nama_kurir,
                'alamat_kurir' =>  $alamat_kurir,
                'no_telp_kurir' => $no_hp_kurir,
            );

            $insert = $this->core_base->insertdata('kurir', $data);

            if ($insert) {
                redirect(base_url('backend/kurir/' . $type . '?info=add-success'));
            } else {
                redirect(base_url('backend/add/' . $type . '?info=add-failed'));
            }
            
            
        } else if ($type == 'ongkir') {
            $nama = $this->input->post('nama');
            $harga = $this->input->post('harga');
            
            
            $data = Array(
                'nama_kota' => $nama,
                'harga_ongkir' =>  $harga
            );

            $insert = $this->core_base->insertdata('ongkir', $data);

            if ($insert) {
                redirect(base_url('backend/ongkir/' . $type . '?info=add-success'));
            } else {
                redirect(base_url('backend/add/' . $type . '?info=add-failed'));
            }
            
            
        } else if ($type == 'pengiriman') {
            $kd_pesanan = $this->input->post('kd_pesanan');
            
            $cek_nopesanan = $this->core_admin->countdata('pesanan', Array('kd_pesanan' => $kd_pesanan));
            $cek_no_order_sj = $this->core_admin->countdata('surat_jalan', Array('kd_pesanan' => $kd_pesanan));

            if($cek_nopesanan < 1) {
                redirect(base_url('backend/add/pengiriman?info=kodepesan-notfound'));
            } else if($cek_no_order_sj == 1) {
                redirect(base_url('backend/add/pengiriman?info=sj-available'));
            } else {
                $product_kd = $this->core_admin->kodesuratjalan();
                $nama_kurir = $this->input->post('nama_kurir');
                $tgl_kirim = $this->input->post('tgl_kirim');
                $status_kirim = $this->input->post('status_kirim');
                $data = Array(
                    'kd_sj' => $product_kd,
                    'kd_pesanan' => $kd_pesanan,
                    'kurir_id'=> $nama_kurir,
                    'tgl_sj' =>  $tgl_kirim,
                    'status_kirim' => $status_kirim,
                    'sj_date_add' => date('Y-m-d H:i:s'),
                );
    
                $insert = $this->core_base->insertdata('surat_jalan', $data);
    
                if ($insert) {
                    if($status_kirim == 1) {
                        $data_status_proccess = Array('status_pesanan' => 3);
                        $where_status = Array('kd_pesanan' => $kd_pesanan);
                        $update_status_pesanan = $this->core_base->updatedata('pesanan', $data_status_proccess, $where_status);
                    } else if($status_kirim == 2) { 
                        $data_status_proccess = Array('status_pesanan' => 4);
                        $where_status = Array('kd_pesanan' => $kd_pesanan);
                        $update_status_pesanan = $this->core_base->updatedata('pesanan', $data_status_proccess, $where_status);
                    }


                    redirect(base_url('backend/pengiriman/' . $type . '?info=add-success'));
                } else {
                    redirect(base_url('backend/add/' . $type . '?info=add-failed'));
                }
            }
            
            
        } else if ($type == 'staff') {
            $staff_hak_akses = $this->input->post('staff_hak_akses');
            $staff_fullname = $this->input->post('staff_fullname');
            $staff_email = $this->input->post('staff_email');
            $staff_phone = $this->input->post('staff_phone');
            $staff_address = $this->input->post('staff_address');
            $staff_gender = $this->input->post('staff_gender');
            $username = $this->input->post('username');
            $password = sha1(md5($this->input->post('password')));

            if (isset($_FILES["photo"]) && $_FILES["photo"]["error"] == UPLOAD_ERR_OK) {
                $UploadDirectory = './repository/staff/';

                //Is file size is less than allowed size.
                if ($_FILES["photo"]["size"] > 1042880) {
                    die("File size is too big!");
                }

                //allowed file type Server side check
                switch (strtolower($_FILES['photo']['type'])) {
                    //allowed file types
                    case 'image/png':
                    case 'image/jpeg':
                    case 'image/pjpeg':
                        break;
                    default:
                        die('Unsupported File!'); //output error
                }

                $File_Name = strtolower($_FILES['photo']['name']);
                $File_Ext = substr($File_Name, strrpos($File_Name, '.')); //get file extention
                $Random_Number = rand(0, 9999); //Random number to be added to name.
                $NewFileName = $Random_Number . $File_Ext; //new file name
                // jika user upload lebih dari 1, maka gagal upload

                $move = move_uploaded_file($_FILES['photo']['tmp_name'], $UploadDirectory . $NewFileName);
                if ($move) {
                    $table = 'staff';
                    $data = Array(
                        'id_privilege' => $staff_hak_akses,
                        'username' => $username,
                        'password' => $password,
                        'staff_fullname' => $staff_fullname,
                        'staff_photo' => $NewFileName,
                        'staff_email' => $staff_email,
                        'staff_phone' => $staff_phone,
                        'staff_address' => $staff_address,
                        'staff_gender' => $staff_gender,
                        'date_add' => $date_add
                    );


                    $insert = $this->core_base->insertdata($table, $data);


                    if ($insert) {
                        redirect(base_url('backend/' . $type . '?info=add-success'));
                    } else {
                        redirect(base_url('backend/add/' . $type . '?info=add-failed'));
                    }
                }
            } else {
                die('Something wrong with upload! Is "upload_max_filesize" set correctly?');
            }
        } else if ($type == 'settings_banner') {
            $banner_title = $this->input->post('banner_title');
            $banner_desc = $this->input->post('banner_desc');

            if (isset($_FILES["photo"]) && $_FILES["photo"]["error"] == UPLOAD_ERR_OK) {
                $UploadDirectory = './repository/banner/';

                //Is file size is less than allowed size.
                if ($_FILES["photo"]["size"] > 1042880) {
                    die("File size is too big!");
                }

                //allowed file type Server side check
                switch (strtolower($_FILES['photo']['type'])) {
                    //allowed file types
                    case 'image/png':
                    case 'image/jpeg':
                    case 'image/pjpeg':
                        break;
                    default:
                        die('Unsupported File!'); //output error
                }

                $File_Name = strtolower($_FILES['photo']['name']);
                $File_Ext = substr($File_Name, strrpos($File_Name, '.')); //get file extention
                $Random_Number = rand(0, 9999); //Random number to be added to name.
                $NewFileName = $Random_Number . $File_Ext; //new file name
                // jika user upload lebih dari 1, maka gagal upload

                $move = move_uploaded_file($_FILES['photo']['tmp_name'], $UploadDirectory . $NewFileName);
                if ($move) {
                    $table = 'settings_banner';
                    $data = Array(
                        'id_staff' => $id_staff,
                        'banner_pict' => $NewFileName,
                        'banner_title' => $banner_title,
                        'banner_desc' => $banner_desc,
                        'date_add' => $date_add
                    );


                    $insert = $this->core_base->insertdata($table, $data);


                    if ($insert) {
                        redirect(base_url('backend/' . $type . '?info=add-success'));
                    } else {
                        redirect(base_url('backend/add/' . $type . '?info=add-failed'));
                    }
                }
            } else {
                die('Something wrong with upload! Is "upload_max_filesize" set correctly?');
            }
        }
    }

    public function processupdate($type = null, $id = null) {
        $ip = $this->input->ip_address();
        $proxy = xhttpdata('proxy');
        $ua = xhttpdata('ua');
        $date_edit = date('Y-m-d H:i:s');

        $id_staff = $this->session->userdata('id_staff');

        if ($id == null) {
            exit();
        }

        if ($type == 'user_order') {


            $purchase_status = $this->input->post('purchase_status');
            
            

            $table = 'pesanan';
            $data = Array(
                'status_pesanan' => $purchase_status
            );
            $where = Array('kd_pesanan' => $id);
            
            if($purchase_status == 2) {
                $this->core_base->updatedata('pembayaran', Array('status_pembayaran'=>1), $where);
            }

            $update = $this->core_base->updatedata($table, $data, $where);


            if ($update) {
                redirect(base_url('backend/edit/' . $type . '/' . $id . '?info=edit-success'));
            } else {
                redirect(base_url('backend/edit/' . $type . '/' . $id . '?info=edit-failed'));
            }
        } else if ($type == 'add_note_user_order') {
            $config = Array(
                'protocol' => $this->config->item('protocol'),
                'smtp_host' => $this->config->item('smtp_host'),
                'smtp_port' => $this->config->item('smtp_port'),
                'smtp_user' => $this->config->item('smtp_user'),
                'smtp_pass' => $this->config->item('smtp_pass'),
                'mailtype' => 'html',
                'charset' => 'iso-8859-1'
            );

            $this->load->library('email', $config);
            $this->email->set_newline("\r\n");

            $add_note = $this->input->post('add_note');
            $email = $this->input->post('email');
            $fullname = $this->input->post('fullname');
            $subject = $this->input->post('subject');

            $this->email->from('admin@anya-living.com', 'PT. Anya Living International');
            $this->email->to($email);
            
            ob_start();
            ?>
            <!Doctype html>
            <html>
                <head>
                    <title></title>
                </head>
                <body>
                    <p>
                        Hallo, <?= $fullname ?>
                    </p>
                    <p><?= $add_note ?></p>
                </body>
            </html>
            <?php
            $html = ob_get_clean();
            $this->email->subject($subject);
            $this->email->message($html);
            $this->email->send();
            redirect(base_url('backend/edit/user_order/' . $id . '?info=edit-success'));
        } else if ($type == 'product') {
            $product_category = $this->input->post('product_category');
            $product_name = $this->input->post('product_name');
            $product_qty = $this->input->post('product_qty');
            $product_diskon = $this->input->post('product_diskon');
            $product_price = $this->input->post('product_price');
            $product_desc = $this->input->post('product_desc');


            $UploadDirectory = './repository/product/';
            if ($_FILES["photo"]["size"] > 1042880) {
                die("File size is too big!");
            }

            //allowed file type Server side check
            switch (strtolower($_FILES['photo']['type'])) {
                //allowed file types
                case 'image/png':
                case 'image/jpeg':
                case 'image/pjpeg':
                case '':
                    break;
                default:
                    die('Unsupported File!'); //output error
            }

            $File_Name = strtolower($_FILES['photo']['name']);
            $File_Ext = substr($File_Name, strrpos($File_Name, '.')); //get file extention
            $Random_Number = rand(0, 9999); //Random number to be added to name.
            $NewFileName = $Random_Number . $File_Ext; //new file name
            // jika user upload lebih dari 1, maka gagal upload

            $move = move_uploaded_file($_FILES['photo']['tmp_name'], $UploadDirectory . $NewFileName);
            $table = 'produk';
            $where = Array('kd_produk' => $id);
            if ($move) {
                $data = Array(
                        'kd_kategori' => $product_category,
                        'nm_produk' => $product_name,
                        'stok' => $product_qty,
                        'gambar_produk' => $NewFileName,
                        'diskon' => $product_diskon,
                        'harga' => $product_price,
                        'deskripsi' => $product_desc,
                        'date_edit' => date('Y-m-d H:i:s')
                    );

                $update = $this->core_base->updatedata($table, $data, $where);


                if ($update) {
                    redirect(base_url('backend/edit/' . $type . '/' . $id . '?info=edit-success'));
                } else {
                    redirect(base_url('backend/edit/' . $type . '/' . $id . '?info=edit-failed'));
                }
            } else {
                 $data = Array(
                        'kd_kategori' => $product_category,
                        'nm_produk' => $product_name,
                        'stok' => $product_qty,
                        'diskon' => $product_diskon,
                        'harga' => $product_price,
                        'deskripsi' => $product_desc,
                        'date_edit' => date('Y-m-d H:i:s')
                    );
                $update = $this->core_base->updatedata($table, $data, $where);


                if ($update) {
                    redirect(base_url('backend/edit/' . $type . '/' . $id . '?info=edit-success'));
                } else {
                    redirect(base_url('backend/edit/' . $type . '/' . $id . '?info=edit-failed'));
                }
            }
        } else if ($type == 'kurir') {
            $nama = $this->input->post('nama');
            $alamat = $this->input->post('alamat');
            $no_telp = $this->input->post('no_telp');
            
            $data = Array(
                'nama_kurir' => $nama,
                'alamat_kurir' => $alamat,
                'no_telp_kurir' => $no_telp,
            );
            $where = Array('id_kurir' => $id);

            $update = $this->core_base->updatedata('kurir', $data, $where);


            if ($update) {
                redirect(base_url('backend/edit/' . $type . '/' . $id . '?info=edit-success'));
            } else {
                redirect(base_url('backend/edit/' . $type . '/' . $id . '?info=edit-failed'));
            }
            
        } else if ($type == 'ongkir') {
            $nama = $this->input->post('nama');
            $harga = $this->input->post('harga');
            
            $data = Array(
                'nama_kota' => $nama,
                'harga_ongkir' => $harga
            );
            $where = Array('id_ongkir' => $id);

            $update = $this->core_base->updatedata('ongkir', $data, $where);


            if ($update) {
                redirect(base_url('backend/edit/' . $type . '/' . $id . '?info=edit-success'));
            } else {
                redirect(base_url('backend/edit/' . $type . '/' . $id . '?info=edit-failed'));
            }
            
        }  else if ($type == 'category_product') {
            $katproductname = $this->input->post('category_product_name');
            
            $data = Array(
                'nm_kategori' => $katproductname
            );
            $where = Array('kd_sj' => $id);

            $update = $this->core_base->updatedata('kategori', $data, $where);


            if ($update) {
                redirect(base_url('backend/edit/' . $type . '/' . $id . '?info=edit-success'));
            } else {
                redirect(base_url('backend/edit/' . $type . '/' . $id . '?info=edit-failed'));
            }
            
        } else if ($type == 'pengiriman') {

            
            $kd_pesanan = $this->input->post('kd_pesanan');
            $nama_kurir = $this->input->post('nama_kurir');
            $tgl_kirim = $this->input->post('tgl_kirim');
            $status_kirim = $this->input->post('status_kirim');
          
            $data = Array(
                'kurir_id' => $nama_kurir,
                'tgl_sj' => $tgl_kirim,
                'status_kirim' => $status_kirim
            );
            $where = Array('kd_sj' => $id);
            $update = $this->core_base->updatedata('surat_jalan', $data, $where);


            if ($update) {
                if($status_kirim == 1) {
                    $data_status_proccess = Array('status_pesanan' => 3);
                    $where_status = Array('kd_pesanan' => $kd_pesanan);
                    $update_status_pesanan = $this->core_base->updatedata('pesanan', $data_status_proccess, $where_status);
                } else if($status_kirim == 2) { 
                   
                    $data_status_proccess = Array('status_pesanan' => 4);
                    $where_status = Array('kd_pesanan' => $kd_pesanan);
                    $update_status_pesanan = $this->core_base->updatedata('pesanan', $data_status_proccess, $where_status);
                }
                redirect(base_url('backend/edit/' . $type . '/' . $id . '?info=edit-success'));
            } else {
                redirect(base_url('backend/edit/' . $type . '/' . $id . '?info=edit-failed'));
            }
            
        } else if ($type == 'pembayaran') {

            
            $status_pembayaran = $this->input->post('status_pembayaran');
            $kd_pesanan = $this->input->post('kd_pesanan');
            
            $data = Array(
                'status_pembayaran' => $status_pembayaran
            );
            $where = Array('kd_pembayaran' => $id);
            
            if($status_pembayaran == 1) {
                $data_status_proccess = Array('status_pesanan' => 2);
                $where_status = Array('kd_pesanan' => $kd_pesanan);
                $update_status_pesanan = $this->core_base->updatedata('pesanan', $data_status_proccess, $where_status);
            }
            
            $update = $this->core_base->updatedata('pembayaran', $data, $where);



            if ($update) {
                redirect(base_url('backend/edit/' . $type . '/' . $id . '?info=edit-success'));
            } else {
                redirect(base_url('backend/edit/' . $type . '/' . $id . '?info=edit-failed'));
            }
            
        } else if ($type == 'staff') {
            $id_privilege = $this->input->post('id_privilege');
            $staff_fullname = $this->input->post('staff_fullname');
            $staff_email = $this->input->post('staff_email');
            $staff_phone = $this->input->post('staff_phone');
            $staff_address = $this->input->post('staff_address');
            $staff_gender = $this->input->post('staff_gender');


            $UploadDirectory = './repository/staff/';
            //Is file size is less than allowed size.
            if ($_FILES["photo"]["size"] > 1042880) {
                die("File size is too big!");
            }

            //allowed file type Server side check
            switch (strtolower($_FILES['photo']['type'])) {
                //allowed file types
                case 'image/png':
                case 'image/jpeg':
                case 'image/pjpeg':
                case '':
                    break;
                default:
                    die('Unsupported File!'); //output error
            }

            $File_Name = strtolower($_FILES['photo']['name']);
            $File_Ext = substr($File_Name, strrpos($File_Name, '.')); //get file extention
            $Random_Number = rand(0, 9999); //Random number to be added to name.
            $NewFileName = $Random_Number . $File_Ext; //new file name
            // jika user upload lebih dari 1, maka gagal upload

            $move = move_uploaded_file($_FILES['photo']['tmp_name'], $UploadDirectory . $NewFileName);
            $table = 'staff';
            $where = Array('id_staff' => $id);
            if ($move) {
                $data = Array(
                    'id_privilege' => $id_privilege,
                    'staff_fullname' => $staff_fullname,
                    'staff_photo' => $NewFileName,
                    'staff_email' => $staff_email,
                    'staff_phone' => $staff_phone,
                    'staff_address' => $staff_address,
                    'staff_gender' => $staff_gender,
                    'date_edit' => $date_edit
                );

//                print_r($data);
//                exit();

                $update = $this->core_base->updatedata($table, $data, $where);


                if ($update) {
                    redirect(base_url('backend/edit/' . $type . '/' . $id . '?info=edit-success'));
                } else {
                    redirect(base_url('backend/edit/' . $type . '/' . $id . '?info=edit-failed'));
                }
            } else {
                $data = Array(
                    'id_privilege' => $id_privilege,
                    'staff_fullname' => $staff_fullname,
                    'staff_email' => $staff_email,
                    'staff_phone' => $staff_phone,
                    'staff_address' => $staff_address,
                    'staff_gender' => $staff_gender,
                    'date_edit' => $date_edit
                );

                $update = $this->core_base->updatedata($table, $data, $where);


                if ($update) {
                    redirect(base_url('backend/edit/' . $type . '/' . $id . '?info=edit-success'));
                } else {
                    redirect(base_url('backend/edit/' . $type . '/' . $id . '?info=edit-failed'));
                }
            }
        } else if ($type == 'change_password') {
            $password = sha1(md5($this->input->post('password')));

            $table = 'staff';
            $data = Array(
                'password' => $password
            );
            $where = Array('id_staff' => $id);

            $update = $this->core_base->updatedata($table, $data, $where);
            if ($update) {
                redirect(base_url('backend/view/staff/' . $id . '?info=edit-success'));
            } else {
                redirect(base_url('backend/view/staff/' . $id . '?info=edit-failed'));
            }
        }  else if ($type == 'settings_banner') {
            $banner_title = $this->input->post('banner_title');
            $banner_desc = $this->input->post('banner_desc');
            $UploadDirectory = './repository/banner/';

            if ($_FILES["photo"]["size"] > 1042880) {
                die("File size is too big!");
            }

            //allowed file type Server side check
            switch (strtolower($_FILES['photo']['type'])) {
                //allowed file types
                case 'image/png':
                case 'image/jpeg':
                case 'image/pjpeg':
                case '':
                    break;
                default:
                    die('Unsupported File!'); //output error
            }

            $File_Name = strtolower($_FILES['photo']['name']);
            $File_Ext = substr($File_Name, strrpos($File_Name, '.')); //get file extention
            $Random_Number = rand(0, 9999); //Random number to be added to name.
            $NewFileName = $Random_Number . $File_Ext; //new file name
            // jika user upload lebih dari 1, maka gagal upload

            $move = move_uploaded_file($_FILES['photo']['tmp_name'], $UploadDirectory . $NewFileName);
            $table = 'settings_banner';
            $where = Array('id_banner' => $id);
            if ($move) {
                $data = Array(
                    'id_staff' => $id_staff,
                    'banner_title' => $banner_title,
                    'banner_desc' => $banner_desc,
                    'banner_pict' => $NewFileName,
                    'date_edit' => $date_edit
                );

//                print_r($data);
//                exit();

                $update = $this->core_base->updatedata($table, $data, $where);


                if ($update) {
                    redirect(base_url('backend/edit/' . $type . '/' . $id . '?info=edit-success'));
                } else {
                    redirect(base_url('backend/edit/' . $type . '/' . $id . '?info=edit-failed'));
                }
            } else {
                $data = Array(
                    'id_staff' => $id_staff,
                    'banner_title' => $banner_title,
                    'banner_desc' => $banner_desc,
                    'date_edit' => $date_edit
                );

//                print_r($data);
//                exit();

                $update = $this->core_base->updatedata($table, $data, $where);


                if ($update) {
                    redirect(base_url('backend/edit/' . $type . '/' . $id . '?info=edit-success'));
                } else {
                    redirect(base_url('backend/edit/' . $type . '/' . $id . '?info=edit-failed'));
                }
            }
        } 
    }

    public function processremove($type = null, $id = null) {
        if ($type == 'product') {
            $table = 'produk';
            $where = Array(
                'kd_produk' => $id
            );

            $delete = $this->core_base->removedata($table, $where);
            if ($delete) {
                redirect(base_url('backend/' . $type . '?info=delete-success'));
            } else {
                redirect(base_url('backend/' . $type . '?info=delete-failed'));
            }
        } else if ($type == 'category_product') {
            $table = 'kategori';
            $where = Array(
                'kd_kategori' => $id
            );

            $delete = $this->core_base->removedata($table, $where);
            if ($delete) {
                redirect(base_url('backend/category_product/' . $type . '?info=delete-success'));
            } else {
                redirect(base_url('backend/category_product/' . $type . '?info=delete-failed'));
            }
        } else if ($type == 'kurir') {
            $table = 'kurir';
            $where = Array(
                'id_kurir' => $id
            );

            $delete = $this->core_base->removedata($table, $where);
            if ($delete) {
                redirect(base_url('backend/' . $type . '?info=delete-success'));
            } else {
                redirect(base_url('backend/' . $type . '?info=delete-failed'));
            }
        } else if ($type == 'pengiriman') {
            $table = 'surat_jalan';
            $where = Array(
                'kd_sj' => $id
            );

            $delete = $this->core_base->removedata($table, $where);
            if ($delete) {
                redirect(base_url('backend/' . $type . '?info=delete-success'));
            } else {
                redirect(base_url('backend/' . $type . '?info=delete-failed'));
            }
        } 
    }

    public function proccess_report_sales() {
        $tanggal1 = $this->input->post('tanggal1');
        $tanggal2 = $this->input->post('tanggal2');

//        if (!empty($tanggal1) && !empty($tanggal2)) {
//            $data['show_data'] = $this->core_admin->getrelasi('report_sales_between', null, $tanggal1, $tanggal2);
//        } else {
//        }
        $data['show_data'] = $this->core_admin->getrelasi('report_sales');

        $arr = Array();
        foreach ($data['show_data'] as $val) {
            $arr[] = Array(
                'y' => date('Y-m-d', strtotime($val['purchase_date'])),
                'item1' => $val['tot']
            );
        }
        echo json_encode($arr);
    }

}
