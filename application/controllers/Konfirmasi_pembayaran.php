<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

Class Konfirmasi_pembayaran extends CI_Controller {

    public function __construct() {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
    }

    public function index() {
        $data['show_contact'] = $this->core_base->getdata('settings_contact', Array('id_settings_contact' => 1));
        $data['menu'] = 'menu';
        $data['module'] = 'konfirmasi_pembayaran';
        $this->load->view('template', $data);
    }

    public function do_confirm() {
        // jika no order tidak valid
        $status = null;
        $msg = null;
        $no_order = $this->input->post('no_order');
        $bank_tujuan = $this->input->post('bank_tujuan');
        $bank_anda = $this->input->post('bank_anda');
        $an = $this->input->post('an');
        $metode_transfer = $this->input->post('metode_transfer');
        $nominal_transfer = $this->input->post('nominal_transfer');
        $tanggal_transfer = $this->input->post('tanggal_transfer');
        

        $cek_no = $this->core_base->countdata('purchase', Array('no_order' => $no_order));

        if ($cek_no < 1) {
            $status = 'not_valid_noorder';
            $msg = 'No Order yang anda masukan tidak valid';
        } else {
            if (isset($_FILES["file"]) && $_FILES["file"]["error"] == UPLOAD_ERR_OK) {
                $UploadDirectory = './repository/konfirmasi_pembayaran/';

                //Is file size is less than allowed size.
                if ($_FILES["file"]["size"] > 1042880) {
                    $status = 'failed';
                    $msg = 'File size is too big!';
                }

                //allowed file type Server side check
                switch (strtolower($_FILES['file']['type'])) {
                    //allowed file types
                    case 'image/png':
                    case 'image/jpeg':
                    case 'image/pjpeg':
                        break;
                    default:
                        $status = 'failed';
                        $msg = 'Unsupported File!';
                }

                $File_Name = strtolower($_FILES['file']['name']);
                $File_Ext = substr($File_Name, strrpos($File_Name, '.')); //get file extention
                $Random_Number = rand(0, 999999); //Random number to be added to name.
                $NewFileName = $Random_Number . $File_Ext; //new file name
                // jika user upload lebih dari 1, maka gagal upload

                $move = move_uploaded_file($_FILES['file']['tmp_name'], $UploadDirectory . $NewFileName);
                if ($move) {
                    $update_purchase_confirmation = $this->core_base->updatedata('purchase', Array(
                        'confirmation_purpose_bank' => $bank_tujuan,
                        'confirmation_customer_bank' => $bank_anda,
                        'confirmation_a_n' => $an,
                        'confirmation_method_transfer' => $metode_transfer,
                        'confirmation_ammount' => $nominal_transfer,
                        'confirmation_payment_evidence' => $NewFileName,
                        'confirmation_tgl_transfer' => $tanggal_transfer
                            ), Array('no_order' => $no_order));
                    if ($update_purchase_confirmation) {
                        $status = 'success';
                        $msg = 'Terimakasih sudah konfirmasi, kami segera proses';
                    }
                }
            } else {
                $status = 'failed';
                $msg = 'Something wrong with upload! Is "upload_max_filesize" set correctly?';
            }
        }

        echo json_encode(Array('status' => $status, 'msg' => $msg));
    }

}
