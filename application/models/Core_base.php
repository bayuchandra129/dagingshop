<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
Class Core_base extends CI_Model {
    public function __construct() {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
    }
    
    function getdata($table = "", $where = "", $order = "", $limit = "") {
        if ($order != "") {
            $this->db->order_by($order[0], $order[1]);
        }

        if ($limit != "") {
            $this->db->limit($limit[1], $limit[0]);
        }

        if ($where == "") {
            $query = $this->db->get($table);
        } else {
            $query = $this->db->get_where($table, $where);
        }

        $result = $query->result_array();
        if ($order == "") {
            if (count($result) > 0) {
                $result = $result[0];
            } else {
                $result = null;
            }
        }
        return $result;
    }

    function countdata($table="", $where="") {
        if ($where != "") {
            $this->db->where($where);
        }
        $count = $this->db->from($table)->count_all_results();
        return $count;
    }

    function updatedata($table = "", $data = "", $where = "") {
        if ($where != "") {
            $this->db->where($where);
        }
        return $this->db->update($table, $data);
    }

    function insertdata($table = "", $data = "") {
        return $this->db->insert($table, $data);
    }

    function removedata($table = "", $where = "") {
        return $this->db->delete($table, $where);
    }
    
    function ceklogin($username = null, $password = null) {
        $this->db->select('*')
                ->from('pelanggan')
                ->where('email_plg', $username)
                ->where('pass_plg', $password);
        $query = $this->db->get()->result_array();
        return $query;
    }
    
    function getrelasi($params, $where = null) {
       if($params == 'relasi_testimonial') {
           if(!empty($where)) {
               $where = " WHERE A.id_c_testimonial='".$where."' && testimonial_status='1'";
           } else {
              $where = " WHERE testimonial_status='1'";
           }
           $query = "SELECT A.*, A.date_add as tgl_add_testimonial, 
                    A.date_edit as tgl_edit_testimonial, B.*, C.*, D.*, E.id_product, E.product_name 
                     FROM testimonial A
                     LEFT JOIN user B
                     ON b.id_user = C.id_user
                     LEFT JOIN category_testimonial D
                     ON A.id_c_testimonial = D.id_c_testimonial
                     LEFT JOIN product E
                     ON A.id_product = E.id_product
                     
                   ";
            $query .= $where;
           $query .= " ORDER BY tgl_add_testimonial DESC";
       } else if ($params == 'relasi_produk_all') {
           if(!empty($where)) {
               $where2 = " WHERE A.id_cproduk='".$where."'";
           }
           $query = "SELECT A.*, A.date_add as date_add_product,
                     A.date_edit as date_edit_product, B.*, C.*, D.*
                     FROM product A
                     LEFT JOIN category_product B 
                     ON A.id_cproduk = B.id_cproduct
                     LEFT JOIN vendor C
                     ON A.id_vendor = C.id_vendor
                     LEFT JOIN type_sales D
                     ON A.id_type_sales = D.id_type_sales ";
           $query .= $where2;
           $query .= " ORDER BY date_add_product DESC";
       } else if ($params == 'relasi_produk_kategori') {
           if(!empty($where)) {
               $where2 = " WHERE A.kd_kategori='".$where."'";
            }

           $query = "SELECT A.*, A.date_add as date_add_product,
                     A.date_edit as date_edit_product, B.*
                     FROM produk A
                     LEFT JOIN kategori B 
                     ON A.kd_kategori = B.kd_kategori";
        //    $query .= " WHERE MONTH(A.date_add) = MONTH(NOW())";
            if(!empty($where)) {
            $query .= $where2;
            }
           $query .= " ORDER BY date_add_product DESC";
       } else if ($params == 'relasi_produk_detail') {
           if(!empty($where)) {
               $where2 = " WHERE A.kd_produk='".$where."'";
           }
           $query = "SELECT A.*, A.date_add as date_add_product,
                     A.date_edit as date_edit_product, B.*
                     FROM produk A
                     LEFT JOIN kategori B 
                     ON A.kd_kategori = B.kd_kategori";
           $query .= $where2;
       } else if ($params == 'relasi_produk_recommendasi') {
           if(!empty($where)) {
               $where2 = " WHERE A.kd_kategori='".$where."'";
           }
           $query = "SELECT A.*, A.date_add as date_add_product,
                     A.date_edit as date_edit_product, B.*
                     FROM produk A
                     LEFT JOIN kategori B 
                     ON A.kd_kategori = B.kd_kategori";
           $query .= $where2;
       } else if ($params == 'relasi_purchase') {
           if(!empty($where)) {
               $where2 = " WHERE A.kd_pesanan='".$where."'";
           }
           $query = "SELECT *, A.kd_pesanan as  pesanan_code FROM pesanan A 
                   LEFT JOIN detil_pesanan B 
                   ON A.kd_pesanan = B.kd_pesanan 
                   LEFT JOIN produk C
                   ON B.kd_produk = C.kd_produk
                   ";
           $query .= $where2;
       } else if ($params == 'relasi_pesanan_ongkir') {
            if(!empty($where)) {
                $where2 = " WHERE A.kd_pesanan='".$where."'";
            }
            $query = "SELECT * FROM pesanan A 
                    LEFT JOIN ongkir B 
                    ON A.ongkir_id = B.id_ongkir
                    ";
            $query .= $where2;
        } else if ($params == 'relasi_pengiriman') {
            if(!empty($where)) {
                $where = " WHERE A.kd_pesanan='".$where."'";
            }
            $query = "SELECT * FROM surat_jalan A
                      LEFT JOIN pesanan B 
                      ON A.kd_pesanan = B.kd_pesanan
                      LEFT JOIN kurir C 
                      ON A.kurir_id = C.id_kurir
                    ";
            $query .= $where;
        } 
       
       $sql = $this->db->query($query);
       $query = $sql->result_array();
       return $query;         
    }
    
    public function get_search($params1) {
        $query = "SELECT * FROM produk WHERE nm_produk LIKE '%".$params1."%'";
        
        $sql = $this->db->query($query);
       $query = $sql->result_array();
       return $query; 
    }

    public function kodepembayaran(){
        $this->db->select('RIGHT(pembayaran.kd_pembayaran,2) as kode_pembayaran', FALSE);
        $this->db->order_by('kode_pembayaran','DESC');    
        $this->db->limit(1);    
        $query = $this->db->get('pembayaran');  //cek dulu apakah ada sudah ada kode di tabel.    
        if($query->num_rows() <> 0){      
            //cek kode jika telah tersedia    
            $data = $query->row();      
            $kode = intval($data->kode_pembayaran) + 1; 
        }
        else{      
            $kode = 1;  //cek jika kode belum terdapat pada table
        }
            $random=create_random(4); 
            $batas = str_pad($kode, 4, "0", STR_PAD_LEFT);    
            $kodetampil = "PEM"."AL-".$random."-".$batas;  //format kode
            return $kodetampil;  
    }
    
}

/* End of file base.php */
/* Location: ./application/models/core_admin.php */