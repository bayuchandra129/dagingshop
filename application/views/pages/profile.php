<section>
    <div class="content-cart">
        <div id="content">

            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h3 class="title">MEMBER AREA<i class="fa fa-caret-right"></i><span>PROFILE</span></h3>
                        <div class="welcome">
                            <span class="hello">Halo, <?= $this->session->userdata('fullname'); ?><i></i></span>
                            <span class="userid"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!-- left form -->
                    <div class="col-xs-2 col-sm-2 col-md-3 ">
                        <?php require_once($sidebar_dashboard . '.php'); ?>
                    </div>
                    <div class="col-xs-10  col-sm-10 col-md-9 content-wrap">
                        <!-- Profil -->
                        <div class="profil">

                            <h3 class="profil-title">Profil Member</h3><br/>

                            <div class="profil-wrap">

                                <h6 class="profil-title">Data Diri</h6>
                                <div class="form-wrap row">
                                    <div class="col-xs-12">
                                        <div class="col-xs-4">Nama Pelanggan</div>
                                        <div class="col-xs-8"><?= $profile['nm_plg'] ?></div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="col-xs-4">Email</div>
                                        <div class="col-xs-8"><?= $profile['email_plg'] ?></div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="col-xs-4">No. Handphone</div>
                                        <div class="col-xs-8"><?= $profile['tlp_plg'] ?></div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="col-xs-4">Alamat Lengkap</div>
                                        <div class="col-xs-8"><?= $profile['alamat'] ?>, <?=$profile['kota'] ?>, <?=$profile['kode_pos'] ?></div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="col-xs-12">
                                            <br>
                                            <a class="pbutton" href="<?= base_url() ?>dashboard/profile/ubah_profile">UBAH PROFIL</a>
                                            <a class="pbutton" href="<?= base_url() ?>dashboard/profile/ubah_password">UBAH PASSWORD</a>
                                        </div>
                                    </div>
                                </div>


                            </div>

                        </div>						</div>
                </div>

            </div>
        </div>
    </div>    
</section>
