<div class="map-responsive">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.9298504941426!2d106.85790701529453!3d-6.1401260619052005!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f5ac5e7d362b%3A0x531c3e6400a77e1e!2sSUSU%20JAHE%20MAS%20APRIL!5e0!3m2!1sen!2sid!4v1643706564552!5m2!1sen!2sid" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>
<!-- End Map -->
<br/><br/>
<!-- Start Content -->
<div id="content">
    <div class="container">

        <div class="row">

            <div class="col-md-8">
                <!-- Classic Heading -->
                <h4 class="classic-title"><span>Kontak Kami</span></h4>


                    <form role="form" class="contact-form" id="contact-form" method="post">
                        <div class="form-group">
                            <div class="controls">
                                <input type="text" placeholder="Nama Lengkap" id="fullname" class="fullname" name="name">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="controls">
                                <input type="email" class="email" id="email" placeholder="Email" name="email">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="controls">
                                <input type="text" class="requiredField subject" id="subject" placeholder="Subjek" name="subject">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="controls">
                                <textarea rows="7" placeholder="Isi Pesan" id="message" class="message" name="message"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <center>
                                <div class="opsi-info">
                                </div>
                            </center>
                        </div>
                        <input type="button" style="float: right" onclick="proccesscontact()" value="SEND" id="submit" class="btn-system btn-large"/>
                    </form>
               
                <!-- End Contact Form Help -->

            </div>
            <div class="m-contact-both" ></div>
            <div class="col-md-4">

                <!-- Classic Heading -->
                <h4 class="classic-title"><span>Information</span></h4>

                <!-- Some Info -->
                <p>PT. GLOBAL PRATAMA WIJAYA</p>

                <!-- Divider -->
                <div class="hr1" style="margin-bottom:10px;"></div>

                <!-- Info - Icons List -->
                <ul class="icons-list">
                    <li><i class="fa fa-globe">  </i> <strong>Address:</strong> Jl. Agung Barat 30, Blok B-36A No. 54A, RT.2/RW.10, Sunter Agung, Tj. Priok, Kota Jkt Utara, Daerah Khusus Ibukota Jakarta 14350</li>
                    <li><i class="fa fa-envelope-o"></i> <strong>Email:</strong> info@globalpratamawijaya.com</li>
                    <li><i class="fa fa-mobile"></i> <strong>Phone:</strong> +62 21 6409169</li>
                </ul>

                <!-- Divider -->
                <div class="hr1" style="margin-bottom:15px;"></div>

            </div>

        </div>

    </div>
</div><br/>
<!-- End content -->