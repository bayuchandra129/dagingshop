
<!-- Start Page Banner -->
<div class="page-banner" style="padding:40px 0; background: url(<?= base_url() ?>assets/images/slide-02-bg.jpg) center #f9f9f9;">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h2>Semua Produk</h2>
            </div>
            <div class="col-md-6">
                <ul class="breadcrumbs">
                    <li><a href="#">Home</a></li>
                    <li>Semua Produk</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- End Page Banner -->
<br/><br/>

<!-- Start Content -->
<div id="content">
    <div class="container">
        <div class="page-content">


            <div class="row">

                <div class="col-md-12">
                        <?php 
                        foreach ($show_product as $val) {
                            $diskon = ($val['harga'] / 100) * $val['diskon'];
                            $harga_diskon = $val['harga'] - $diskon;
                            ?>
                            <div class="col-sm-4 col-md-3">
                                <div class="thumbnail" >
                                     <?php
                                        if ($val['gambar_produk'] == "") {
                                        ?>
                                        <a title="<?= $val['nm_produk'] ?>" href="<?= base_url() ?>product/detail/<?= $val['kd_produk'] ?>" >
                                            <div class="thumb-overlay"><i class="fa fa-link"></i></div>
                                            <img class="img-thumbnail img-responsive img-recent-produkall" width="480px" height="332px" alt="" src="<?= base_url() ?>assets/admin/img/not_available.jpg"/>
                                        </a>
                                    <?php } else { ?>
                                        <a  title="<?= $val['nm_produk'] ?>" href="<?= base_url() ?>product/detail/<?= $val['kd_produk'] ?>" >
                                            <img class="img-thumbnail img-responsive img-recent-produkall" width="480" alt="" src="<?= base_url() ?>repository/product/<?= $val['gambar_produk'] ?>" />
                                        </a>
                                    <?php } ?>
                                    <div class="caption">
                                        <div class="row">
                                            <div class="col-md-12 price ">
                                            <?php 
                                                if($val['diskon'] > 0) {
                                            ?>
                                            <label class="color-red-bold text-product-price  pull-right"><?= convert_to_rupiah($harga_diskon) ?></label>  
                                            <label class="color-red-bold text-product-price coret  pull-right" style="padding-right:10px;"><?= convert_to_rupiah($val['harga']) ?></label> &nbsp;&nbsp;&nbsp;
                                                <?php } else { ?>
                                                    <label class="color-red-bold text-product-price  pull-right" style="padding-right:10px;"><?= convert_to_rupiah($val['harga']) ?></label> &nbsp;&nbsp;&nbsp;
                                                <?php } ?>
                                            </div>
                                            <div class="col-md-12" style="clear:both;">
                                                <h3><?= $val['nm_produk'] ?></h3>
                                            </div>
                                            
                                        </div> <br/>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <a href="<?= base_url() ?>product/detail/<?= $val['kd_produk'] ?>" class="btn btn-primary btn-product" style="width:100%;"><span class="glyphicon glyphicon-shopping-cart"></span> Lihat Detil</a>
                                            </div>
                                        </div>

                                        <p> </p>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                         ?>

                </div>



            </div>
            </br>
            </br>


            <!-- Start Team Members -->
            <div class="row">

                <!-- Start Memebr 1 -->
                <div class="col-md-12 col-sm-12 col-xs-12" data-animation="fadeIn" data-animation-delay="03">
                    <div class="recent-projects">
                        
                    </div>
                </div>
                <!-- End Memebr 1 -->


            </div>
            <!-- End Memebr 4 -->

        </div>
        <!-- End Team Members -->

    </div>
    <!-- .container -->
</div><br/><br/>
<!-- End Team Member Section -->
</div>