<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Data Staff
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?=base_url()?>backend"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li class="active">Data Staff</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                
                <div class="box-header">
                    <h3 class="box-title">Data Master Staff</h3><br/><br/>
                    <div class="col-md-2">
                        <a href="<?=base_url()?>backend/add/staff" class="btn btn-block btn-primary">Tambah Staff</a>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                     <table id="example1" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th class="text-center">Opsi</th>
                                <th class="text-center">Jabatan</th>
                                <th class="text-center">Photo</th>
                                <th class="text-center">Full Name</th>
                                <th class="text-center">Username</th>
                                <th class="text-center">Email</th>
                                <th class="text-center">Phone</th>
                                <th class="text-center">JK</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            $no = 1;
                                foreach($show_data as $val) {
                            ?>
                            <tr>
                                <td>
                                    <?=$no;?>
                                </td>
                                <td class="text-center">
                                  <a href="<?=base_url()?>backend/view/staff/<?=$val['id_staff']?>">
                                        <span title="View" aria-hidden="true" class="glyphicon glyphicon-eye-open"></span> &nbsp;&nbsp;&nbsp;
                                    </a>
                                      <a href="<?=base_url()?>backend/edit/staff/<?=$val['id_staff']?>">
                                        <span title="Edit" aria-hidden="true" class="glyphicon glyphicon-edit"></span>  &nbsp;&nbsp;&nbsp;
                                    </a>
                                    <!--<a onclick="return confirm('Apakah anda yakin ingin menghapus data ini ? ')" href="<?=base_url()?>proccess_backend/processremove/staff/<?=$val['id_staff']?>">
                                        <span title="Remove" aria-hidden="true" class="fa fa-trash"></span> 
                                    </a>-->
                                </td>
                                <td class="text-center">
                                    <?=strtoupper($val['privilege_type'])?>
                                </td>
                                <td class="text-center">
                                     <?php
                                if ($val['staff_photo'] == "") {
                                    ?>
                                    <a class="fancybox" data-fancybox-group="gallery" href="<?= base_url() ?>assets/admin/img/not_available.jpg" >
                                    <img width="100" style="border:2px solid #000" src="<?= base_url() ?>assets/admin/img/not_available.jpg"/>
                                    </a>
                                <?php } else { ?>
                                    <a class="fancybox" data-fancybox-group="gallery" title="<?=$val['staff_fullname']?>" href="<?= base_url() ?>repository/staff/<?=$val['staff_photo'] ?>" >
                                    <img width="100" style="border:2px solid #000" src="<?= base_url() ?>repository/staff/<?=$val['staff_photo'] ?>"/>
                                    </a>
                                <?php } ?>    
                                </td>
                                <td class="text-center">
                                    <?=$val['staff_fullname']?>
                                </td>
                                <td class="text-center">
                                    <?=$val['username']?>
                                </td>
                                <td class="text-center">
                                    <?=$val['staff_email']?>
                                </td>
                                <td class="text-center">
                                    <?=$val['staff_phone']?>
                                </td>
                                <td class="text-center">
                                    <?= stats_gender($show_data[0]['staff_gender']) ?>
                                </td>
                            </tr>
                            <?php $no++; } ?>
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
