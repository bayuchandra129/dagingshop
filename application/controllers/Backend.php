<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');
Class Backend extends CI_Controller {

    public function __construct() {
        parent::__construct();
        
        if ($this->session->userdata('logged_in') != "Sudah Loggin") {
            redirect(base_url() . 'login');
        } 
        date_default_timezone_set("Asia/Jakarta");        
    }

    public function panel() {
        $data['module'] = 'home';
        $this->load->view('panel', $data);
    }
    
    
    public function user() {
        $data['show_data'] = $this->core_admin->getdata('pelanggan', null, Array('date_register','DESC'), null);
 
        $data['module'] = 'user';
        $this->load->view('panel', $data);
    }
    
    public function user_order() {
        $data['count_cancel'] = $this->core_admin->countdata('pesanan', Array('status_pesanan'=>0));
        $data['count_pending'] = $this->core_admin->countdata('pesanan', Array('status_pesanan'=>1));
        $data['count_proccessing'] = $this->core_admin->countdata('pesanan', Array('status_pesanan'=>2));
        $data['count_delivery'] = $this->core_admin->countdata('pesanan', Array('status_pesanan'=>3));
        $data['count_complete'] = $this->core_admin->countdata('pesanan', Array('status_pesanan'=>4));
        $data['count_retur'] = $this->core_admin->countdata('pesanan', Array('status_pesanan'=>5));
        $data['show_data'] = $this->core_admin->getrelasi('user_order_admin');
        $data['module'] = 'user_order';
        $this->load->view('panel', $data);
    }
    
    public function user_konfirmasi_pembayaran() {        
        $data['show_data'] = $this->core_admin->getdata('pembayaran', null, Array('pem_date_add','DESC'));
        $data['module'] = 'user_konfirmasi_pembayaran';
        $this->load->view('panel', $data);
    }

    public function ongkir() {
        $data['module'] = 'ongkir';
        $data['show_data'] = $this->core_admin->getdata('ongkir', null, Array('nama_kota','ASC'));

        $this->load->view('panel', $data);
    }

    public function kurir() {
        $data['module'] = 'kurir';
        $data['show_data'] = $this->core_admin->getdata('kurir', null, Array('nama_kurir','ASC'));


        $this->load->view('panel', $data);
    }

    public function product() {        
        $data['module'] = 'product';
        $data['show_data'] = $this->core_admin->getrelasi('relasi_produk');

        $this->load->view('panel', $data);
    }

    public function category_product() {
        $data['module'] = 'category_product';
        $data['show_data'] = $this->core_base->getdata('kategori', null, Array('nm_kategori', 'ASC'), null);
        $this->load->view('panel', $data);
    }
    
    public function pengiriman() {
        $id_staff = $this->session->userdata('logged_in');
        
        $data['module'] = 'pengiriman';
        $data['show_data'] = $this->core_admin->getrelasi('relasi_pengiriman');
    
        $this->load->view('panel', $data);
    }

    public function retur() {
        $id_staff = $this->session->userdata('logged_in');
        
        $data['module'] = 'retur';
        $data['show_data'] = $this->core_admin->getdata('retur', null, Array('tgl_retur', 'desc'), null);
        $this->load->view('panel', $data);
    }

    public function staff() {
        $id_staff = $this->session->userdata('logged_in');
        // jika not session
        if (!$id_staff) {
            redirect(base_url() . 'backend');
        }
        
        $data['module'] = 'staff';
        $data['show_data'] = $this->core_admin->getrelasi('relasi_staff');
     
        $this->load->view('panel', $data);
    }
    public function staff_login() {
        $id_staff = $this->session->userdata('logged_in');
        // jika not session
        if (!$id_staff) {
            redirect(base_url() . 'backend');
        }
        
        $data['module'] = 'client';
        $data['show_data'] = $this->core_base->getdata('client', null, Array('date_add', 'DESC'), null);
        $this->load->view('panel', $data);
    }

    
    
    public function laporan_pesanan() {
        $data['module'] = 'laporan_pesanan';
        $data['show_data'] = $this->core_admin->getrelasi('relasi_pengiriman_detail');

        
        $this->load->view('panel', $data);
    }

    public function laporan_penjualan() {
        $data['module'] = 'laporan_penjualan';
        $data['show_data'] = $this->core_admin->getrelasi('relasi_laporan_penjualan');
        
        $this->load->view('panel', $data);
    }


    public function laporan_pembayaran() {
        $data['module'] = 'laporan_pembayaran';
        $data['show_data'] = $this->core_admin->getrelasi('relasi_laporan_pembayaran');
        
        $this->load->view('panel', $data);
    }

    public function laporan_retur() {
        $data['module'] = 'laporan_retur';
        $data['show_data'] = $this->core_admin->getrelasi('relasi_retur');
        
        $this->load->view('panel', $data);
    }

    public function laporan_produk_terlaris() {
        $data['module'] = 'laporan_produk_terlaris';
        $data['show_data'] = $this->core_admin->getrelasi('relasi_laporan_produk_terlaris');
        
        $this->load->view('panel', $data);
    }

    public function settings_banner() {
        $id_staff = $this->session->userdata('logged_in');
        // jika not session
        if (!$id_staff) {
            redirect(base_url() . 'backend');
        }
        
        $data['module'] = 'settings_banner';
        $data['show_data'] = $this->core_admin->getrelasi('relasi_setting_banner');
        $this->load->view('panel', $data);
    }
    
    

    public function add($type = null) {
        $id_staff = $this->session->userdata('logged_in');
        // jika not session
        if (!$id_staff) {
            redirect(base_url() . 'backend');
        }
        
        if($type == 'product') {
            $data['show_kategory'] = $this->core_base->getdata('kategori', null, Array('nm_kategori', 'ASC'), null); 
        } else if($type == 'kurir') {

        } else if($type == 'pengiriman') {
            $data['kurir'] = $this->core_base->getdata('kurir', null, Array('nama_kurir', 'ASC')); 
        } 

        $data['type'] = $type;
        $data['module'] = 'add';
        $this->load->view('panel', $data);
    }

    public function edit($type = null, $id = null) {

        
        if($type == 'user_order') {
            $data['show_data'] = $this->core_admin->getrelasi('user_order', $id);
            $data['pesanan_saya'] = $this->core_base->getrelasi('relasi_purchase', $id);
            
            $data['pembayaran'] = $this->core_base->getdata('pembayaran', Array('kd_pesanan'=>$id));
            $data['kdpembayaran'] = $data['pembayaran']['kd_pembayaran'];
            $data['tglpembayaran'] = formatDates($data['pembayaran']['tgl_pembayaran']);
            $data['nm_pembank'] = $data['pembayaran']['nm_pembank'];
            $data['no_rek'] = $data['pembayaran']['no_rek'];
            $data['nm_bank'] = $data['pembayaran']['nm_bank'];
            $data['total_bayar'] = $data['pembayaran']['total_bayar'];
            $data['foto_bukti'] = $data['pembayaran']['foto_bukti'];
            $data['status_pembayaran'] = $data['pembayaran']['status_pembayaran'];
            
            $data['count_items'] = $this->core_admin->countdata('detil_pesanan', Array('kd_pesanan'=>$data['pesanan_saya'][0]['pesanan_code']));

        } else if ($type == 'product') {
            $data['show_data'] = $this->core_admin->getrelasi('relasi_produk', $id);
            $data['show_kategory'] = $this->core_base->getdata('kategori', null, Array('nm_kategori', 'ASC'), null);
        } else if ($type == 'category_product') {
            $data['show_data'] = $this->core_base->getdata('kategori', Array('kd_kategori' => $id));
        } else if ($type == 'staff') {
              $data['show_hak_akses'] = $this->core_base->getdata('privilege', null, Array('id_privilege', 'ASC'), null);
            $data['show_data'] = $this->core_admin->getrelasi('relasi_staff', $id);
        } else if ($type == 'kurir') {
            $data['show_data'] = $this->core_admin->getdata('kurir', Array('id_kurir'=>$id)); 
        } else if ($type == 'ongkir') {
            $data['show_data'] = $this->core_admin->getdata('ongkir', Array('id_ongkir'=>$id)); 
        } else if ($type == 'pengiriman') {
            $data['show_data'] = $this->core_admin->getdata('surat_jalan', Array('kd_sj'=>$id)); 
                $data['kurir'] = $this->core_base->getdata('kurir', null, Array('nama_kurir', 'ASC')); 
        } else if ($type == 'pembayaran') {
            $data['show_data'] = $this->core_admin->getdata('pembayaran', Array('kd_pembayaran'=>$id)); 

        } 

        $data['id'] = $id;
        $data['type'] = $type;
        $data['module'] = 'edit';
        $this->load->view('panel', $data);
    }
    
    public function view($type = null, $id = null) {
        
        $id_staff = $this->session->userdata('id_staff');
        if($type == 'user') {
            $data['show_data'] = $this->core_base->getdata('pelanggan', Array('id_plg' => $id));
        } else if ($type == 'product') {
            $data['show_data'] = $this->core_admin->getrelasi('relasi_produk', $id);
        } else if ($type == 'category_product') {
            $data['show_data'] = $this->core_base->getdata('category_product', Array('id_cproduct' => $id));
        } else if ($type == 'kurir') {
            $data['show_data'] = $this->core_base->getdata('kurir', Array('id_kurir' => $id));
        } else if ($type == 'ongkir') {
            $data['show_data'] = $this->core_base->getdata('ongkir', Array('id_ongkir' => $id));
        } else if ($type == 'pengiriman') {
            $data['show_data'] = $this->core_admin->getrelasi('relasi_pengiriman', $id); 
        } else if ($type == 'retur') {
            $data['show_data'] = $this->core_admin->getrelasi('cetak_retur', $id);
            $data['pesanan_saya'] = $this->core_admin->getrelasi('cetak_detil_retur', $id);
        } 
                    
         $data['id'] = $id;
        $data['type'] = $type;
        $data['module'] = 'view';
        $this->load->view('panel', $data);
    }

    

    /* ==== Method Process Add ==== */

    public function processadd($type = null) {

        $ip = $this->input->ip_address();
        $proxy = xhttpdata('proxy');
        $ua = xhttpdata('ua');
        $date_add = date('Y-m-d H:i:s');

//        if($type = null) {
//            exit();
//        }

        
    }

    /* ==== Method Process Edit ==== */

    public function processupdate($type = null, $id = null) {
        $ip = $this->input->ip_address();
        $proxy = xhttpdata('proxy');
        $ua = xhttpdata('ua');
        $date_edit = date('Y-m-d H:i:s');

        if ($id == null) {
            exit();
        }

       
    }
    
    public function processremove($type = null, $id = null) {
        if($type == 'kurir') {
            $table = 'kurir';
            $where = Array(
                    'id_kurir' => $id
                  );
            
            $delete = $this->core_base->removedata($table, $where);
            if ($delete) {
                redirect(base_url('backend/'. $type .'?info=edit-success'));
            } else {
                redirect(base_url('backend/'. $type . '?info=edit-failed'));
            }
        }
    }

}