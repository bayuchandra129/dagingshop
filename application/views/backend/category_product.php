<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Data Kategori Produk
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= base_url() ?>backend"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li class="active">Data Kategori Produk</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Data Master Kategori Produk</h3><br/><br/>
                    <div class="col-md-3">
                        <a href="<?= base_url() ?>backend/add/category_product" class="btn btn-block btn-primary">Tambah Kategori Produk</a>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th class="text-center">Opsi</th>
                                <th class="text-center">Nama Kategori</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            foreach ($show_data as $val) {
                                ?>
                                <tr>
                                    <td>
                                        <?= $no; ?>
                                    </td>
                                    <td class="text-center">
                                        <a href="<?= base_url() ?>backend/edit/category_product/<?= $val['kd_kategori'] ?>">
                                            <span title="Edit" aria-hidden="true" class="glyphicon glyphicon-edit"></span>  &nbsp;&nbsp;&nbsp;
                                        </a>
                                        <a onclick="return confirm('Apakah anda yakin ingin menghapus data ini ? ')" href="<?= base_url() ?>proccess_backend/processremove/category_product/<?= $val['kd_kategori'] ?>">
                                            <span title="Remove" aria-hidden="true" class="fa fa-trash"></span> 
                                        </a>
                                    </td>
                                   
                                    <td class="text-center">
                                        <?= $val['nm_kategori'] ?>
                                    </td>
                                </tr>
                                <?php $no++;
                            } ?>
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
