
<!-- Container -->
<div id="container">
    <br/> 
    <!-- Start Page Banner -->
    <div class="page-banner" style="padding:40px 0; background: url(images/slide-02-bg.jpg) center #f9f9f9;">
        <div class="container">
            <div class="row">
                <?php
                if (!empty($show_cproduct)) {
                    ?>
                    <?php
                    if($show_cproduct == 'all') {
                        ?>
                        <div class="col-md-6">
                            <h2>Produk <?=$nm_kategori?></h2>
                        </div>
                        <div class="col-md-6">
                            <ul class="breadcrumbs">
                                <li><a href="<?= base_url() ?>">Home</a></li>
                                <li><a href="<?= base_url() ?>product/all">All Product</a></li>
                                <li>Produk Terbaru</li>
                            </ul>
                        </div>
                    <?php } else { ?>
                        <div class="col-md-6">
                            <h2>Semua Produk <?= $nm_kategori ?></h2>
                        </div>
                        <div class="col-md-6">
                            <ul class="breadcrumbs">
                                <li><a href="<?= base_url() ?>">Home</a></li>
                                <li><a href="<?= base_url() ?>product/<?=$kd_kategori?>">Product</a></li>
                                <li><?= $nm_kategori ?></li>
                            </ul>
                        </div>
                    <?php } ?>
                <?php } else { ?>
                    <div class="col-md-6">
                        <h2>All Product</h2>
                        <p>We Are Professional</p>
                    </div>
                    <div class="col-md-6">
                        <ul class="breadcrumbs">
                            <li><a href="<?= base_url() ?>">Home</a></li>
                            <li><a href="<?= base_url() ?>product/all">All Product</a></li>
                            <li><?= $search ?></li>
                        </ul>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <br/><br/>
    <div id="content">
        <div class="container">
            <div class="page-content">
                <div class="row row-table" style="margin-left: 0px;">
                    <?php
                    if (empty($show_product)) {
                        ?>
                        <div class="col-md-12 ">
                            <div class="bg-warning" style="padding:20px;border-radius:5px;">
                                <h4 style="color:#ff0000">Ups, Stok produk tidak tersedia!</h4>
                            </div>
                        </div>
                    <?php
                    } else {
                        $no = 2;
                        foreach ($show_product as $val) {
                            ?>
                            <div class="col-sm-4 col-md-3">
                                <div class="thumbnail" >
                                     <?php
                                        if ($val['gambar_produk'] == "") {
                                        ?>
                                        <a title="<?= $val['nm_produk'] ?>" href="<?= base_url() ?>product/detail/<?= $val['kd_produk'] ?>" >
                                            <div class="thumb-overlay"><i class="fa fa-link"></i></div>
                                            <img class="img-thumbnail img-responsive img-recent-produkall" width="480px" height="332px" alt="" src="<?= base_url() ?>assets/admin/img/not_available.jpg"/>
                                        </a>
                                    <?php } else { ?>
                                        <a  title="<?= $val['nm_produk'] ?>" href="<?= base_url() ?>product/detail/<?= $val['kd_produk'] ?>" >
                                            <img class="img-thumbnail img-responsive img-recent-produkall" width="480" alt="" src="<?= base_url() ?>repository/product/<?= $val['gambar_produk'] ?>" />
                                        </a>
                                    <?php } ?>
                                    <div class="caption">
                                         <div class="row">
                                            <div class="col-md-12 price ">
                                                <label class="color-red-bold text-product-price pull-right"><?= convert_to_rupiah($val['harga']) ?></label></h3>
                                            </div>
                                            <div class="col-md-12" style="clear:both;">
                                                <h3><?= $val['nm_produk'] ?></h3>
                                            </div>
                                            
                                        </div> <br/>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <a href="<?= base_url() ?>product/detail/<?= $val['kd_produk'] ?>" class="btn btn-primary btn-product" style="width:100%;"><span class="glyphicon glyphicon-shopping-cart"></span> Lihat Detil</a>
                                            </div>
                                        </div>

                                        <p> </p>
                                    </div>
                                </div>
                            </div>
                            <?php
                            $no++;
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div style="clear:both"></div><br/><br/>
