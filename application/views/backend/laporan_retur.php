<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Data Laporan Retur
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?=base_url()?>backend"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li class="active">Data Laporan Retur</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Data Laporan Retur</h3><br/><br/>
                    <div class="pull-right">
                    <form class="form-inline" method="post" action="<?= base_url() ?>excel/getexcel/laporan_retur" autocomplete="off">
                        <div class="form-group">
                            <label for="exampleInputName2">Dari Tanggal</label>
                            <input type="text" name="from_date" require class="form-control datepicker"  placeholder="Dari Tanggal">
                        </div> &nbsp;&nbsp;&nbsp;
                        <div class="form-group">
                            <label for="exampleInputEmail2">Sampai Tanggal</label>
                            <input type="text" name="to_date" require class="form-control datepicker"  placeholder="Sampai Tanggal">
                        </div> &nbsp;&nbsp;&nbsp;
                    <button type="submit" class="btn btn-success pull-right">EXPORT LAPORAN</button> 
                    </form>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                     <table id="example1" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th class="text-center">No. Pesanan</th>
                                <th class="text-center">No. Pelanggan</th>
                                <th class="text-center">Nama Produk</th>
                                <th class="text-center">Qty</th>
                                <th class="text-center">Keterangan</th>
                                <th class="text-center">Tanggal Retur</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            $no = 1;
                                foreach($show_data as $val) {
                            ?>
                            <tr>
                                <td>
                                    <?=$no;?>
                                </td>
                                <td style="text-align:center;">                        
                                    <a href="<?=base_url()?>backend/edit/user_order/<?=$val['kd_pesanan']?>"><?=$val['kd_pesanan']?></a>
                                </td>
                                <td style="text-align:center;">                        
                                    <?=$val['nm_plg']?>
                                </td>
                                <td style="text-align:center;">                        
                                    <?=$val['nama_produk']?>
                                </td>    
                                <td style="text-align:center;">                        
                                    <?=$val['quantity']?>
                                </td>   
                                <td style="text-align:center;">                        
                                    <?=$val['keterangan']?>
                                </td>   
                                 <td class="text-center">
                                    <?=formatDateMonthNumerik($val['tgl_retur'])?>
                                </td>
                            </tr>
                            <?php $no++; } ?>
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
