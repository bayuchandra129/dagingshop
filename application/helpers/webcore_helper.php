<?php

function xhttpdata($type = "") {
    switch ($type) {
        case "ip":
            $xdata = $_SERVER['REMOTE_ADDR'];
            break;
        case "proxy":
            if (isset($_SERVER['HTTP_X_FORWARDED_FOR']) === TRUE) {
                $xdata = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                $xdata = "-";
            }
            break;
        case "ua":
            $xdata = $_SERVER['HTTP_USER_AGENT'];
            break;
    }
    return $xdata;
}


/* ====== Format Date Example : 16 September 2014. ====== */

function formatDate($dateval, $dateformat = 'd F Y H:i:s') {
    $datedb = strtotime($dateval);
    $dateret = date($dateformat, $datedb);
    return $dateret;
}

function formatDateMonthNumerik($dateval, $dateformat = 'd-m-Y H:i:s') {
    $datedb = strtotime($dateval);
    $dateret = date($dateformat, $datedb);
    return $dateret;
}

function formatDateNumerik($dateval, $dateformat = 'd-m-Y') {
    $datedb = strtotime($dateval);
    $dateret = date($dateformat, $datedb);
    return $dateret;
}

function formatDates($dateval, $dateformat = 'd F Y') {
    $datedb = strtotime($dateval);
    $dateret = date($dateformat, $datedb);
    return $dateret;
}

function stats_publish($params) {
    if ($params == 0) {
        $params = 'Non Publish';
    } else if ($params == 1) {
        $params = 'Publish';
    }

    return $params;
}

function status_payment($params) {
    if ($params == 0) {
        $params = 'Sedang di Proses';
    } else if ($params == 1) {
        $params = 'Lunas';
    }

    return $params;
}

function status_surat_jalan($status) {
    if ($status == 1) {
        $status = "Dalam Perjalanan";
    } else if ($status == 2) {
        $status = "Sudah Selesai";
    } else {
        $status = "Belum Dibuat";
    }

    return $status;
}


function stats_gender($params) {
    if ($params == "L") {
        $params = 'Laki - Laki';
    } else if ($params == "P") {
        $params = 'Perempuan';
    }

    return $params;
}

function status_aktivasi($status) {
    if($status == 0) {
        $status = 'Belum Aktivasi';
    } else if($status == 1) {
        $status = 'Teraktivasi';
    }

    return $status;
}

if (!function_exists('set_permalink')) {

    function set_permalink($content) {
        $karakter = array('{', '}', ')', '(', '|', '`', '~', '!', '@', '%', '$', '^', '&', '*', '=', '?', '+', '-', '/', '\\', ',', '.', '#', ':', ';', '\'', '"', '[', ']');
        $hapus_karakter_aneh = strtolower(str_replace($karakter, "", $content));
        $tambah_strip = strtolower(str_replace(' ', '-', $hapus_karakter_aneh));
        $link_akhir = $tambah_strip;
        return $link_akhir;
    }

}

function rupiah($nilai, $pecahan = 0) {
    return number_format($nilai, $pecahan, ',', '.');
}

/* ===== Kalkulasi Umur ======= */

function calc_age($birthdate) {
    list($year, $month, $day) = explode("-", $birthdate);
    $year_diff = date("Y") - $year;
    $month_diff = date("m") - $month;
    $day_diff = date("d") - $day;
    if ($month_diff < 0)
        $year_diff--;
    elseif (($month_diff == 0) && ($day_diff < 0))
        $year_diff--;
    return $year_diff;
}

function getsession($param = "") {
    $CI = get_instance();
    $CI->load->library('session');
    $result = $CI->session->userdata($param);
    return $result;
}

function acakangka($panjang) {
    $karakter = '1234567890';
    $string = '';
    for ($i = 0; $i < $panjang; $i++) {
        $pos = rand(0, strlen($karakter) - 1);
        $string .= $karakter{$pos};
    }
    return $string;
}

function status_pesanan($params) {
    $status = null;
    if ($params == 0) {
        $status = 'CANCEL';
    } else if ($params == 1) {
        $status = 'PENDING';
    } else if ($params == 2) {
        $status = 'PROCCESS';
    } else if ($params == 3) {
        $status = 'DELIVERY';
    } else if ($params == 4) {
        $status = 'COMPLETE';
    } else if ($params == 5) {
        $status = 'RETUR';
    }

    return $status;
}

function create_random($length)
{
    $data = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $string = '';
    for($i = 0; $i < $length; $i++) {
        $pos = rand(0, strlen($data)-1);
        $string .= $data{$pos};
    }
    return $string;
}

function potong_kata($str, $pertama, $kedua) {
    $str = substr($str, $pertama, $kedua);
    return $str;
}

function randomPassword() {
    $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}

/* ===== Thumbnail Photo ====== */

function makeThumbnails($updir, $img, $id) {
    $thumbnail_width = 155;
    $thumbnail_height = 245;
    $thumb_beforeword = "thumb";
    $arr_image_details = getimagesize("$updir" . $id . '_' . "$img"); // pass id to thumb name
    $original_width = $arr_image_details[0];
    $original_height = $arr_image_details[1];
    if ($original_width > $original_height) {
        $new_width = $thumbnail_width;
        $new_height = intval($original_height * $new_width / $original_width);
    } else {
        $new_height = $thumbnail_height;
        $new_width = intval($original_width * $new_height / $original_height);
    }
    $dest_x = intval(($thumbnail_width - $new_width) / 2);
    $dest_y = intval(($thumbnail_height - $new_height) / 2);
    if ($arr_image_details[2] == 1) {
        $imgt = "ImageGIF";
        $imgcreatefrom = "ImageCreateFromGIF";
    }
    if ($arr_image_details[2] == 2) {
        $imgt = "ImageJPEG";
        $imgcreatefrom = "ImageCreateFromJPEG";
    }
    if ($arr_image_details[2] == 3) {
        $imgt = "ImagePNG";
        $imgcreatefrom = "ImageCreateFromPNG";
    }
    if ($imgt) {
        $old_image = $imgcreatefrom("$updir" . $id . '_' . "$img");
        $new_image = imagecreatetruecolor($thumbnail_width, $thumbnail_height);
        imagecopyresized($new_image, $old_image, $dest_x, $dest_y, 0, 0, $new_width, $new_height, $original_width, $original_height);
        $imgt($new_image, "$updir" . $id . '_' . "$thumb_beforeword" . "$img");
    }
}


function ak_img_resize($target, $newcopy, $w, $h, $ext) {
    list($w_orig, $h_orig) = getimagesize($target);
    $scale_ratio = $w_orig / $h_orig;
    if (($w / $h) > $scale_ratio) {
           $w = $h * $scale_ratio;
    } else {
           $h = $w / $scale_ratio;
    }
    $img = "";
    $ext = strtolower($ext);
    if ($ext == "gif"){ 
    $img = imagecreatefromgif($target);
    } else if($ext =="png"){ 
    $img = imagecreatefrompng($target);
    } else { 
    $img = imagecreatefromjpeg($target);
    }
    $tci = imagecreatetruecolor($w, $h);
    // imagecopyresampled(dst_img, src_img, dst_x, dst_y, src_x, src_y, dst_w, dst_h, src_w, src_h)
    imagecopyresampled($tci, $img, 0, 0, 0, 0, $w, $h, $w_orig, $h_orig);
    if ($ext == "gif"){ 
        imagegif($tci, $newcopy);
    } else if($ext =="png"){ 
        imagepng($tci, $newcopy);
    } else { 
        imagejpeg($tci, $newcopy, 84);
    }
}

function convert_to_rupiah($angka) {
        return 'Rp ' . strrev(implode('.', str_split(strrev(strval($angka)), 3)));
    }