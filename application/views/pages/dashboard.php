<section>
<div class="content-cart">
    <div id="content">

        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="title">MEMBER AREA<i class="fa fa-caret-right"></i><span>DASHBOARD</span></h3>
                    <div class="welcome">
                        <span class="hello">Halo, <?= $this->session->userdata('email'); ?><i></i></span>
                        <span class="userid"></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <!-- left form -->
                <div class="col-xs-2 col-sm-2 col-md-3 ">
                    <?php require_once($sidebar_dashboard . '.php'); ?>
                </div>
                <div class="col-xs-10  col-sm-10 col-md-9 content-wrap">
                    <!-- Profil -->
                    <div class="profil">

                        <h2 class="profil-title">Dashboard</h2><br/>

                        <div class="profil-wrap">
                            <?php 
                                if($count_pending > 0) {
                            ?>
                            <div class = "alert alert-danger">
                            Warning !
                            <p style="color:#666">
                                Anda memiliki <a href="<?=base_url()?>dashboard/keranjang"><span class="profil-title"><?=$count_pending?></span></a> orderan yang pending, Silahkan lakukan pembayaran dan konfirmasikan segera.
                            </p>
                            </div>
                            <?php } ?>
                            
                            <?php 
                            foreach ($data_proccess as $val) {
                                if($val['status_pesanan'] == 2) {
                                ?>
                            <div class="alert alert-warning">
                            <!-- <a title="close" aria-label="close" data-dismiss="alert" class="close" href="<?=base_url()?>dashboard/is_view_proccess/<?=$val['kd_pesanan']?>">×</a>     -->
                            Barang Sedang di proses !
                            <p style="color:#666">
                                Nomer Order <a href="<?=base_url()?>dashboard/keranjang_detail/<?=$val['kd_pesanan']?>"><span class="profil-title"><?=$val['kd_pesanan']?></span></a> sedang di proses, Kami akan memberitahu anda segera!
                            </p>
                            </div>
                                <?php } } ?>        
                            
                            <?php 
                            foreach ($data_delivery as $val) {
                                if($val['status_pesanan'] == 3) {
                                ?>
                            <div class = "alert alert-info">
                                <!-- <a title="close" aria-label="close" data-dismiss="alert" class="close" href="<?=base_url()?>dashboard/is_view_delivery/<?=$val['status_pesanan']?>">×</a> -->
                            Barang Sedang Dikirim !
                            <p style="color:#666">
                                Nomer Order <a href="<?=base_url()?>dashboard/keranjang_detail/<?=$val['kd_pesanan']?>"><span class="profil-title"><?=$val['kd_pesanan']?></span></a> Kami telah melakukan pengiriman barang anda dan silahkan menunggu :).
                            </p>
                            </div>
                                <?php } } ?>   

                                 <?php 
                            foreach ($data_complete as $val) {
                                if($val['status_pesanan'] == 4) {
                                ?>
                            <div class = "alert alert-info">
                                <!-- <a title="close" aria-label="close" data-dismiss="alert" class="close" href="<?=base_url()?>dashboard/is_view_delivery/<?=$val['status_pesanan']?>">×</a> -->
                            Pesanan Anda Sudah Selesai!
                            <p style="color:#666">
                                Nomer Order <a href="<?=base_url()?>dashboard/keranjang_detail/<?=$val['kd_pesanan']?>"><span class="profil-title"><?=$val['kd_pesanan']?></span></a> Pesanan anda sudah selesai, Terimakasih sudah belanja di Kami.
                            </p>
                            </div>
                                <?php } } ?>  
                        </div>

                    </div>						</div>
            </div>

        </div>
    </div>
</div>   
</section>
