<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Data Ongkos Kirim
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?=base_url()?>backend"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li class="active">Data Ongkos Kirim</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Data Master Ongkos Kirim</h3><br/><br/>
                    <div class="col-md-2">
                        <a href="<?=base_url()?>backend/add/ongkir" class="btn btn-block btn-primary">Tambah Ongkos Kirim</a>
                    </div>
                    <div class="pull-right">
                        <!-- <form action="<?= base_url() ?>excel/getexcel/product" id="formReport_<?=$module?>" method="post">
                            <span>Sort By : </span>
                            FROM &nbsp;&nbsp; <input type="text" required="required" name="tanggal1" class="datepicker form-sortby" placeholder="From Tanggal" value="" id="tanggal"  autocomplete="off"> 
                            &nbsp;TO &nbsp;&nbsp; <input type="text" required="required" name="tanggal2" class="datepicker form-sortby" placeholder="To Tanggal" value="" id="tanggal" autocomplete="off">
                            <input type="submit" class="btn btn-primary" value="Export Excel"/>
                        </form> -->
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                     <table id="example1" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th class="text-center">Opsi</th>
                                <th class="text-center">Nama Kota</th>
                                <th class="text-center">Harga Ongkir</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            $no = 1;
                                foreach($show_data as $val) {
                            ?>
                            <tr>
                                <td>
                                    <?=$no;?>
                                </td>
                                <td class="text-center">
                                    <a href="<?=base_url()?>backend/view/ongkir/<?=$val['id_ongkir']?>">
                                        <span title="View" aria-hidden="true" class="glyphicon glyphicon-eye-open"></span> &nbsp;&nbsp;&nbsp;
                                    </a>
                                    <a href="<?=base_url()?>backend/edit/ongkir/<?=$val['id_ongkir']?>">
                                        <span title="Edit" aria-hidden="true" class="glyphicon glyphicon-edit"></span>  &nbsp;&nbsp;&nbsp;
                                    </a>
                                    <!-- <a onclick="return confirm('Apakah anda yakin ingin menghapus data ini ? ')" href="<?=base_url()?>proccess_backend/processremove/ongkir/<?=$val['id_ongkir']?>">
                                        <span title="Remove" aria-hidden="true" class="fa fa-trash"></span> 
                                    </a> -->
                                </td>
                                <td class="text-center">
                                    <?=$val['nama_kota']?>
                                </td>
                               <td class="text-center">
                                    <?=$val['harga_ongkir']?> 
                                </td>
                            </tr>
                            <?php $no++; } ?>
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
