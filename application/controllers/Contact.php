<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');

Class Contact extends CI_Controller {

    public function __construct() {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
    }

    public function index($params = null) {

      
        $data['menu'] = 'menu';
        $data['params'] = $params;
        $data['module'] = 'contact';
        $this->load->view('template', $data);
    }

    public function processContact($params = null) {
        $getsession_id = $this->session->userdata('id');
        $getsession_logged_in = $this->session->userdata('logged_in');
        
        $status = null;
        $msg = null;
        
        if ($params == 'help') {
            $name = $this->input->post('name');
            $email = $this->input->post('email');
            $subject = $this->input->post('subject');
            $message = $this->input->post('message');
            $ip = $this->input->ip_address();
            $proxy = xhttpdata('proxy');
            $ua = xhttpdata('ua');
            $date_add = date('Y-m-d H:i:s');


            $table = 'contact';
            $data = Array(
                'id_ccontact' => 1,
                'contact_fullname' => $name,
                'contact_email' => $email,
                'contact_subject' => $subject,
                'contact_message' => $message,
                'ip' => $ip,
                'proxy' => $proxy,
                'ua' => $ua,
                'date_add' => $date_add
            );
            $insert = $this->db->insert($table, $data);

            if ($insert) {
                echo 1;
            } else {
                echo 0;
            }
        } else if ($params == 'complain') {
            // Jika si user belom login
            if(empty($getsession_logged_in)) {
                $status = 0;
                $msg = 'Ups! Harap, login terlebih dahulu untuk mengisi form complain.';
                
            } else {
                $no_order = $this->input->post('no_order');
                $name = $this->input->post('name');
                $email = $this->input->post('email');
                $subject = $this->input->post('subject');
                $message = $this->input->post('message');
                $ip = $this->input->ip_address();
                $proxy = xhttpdata('proxy');
                $ua = xhttpdata('ua');
                $date_add = date('Y-m-d H:i:s');


                $table = 'contact';
                $data = Array(
                    'id_ccontact' => 2,
                    'id_user' => $getsession_id,
                    'no_order' => $no_order,
                    'contact_fullname' => $name,
                    'contact_email' => $email,
                    'contact_subject' => $subject,
                    'contact_message' => $message,
                    'ip' => $ip,
                    'proxy' => $proxy,
                    'ua' => $ua,
                    'date_add' => $date_add
                );
                
                $insert = $this->db->insert($table, $data);
                if($insert) {
                    $status = 1;
                    $msg = 'Thank you for your response. We will get back to you soon.';
                }
            }
            
            
            
          echo json_encode(
                        Array(
                            'status' => $status,
                            'msg' => $msg
                        )
                    );
        }
    }

}
