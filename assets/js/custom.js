jQuery(window).scroll(function () {
  if (jQuery(this).scrollTop() > 100) {
    jQuery("#scrollToTop").fadeIn();
  } else {
    jQuery("#scrollToTop").fadeOut();
  }
});

//Click event to scroll to top
jQuery("#scrollToTop").click(function () {
  jQuery("html, body").animate({ scrollTop: 0 }, 800);
  return false;
});

/* ====== Ajax Form Contact Help ===== */
function proccesscontact() {
  var fullname = $(".fullname").val();
  var email = $(".email").val();
  var subject = $(".subject").val();
  var message = $(".message").val();

  if (fullname == "") {
    $(".opsi-info")
      .html("Nama Lengkap Harap Di isi")
      .delay(2000)
      .fadeOut(500, function () {
        $(".opsi-info").html("");
        $(".opsi-info").show();
      });
  } else if (email == "") {
    $(".opsi-info")
      .html("Email Harap Di isi")
      .delay(2000)
      .fadeOut(500, function () {
        $(".opsi-info").html("");
        $(".opsi-info").show();
      });
  } else if (subject == "") {
    $(".opsi-info")
      .html("Subject Harap Di Isi")
      .delay(2000)
      .fadeOut(500, function () {
        $(".opsi-info").html("");
        $(".opsi-info").show();
      });
  } else if (message == "") {
    $(".opsi-info")
      .html("Message Harap Di Isi")
      .delay(2000)
      .fadeOut(500, function () {
        $(".opsi-info").html("");
        $(".opsi-info").show();
      });
  } else {
    $.post(
      host + "informasi/processContact",
      $("#contact-form").serializeArray(),
      function (data) {
        if (data == 1) {
          swal("Thank you", "We will get back to you soon.", "success");
          $("#contact-form")[0].reset();
        } else {
          alert("Failed");
        }
      }
    );
  }
}


$(function () {
  $(document).ready(function () {
    
      /* ===== Change Ongkos Kirim ===== */
      $("#ongkos_kirim").on('change', function () {
        var id_ongkir = $(this).val();
        var grand_total;

        $.ajax({
          type: "POST",
          dataType: "json",
          url: host + "ajax/ongkos_kirim",
          data: {
            'id_ongkir': id_ongkir
          },
          success: function (data) {
            $("#txt_ongkos_kirim").text(convertToRupiah(data.ongkos_kirim) + ".00")
            grand_total = parseInt($("#ongkir_val").val()) + parseInt(data.ongkos_kirim);

            $("#txt_grand_total").text(convertToRupiah(grand_total) + ".00");
          }
        });
      });

    /* ====== Ajax Form Contact Complain ===== */
    $(".contact-complainAjax").submit(function (e) {
      e.preventDefault();
      //alert('test');
      $.ajax({
        type: "POST",
        dataType: "json",
        url: host + "contact/processContact/complain",
        data: $(".contact-complainAjax").serialize(), // serializes the form's elements.
        success: function (data) {
          if (data.status == 0) {
            swal("Ups", data.msg);
          } else if (data.status == 1) {
            swal("Thank you", "We will get back to you soon.", "success");
            $(".contact-complainAjax")[0].reset();
          }
        }
      });
    });
    /* ====== Ajax Form Ubah Password Dashboard ===== */
    $("#Form-ChangePasswordId").submit(function (e) {
      e.preventDefault();
      //alert('test');
      $.ajax({
        type: "POST",
        dataType: "json",
        url: host + "dashboard/ProccessProfile_Edit/ubah_password",
        data: $("#Form-ChangePasswordId").serialize(), // serializes the form's elements.
        success: function (data) {
          if (data.status == "password_tak_sama") {
            swal("", data.msg);
          } else if (data.status == "password_konfirmasi_tak_sama") {
            swal("", data.msg);
          } else if (data.status == "berhasil") {
            swal("Berhasil", data.msg, "success");
            $("#Form-ChangePasswordId")[0].reset();
          }
        }
      });
    });

    /* ====== Ajax Form Ubah Password FPassword ===== */
    $("#form-ufpassword").submit(function (e) {
      e.preventDefault();
      //alert('test');
      $.ajax({
        type: "POST",
        dataType: "json",
        url: host + "fpassword/do_ubahfpassword",
        data: $("#form-ufpassword").serialize(), // serializes the form's elements.
        success: function (data) {
          if (data.status == "password_tak_sama") {
            swal("", data.msg);
          } else if (data.status == "password_konfirmasi_tak_sama") {
            swal("", data.msg);
          } else if (data.status == "berhasil") {
            swal("Anda Berhasil Ubah Password", data.msg, "success");
            $("#form-ufpassword")[0].reset();
          }
        }
      });
    });

    $("#form-registerid").submit(function (e) {
      e.preventDefault();
      if (confirm('Apakah data Anda sudah benar?')) {
        $.ajax({
          type: "POST",
          dataType: "json",
          url: host + "register/do_register",
          data: $("#form-registerid").serialize(), // serializes the form's elements.
          success: function (data) {
            if (data.status == 0) {
              swal("", data.msg);
            } else if (data.status == 1) {
              swal(
                { title: "SUCCESS", text: data.msg, type: "success" },
                function () {
                  window.location = host + "register";
                }
              );
            }
          }
        });
      }
    });

    $("#form-loginid").submit(function (e) {
      e.preventDefault();
      $.ajax({
        type: "POST",
        dataType: "json",
        url: host + "register/do_login",
        data: $("#form-loginid").serialize(),
        success: function (data) {
          console.log(data.status);
          if (data.status == 0) {
            swal("", data.msg);
          } else if (data.status == 1) {
            window.location = host + "dashboard/index";
          } else if (data.status == 3) {
            window.location = host + "product/cart";
          } else if (data.status == "not_actived") {
            swal("Ups..", data.msg, "error");
          }
        }
      });
    });

    /* ====== Confirmation Payment ===== */
    $(".form-confirmid").submit(function (e) {
      var formData = new FormData($(this)[0]);
      e.preventDefault();
      $.ajax({
        url: host + "informasi/proccessConfirmPayment",
        type: "POST",
        dataType: "json",
        data: formData,
        async: false,
        success: function (data) {
          if (data.status == "not_valid_noorder") {
            swal("Ups..", data.msg, "error");
          } else if (data.status == "success") {
            swal(
              { title: "Success", text: data.msg, type: "success" },
              function () {
                $(".form-confirmid")[0].reset();
              }
            );
          } else if (data.status == "failed") {
            swal("Ups..", data.msg, "error");
          }
        },
        cache: false,
        contentType: false,
        processData: false
      });
    });

    /* ====== Retur Produk ===== */
    $(".form-returids").submit(function (e) {
      var formData = new FormData($(this)[0]);
      e.preventDefault();

      $.ajax({
        url: host + "dashboard/proccessRetur",
        type: "POST",
        dataType: "json",
        data: formData,
        async: false,
        success: function (data) {
          if (data.status == "not_valid_noorder") {
            swal("Ups..", data.msg, "error");
          } else if (data.status == "success") {
            swal(
              { title: "Success", text: data.msg, type: "success" },
              function () {
                $(".form-returid")[0].reset();
              }
            );
          } else if (data.status == "failed") {
            swal("Ups..", data.msg, "error");
          }
        },
        cache: false,
        contentType: false,
        processData: false
      });
    });

    /* ====== Ajax Forgot Password ===== */
    $("#form-forgotid").submit(function (e) {
      e.preventDefault();
      $.ajax({
        type: "POST",
        dataType: "json",
        url: host + "register/do_forgot_password",
        data: $("#form-forgotid").serialize(),
        success: function (data) {
          if (data.status == "not_valid_email") {
            swal("Ups", data.msg, "error");
          } else if (data.status == "valid_email") {
            swal("Success", data.msg, "success");
            $("#form-forgotid")[0].reset();
          }
        }
      });
    });

    /* ====== Ajax Form Testimonial ===== */
    $("#formTestimoni").submit(function (e) {
      e.preventDefault();

      $.ajax({
        type: "POST",
        dataType: "json",
        url: host + "dashboard/proccessTestimoni",
        data: $("#formTestimoni").serialize(), // serializes the form's elements.
        success: function (data) {
          if (data.status == "cant_testi") {
            swal("Warning", data.msg);
          } else if (data.status == "success") {
            swal("Berhasil", data.msg, "success");
            $("#formTestimoni")[0].reset();
          }
        }
      });
    });
  });
});

/* ==== Bootstrap Datepicker ===== */
$(function () {
  $(".datepicker").datepicker({
    format: "yyyy-mm-dd"
  });
});
/* ==== Function isNumber ===== */
function isNumber(evt) {
  evt = evt ? evt : window.event;
  var charCode = evt.which ? evt.which : evt.keyCode;
  if (charCode > 31 && (charCode < 48 || charCode > 57)) {
    return false;
  }
  return true;
}

function convertToRupiah(angka) {
  var rupiah = '';
  var angkarev = angka.toString().split('').reverse().join('');
  for (var i = 0; i < angkarev.length; i++) if (i % 3 == 0) rupiah += angkarev.substr(i, 3) + '.';
  return 'Rp. ' + rupiah.split('', rupiah.length - 1).reverse().join('');
}