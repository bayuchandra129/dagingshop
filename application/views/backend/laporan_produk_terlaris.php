<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Data Laporan Produk Terlaris
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?=base_url()?>backend"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li class="active">Data Laporan Produk Terlaris</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Data Laporan Produk Terlaris</h3><br/><br/>
                     <div class="pull-right">
                    <form class="form-inline" method="post" action="<?= base_url() ?>excel/getexcel/laporan_produk_terlaris" autocomplete="off">
                        <div class="form-group">
                            <label for="exampleInputName2">Dari Tanggal</label>
                            <input type="text" name="from_date" require class="form-control datepicker"  placeholder="Dari Tanggal">
                        </div> &nbsp;&nbsp;&nbsp;
                        <div class="form-group">
                            <label for="exampleInputEmail2">Sampai Tanggal</label>
                            <input type="text" name="to_date" require class="form-control datepicker"  placeholder="Sampai Tanggal">
                        </div> &nbsp;&nbsp;&nbsp;
                    <button type="submit" class="btn btn-success pull-right">EXPORT LAPORAN</button> 
                    </form>
                    </div>
                    <!-- <a href="<?= base_url() ?>excel/getexcel/laporan_produk_terlaris" class="btn btn-success pull-right">EXPORT EXCEL</a> -->
                </div><!-- /.box-header -->
                <div class="box-body">
                     <table id="example1" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th class="text-center">Kode Produk</th>
                                <th class="text-center">Nama Produk</th>
                                <th class="text-center">Total Ter-Jual</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            $no = 1;
                                foreach($show_data as $val) {
                            ?>
                            <tr>
                                <td>
                                    <?=$no;?>
                                </td>
                                <td style="text-align:center;">  
                                    <a href="<?=base_url()?>backend/view/product/<?=$val['kd_produk']?>">                      
                                    <?=$val['kd_produk']?>
                                    </a>
                                </td>
                                <td class="text-center">
                                    <?=$val['nm_produk']?>
                                </td>
                                <td class="text-center">
                                    <?=$val['total_jual']?>
                                </td>
                            </tr>
                            <?php $no++; } ?>
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
