<div class="left-nav">
    <ul>
        <li class="<?= ($module == 'dashboard' ? 'active' : '') ?>"><a class="lm-dashboard" href="<?= base_url() ?>dashboard/index">Dashboard</a></li>
        <li class="<?= ($module == 'profile' || $module == 'ubah_profile' ? 'active' : '') ?>"><a href="<?= base_url() ?>dashboard/profile" class="lm-profil">Profil</a></li>
        <li class="<?= ($module == 'keranjang_dashboard' || $module == 'keranjang_detail_dashboard' ? 'active' : '')?>"><a href="<?= base_url() ?>dashboard/keranjang" class="lm-pesanan">Pesanan Saya</a></li>
        <li class="<?= ($module == 'konfirmasi_pembayaran' ? 'active' : '')?>"><a href="<?=base_url()?>informasi/konfirmasipembayaran" class="lm-pesanan">Konfirmasi Pembayaran</a></li>
        <!-- <li class="<?= ($module == 'retur_dashboard' ? 'active' : '')?>"><a href="<?=base_url()?>dashboard/retur" class="lm-pesanan">Retur</a></li> -->
        <li class=""><a href="<?= base_url() ?>register/do_logout" class="lm-signout-alias">Sign Out</a></li>
    </ul>
</div>