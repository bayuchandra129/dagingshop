<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Data Pelanggan
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= base_url() ?>backend"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li class="active">Data Pelanggan</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">

                <div class="box-header">
                    <h3 class="box-title">Data Master Pelanggan</h3><br/><br/>
                    <div class="pull-right">
                        <!-- <form action="<?= base_url() ?>excel/getexcel/user" id="formReport_<?=$module?>" method="post">
                            <span>Sort By : </span>
                            FROM &nbsp;&nbsp; <input type="text" required="required" name="tanggal1" class="datepicker form-sortby" placeholder="From Tanggal" value="" id="tanggal"  autocomplete="off"> 
                            &nbsp;TO &nbsp;&nbsp; <input type="text" required="required" name="tanggal2" class="datepicker form-sortby" placeholder="To Tanggal" value="" id="tanggal" autocomplete="off">
                            <input type="submit" class="btn btn-primary" value="Export Excel"/>
                        </form> -->
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Opsi</th>
                                <th class="text-center">Nama Lengkap</th>
                                <th class="text-center">No. Handphone</th>
                                <th class="text-center">Email</th>
                                <th class="text-center">Provinsi</th>
                                <th class="text-center">Alamat Lengkap</th>
                                <th class="text-center">Tanggal Daftar</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            foreach ($show_data as $val) {
                                ?>
                                <tr>
                                    <td>
                                        <?= $no; ?>
                                    </td>
                                    <td class="text-center">
                                        <a href="<?= base_url() ?>backend/view/user/<?= $val['id_plg'] ?>">
                                            <span title="View" aria-hidden="true" class="glyphicon glyphicon-eye-open"></span> &nbsp;&nbsp;&nbsp;
                                        </a>
                                    </td>
                                    <td><?= $val['nm_plg'] ?></td>
                                    <td><?= $val['tlp_plg'] ?></td>
                                    <td><?= $val['email_plg'] ?></td>
                                    <td><?= $val['kota'] ?></td>
                                    <td><?= $val['alamat'] ?>, <?=$val['kode_pos']?></td>
                                    <td><?= $val['date_register'] ?></td>
                                </tr>
                                <?php $no++;
                            } ?>
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
<script type="text/javascript">
    var module = '<?=$module?>';
</script>