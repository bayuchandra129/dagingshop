<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

Class Register extends CI_Controller {

    public function __construct() {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
    }

    public function index($params = null) {
        $getsession_logged = $this->session->userdata('logged_plg');
        if (!empty($getsession_logged)) {
            redirect(base_url() . 'dashboard');
        }
        $data['show_kategori_menu'] = $this->core_base->getdata('kategori', null, Array('nm_kategori','DESC'));
        $data['ongkir'] = $this->core_base->getdata('ongkir', null, Array('nama_kota','ASC'));
        $data['params'] = $params;
        $data['menu'] = 'menu';
        $data['module'] = 'register';
        $this->load->view('template', $data);
    }
    

    /* Proses Register User */

    public function do_register() {
        


        $name = $this->input->post('nama');
        $email = $this->input->post('alamat_email');
        $password = sha1(md5($this->input->post('password')));
        $nomor_hp = $this->input->post('nomor_hp');
        $alamat_domisili = $this->input->post('alamat_domisili');
        $kota = $this->input->post('kota');
        $kodepos = $this->input->post('kodepos');
        $date_register = date('Y-m-d H:i:s');


        /** === Pengiriman === **/
        $ongkos_kirim = $this->input->post('ongkos_kirim');
        $alamat_pengiriman = $this->input->post('alamat_pengiriman');
        $catatan = $this->input->post('catatan');
    
       
        if (empty($this->cart->contents())) {
            $data = Array(
                'pass_plg' => $password,
                'nm_plg' => $name,
                'email_plg' => $email,
                'tlp_plg' => $nomor_hp,
                'alamat' => $alamat_domisili,
                'kota' => $kota,
                'kode_pos' => $kodepos,
                'date_register' => $date_register
            );

            // cek email 
            $cek_email = $this->core_base->countdata('pelanggan', Array('email_plg' => $email));
            //kalo email lebih dari 0
            if ($cek_email > 0) {
                $status = 0;
                $msg = 'Email yang anda gunakan sudah terpakai';
            } else {
                $insert = $this->core_base->insertdata('pelanggan', $data);
                if ($insert) {
                    $status = 1;
                    $msg = 'Anda berhasil mendaftar';
                }
            }
        } else {
            $data = Array(
                'pass_plg' => $password,
                'nm_plg' => $name,
                'email_plg' => $email,
                'tlp_plg' => $nomor_hp,
                'alamat' => $alamat_domisili,
                'kota' => $kota,
                'kode_pos' => $kodepos,
                'date_register' => $date_register
            );

            // cek email 
            $cek_email = $this->core_base->countdata('pelanggan', Array('email_plg' => $email));
            //kalo email lebih dari 0
            if ($cek_email > 0) {
                $status = 0;
                $msg = 'Email yang anda gunakan sudah terpakai';
            } else {
                // email belum terdaftar, makan insert table user
                $insert = $this->core_base->insertdata('pelanggan', $data);
                if ($insert) {
                    $get_id_cust = $this->core_base->getdata('pelanggan', Array('email_plg' => $email));
                    $no_order = 'NO-' . acakangka(5) . '-AL';

                    $order = array(
                        'kd_pesanan' => $no_order,
                        'kd_plg' => $get_id_cust['id_plg'],
                        'ongkir_id' => $ongkos_kirim,
                        'alamat_kirim' => $alamat_pengiriman,
                        'catatan' => $catatan,
                        'tgl_pesanan' => date('Y-m-d H:i:s'),
                        'status_pesanan' => 1
                    );

                    $ord_id = $this->core_base->insertdata('pesanan', $order);

                    if ($ord_id) {
                        $get_id_purchase = $this->core_base->getdata('pesanan', Array('kd_pesanan' => $no_order));
                        if ($cart = $this->cart->contents()):
                            foreach ($cart as $item):
                                $order_detail = array(
                                    'kd_pesanan' => $get_id_purchase['kd_pesanan'],
                                    'kd_produk' => $item['id'],
                                    'qty' => $item['qty'],
                                    'harga_jual' => $item['price']
                                );              

                                // Pengurangan Stok
                                $get_produk = $this->core_base->getdata('produk', Array('kd_produk'=>$item['id']));
                                
                                $stok_old = $get_produk['stok'];
                                $stok_now = $stok_old - $item['qty'];
                                $data_stok_produk = Array(
                                                    'stok' => $stok_now
                                                    );
                                $update_stok_produk = $this->core_base->updatedata('produk', 
                                                                        $data_stok_produk,
                                                                        Array('kd_produk'=> $item['id']));

                                
                                // Insert product imformation with order detail, store in cart also store in database. 

                                $cust_id = $this->core_base->insertdata('detil_pesanan', $order_detail);
                                
                            endforeach;
                                 
                            $this->cart->destroy();
                        endif;
                         $status = 1;
                         $msg = 'Selamat Data Pesanan anda sudah kami terima, silahkan melakukan pembayaran!';
                    }

                    
                }
            }
        }

        echo json_encode(Array('status' => $status, 'msg' => $msg));
    }

    function do_actived($email) {

        $insert = $this->core_base->updatedata('pelanggan', Array('aktivasi'=>1), Array('email_plg'=>$email));
    
        if($insert) {
            echo "<script>alert('Selamat, Akun anda berhasil di aktivasi!');document.location='".base_url()."register'</script>";
        }
    }

    function do_login() {
        $username = $this->input->post('user_email');
        $password = sha1(md5($this->input->post('user_password')));

        $cek_akun = $this->core_base->ceklogin($username, $password);

        if ($cek_akun == TRUE) {
            // if($cek_akun[0]['aktivasi'] == 1) {
                $sess_array = Array(
                    'id' => $cek_akun[0]['id_plg'],
                    'fullname' => $cek_akun[0]['nm_plg'],
                    'email' => $cek_akun[0]['email_plg'],
                    'logged_plg' => 1
                );
                
                $cart = $this->session->userdata('cart_contents');
                if(empty($cart)) {
                    $session = $this->session->set_userdata($sess_array);
                    $status = 1;
                    $msg = '';
                } else {
                    $session = $this->session->set_userdata($sess_array);
                    $status = 3;
                    $msg = '';
                }
            // } else {
            //     $status = 'not_actived';
            //     $msg = 'Login gagal! Akun anda belum terverifikasi, Silahkan Cek Email anda dan Silahkan verifikasi dahulu!';     
            // }
        } else {
            $status = 0;
            $msg = 'Login gagal! Silahkan periksa kembali Username atau Password Anda';
        }

        echo json_encode(Array('status' => $status, 'msg' => $msg));
    }

    function do_forgot_password() {
        $email = $this->input->post('user_email');
        $password_new = sha1(md5($this->input->post('password_new')));

        $cek_email = $this->core_base->countdata('pelanggan', Array('email_plg' => $email));

        if ($cek_email < 1) {
            $status = 'not_valid_email';
            $msg = 'Email yang anda masukan tidak valid';
        } else if ($cek_email > 0) {
            

            $update = $this->core_base->updatedata('pelanggan', Array('pass_plg' => $password_new), Array('email_plg' => $email));
            if($update == TRUE) {
                $status = 'valid_email';
                $msg = 'Password Baru anda sudah Berhasil yang Terbaru!';
            }
        }

        echo json_encode(Array('status' => $status, 'msg' => $msg));
    }

    function do_logout() {
        session_destroy();
        redirect(base_url() . 'register');
    }

}
