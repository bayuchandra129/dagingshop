<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

Class Dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
        $getsession_logged = $this->session->userdata('id');
        
        if (empty($getsession_logged)) {
            session_destroy();
            redirect(base_url() . 'register');
        }
    }

    public function index() {

        $getsession_id = $this->session->userdata('id');
        $data['count_pending'] = $this->core_base->countdata('pesanan', Array('kd_plg' => $getsession_id, 'status_pesanan' => 1));
        $data['data_proccess'] = $this->core_base->getdata('pesanan', Array('kd_plg' => $getsession_id, 'status_pesanan' => 2), Array('tgl_pesanan', 'ASC'));
        $data['data_delivery'] = $this->core_base->getdata('pesanan', Array('kd_plg' => $getsession_id, 'status_pesanan' => 3), Array('tgl_pesanan', 'ASC'));
        $data['data_complete'] = $this->core_base->getdata('pesanan', Array('kd_plg' => $getsession_id, 'status_pesanan' => 4), Array('tgl_pesanan', 'ASC'));
        
        $data['show_kategori_menu'] = $this->core_base->getdata('kategori', null, Array('nm_kategori','DESC'));
        $data['menu'] = 'menu';
        $data['sidebar_dashboard'] = 'sidebar_dashboard';
        $data['module'] = 'dashboard';
        $this->load->view('template', $data);
    }

    public function profile($params = null) {
        $getsession_id = $this->session->userdata('id');
        $data['profile'] = $this->core_base->getdata('pelanggan', Array('id_plg' => $getsession_id));
        
        $data['show_kategori_menu'] = $this->core_base->getdata('kategori', null, Array('nm_kategori','DESC'));
        $data['menu'] = 'menu';
        $data['sidebar_dashboard'] = 'sidebar_dashboard';

        if ($params == null) {
            $data['module'] = 'profile';
        } else if ($params == 'ubah_profile') {
            $data['module'] = 'profile_ubah';
        } else if ($params == 'ubah_password') {
            $data['module'] = 'profile_password';
        }

        $this->load->view('template', $data);
    }

    public function keranjang($params = null) {
        $getsession_id = $this->session->userdata('id');
        $data['pesanan_saya'] = $this->core_base->getdata('pesanan', Array('kd_plg' => $getsession_id), Array('tgl_pesanan', 'DESC'), null);
        
        $data['show_kategori_menu'] = $this->core_base->getdata('kategori', null, Array('nm_kategori','DESC'));
        $data['menu'] = 'menu';
        $data['sidebar_dashboard'] = 'sidebar_dashboard';
        $data['module'] = 'keranjang_dashboard';

        $this->load->view('template', $data);
    }

    public function keranjang_detail($params = null) {
        $getsession_id = $this->session->userdata('id');
        
        $data['pesanan_saya'] = $this->core_base->getrelasi('relasi_purchase', $params);
        $data['pembayaran'] = $this->core_base->getdata('pembayaran', Array('kd_pesanan'=>$params));
        $data['pesanan'] = $this->core_base->getrelasi('relasi_pesanan_ongkir', $params);

        //** Detail Pesanan Sekaligus Ongkir **//
        $data['kota'] = $data['pesanan'][0]['nama_kota'];
        $data['alamat_kirim'] = $data['pesanan'][0]['alamat_kirim'];
        $data['catatan'] = $data['pesanan'][0]['catatan'];
        $data['tgl_pesanan'] = $data['pesanan'][0]['tgl_pesanan'];
        $data['harga_ongkir'] = $data['pesanan'][0]['harga_ongkir'];

        $data['kd_pembayaran'] = $data['pembayaran']['kd_pembayaran'];
        $data['total_bayar'] = $data['pembayaran']['total_bayar'];
        $data['foto_butki'] = $data['pembayaran']['foto_bukti'];
        $data['status_pembayaran'] = $data['pembayaran']['status_pembayaran'];

        $data['pengiriman'] = $this->core_base->getrelasi('relasi_pengiriman', $params);
        
        // print_r($data['pengiriman']);
        // exit();

        if(!empty($data['pengiriman'])) {
            $data['status_kirim'] = $data['pengiriman'][0]['status_kirim'];
            $data['nama_kurir'] = $data['pengiriman'][0]['nama_kurir'];
            $data['tgl_sj'] = $data['pengiriman'][0]['tgl_sj'];
            $data['alamat_pengiriman'] = $data['pengiriman'][0]['alamat_kirim'];
        } else {
            $data['status_kirim'] = null;
            $data['nama_kurir'] = null;
            $data['tgl_sj'] = null;
            $data['alamat_pengiriman'] = null;
        }
       

        $data['show_kategori_menu'] = $this->core_base->getdata('kategori', null, Array('nm_kategori','DESC'));
        $data['menu'] = 'menu';
        $data['no_order'] = $params;
        $data['sidebar_dashboard'] = 'sidebar_dashboard';
        $data['module'] = 'keranjang_detail_dashboard';

        $this->load->view('template', $data);
    }

    public function retur() {
        $data['show_kategori_menu'] = $this->core_base->getdata('kategori', null, Array('nm_kategori','DESC'));
        $data['menu'] = 'menu';
        $data['sidebar_dashboard'] = 'sidebar_dashboard';
        $data['module'] = 'retur_dashboard';

        $this->load->view('template', $data);
    }

    public function is_view_proccess($no_order) {
        $update = $this->core_base->updatedata('pesanan', Array('is_view_proccess' => 1), Array('kd_pesanan' => $no_order));
        if ($update) {
            redirect(base_url() . 'dashboard');
        }
    }

    public function is_view_complete($no_order) {
        $update = $this->core_base->updatedata('pesanan', Array('is_view_complete' => 1), Array('kd_pesanan' => $no_order));
        if ($update) {
            redirect(base_url() . 'dashboard');
        }
    }

    public function proccessRetur() {
        // jika no order tidak valid
        $status = null;
        $msg = null;
        $kode_retur = 'R-'.acakangka(4).'-AL';
        $kode_pesanan = $this->input->post('kode_pesanan');
        
        $product = $this->input->post('product');
        $quantity = $this->input->post('quantity');
        $keterangan = $this->input->post('keterangan');

        $cek_no = $this->core_base->countdata('pesanan', Array('kd_pesanan' => $kode_pesanan));

            if ($cek_no < 1) {
                redirect(base_url('dashboard/retur?info=not_valid_noorder'));
                // $status = 'not_valid_noorder';
                // $msg = 'No Pesanan yang anda masukan tidak valid';
            } else {

                if (isset($_FILES["foto_bukti"]) && $_FILES["foto_bukti"]["error"] == UPLOAD_ERR_OK) {
                    $UploadDirectory = './repository/retur/';

                    //Is file size is less than allowed size.
                    if ($_FILES["foto_bukti"]["size"] > 2042880) {
                        $status = 'failed';
                        $msg = 'File size is too big!';
                    }

                    //allowed file type Server side check
                    switch (strtolower($_FILES['foto_bukti']['type'])) {
                        //allowed file types
                        case 'image/png':
                        case 'image/jpeg':
                        case 'image/pjpeg':
                            break;
                        default:
                            $status = 'failed';
                            $msg = 'Unsupported File!';
                    }

                    $File_Name = strtolower($_FILES['foto_bukti']['name']);
                    $File_Ext = substr($File_Name, strrpos($File_Name, '.')); //get file extention
                    $Random_Number = rand(0, 9999); //Random number to be added to name.
                    $NewFileName = $Random_Number . $File_Ext; //new file name
                    // jika user upload lebih dari 1, maka gagal upload

                    $move = move_uploaded_file($_FILES['foto_bukti']['tmp_name'], $UploadDirectory . $NewFileName);
                    if ($move) {
                        $retur = $this->core_base->insertdata('retur', Array(
                            'kd_retur' => $kode_retur,
                            'kd_pesanan' => $kode_pesanan,
                            'tgl_retur' => date('Y-m-d H:i:s'),
                            'gambar_retur' => $NewFileName 
                        ));

                        if ($retur) {
                            $retur_id_desc = $this->core_base->getdata('retur', null, Array('kd_retur','DESC'), Array(0,1));

                            //  echo $retur_id_desc[0]['kd_retur'];
                            //   exit();
              
                              foreach($product as $key => $val) {
                                  $data_detail_retur = Array(
                                                          'kd_retur' => $retur_id_desc[0]['kd_retur'],
                                                          'nama_produk' => $product[$key],
                                                          'quantity' => $quantity[$key],
                                                          'keterangan' => $keterangan[$key],
                                                      );
                                  $insert_detail_retur = $this->core_base->insertdata('detil_retur', $data_detail_retur);
                              }
                            redirect(base_url('dashboard/retur?info=success'));
                            // $status = 'success';
                            // $msg = 'Terimakasih sudah konfirmasi, informasi retur anda kami segara proses!';
                        }
                    }
                }
            }

            // echo json_encode(Array('status' => $status, 'msg' => $msg));
    }
    

    public function proccessTestimoni() {
        $kd_pesanan = $this->input->post('kd_pesanan');
        $testimoni = $this->input->post('isi_testimoni');

        // Jika Pesanan belum Status Complete user tidak bisa isi Testimoni

        $cek_pesanan = $this->core_base->getdata('pesanan', Array('kd_pesanan'=>$kd_pesanan));
        
        if($cek_pesanan['status_pesanan'] != 4) {
             $status = 'cant_testi';
            $msg = 'Anda tidak bisa melakukan testimoni, status pesanan Anda belum selesai!';
        } else {
            $data = Array(
                'kd_pesanan' => $kd_pesanan,
                'isi_testi' => $testimoni,
                'tgl_testi' => date('Y-m-d H:i:s')
            );
            
            $insert = $this->core_base->insertdata('testimoni', $data);
            if($insert) {
                $status = 'success';
                $msg = 'Terimakasih, sudah memberikan testimonial terhadap produk kami :)';
            }
        }

        

        
        echo json_encode(Array('status' => $status, 'msg' => $msg));
    }

    public function ProccessProfile_Edit($params = null) {
        $getsession_id = $this->session->userdata('id');
        $date = date('Y-m-d H:i:s');

        if ($params == 'ubah_profile') {
            $fullname = $this->input->post('user_fullname');
            $email = $this->input->post('user_email');
            $phone = $this->input->post('user_phone');
            $address = $this->input->post('user_address');
            $kota = $this->input->post('kota');
            $kode_pos = $this->input->post('kode_pos');

            if (isset($_FILES["user_photo"]) && $_FILES["user_photo"]["error"] == UPLOAD_ERR_OK) {
                $UploadDirectory = './repository/user_profile/';
                //Is file size is less than allowed size.
                if ($_FILES["user_photo"]["size"] > 1042880) {
                    echo 'File size is too big!';
                }

                //allowed file type Server side check
                switch (strtolower($_FILES['user_photo']['type'])) {
                    //allowed file types
                    case 'image/png':
                    case 'image/jpeg':
                    case 'image/pjpeg':
                        break;
                    default:
                        echo 'Unsupported File!';
                }

                $File_Name = strtolower($_FILES['user_photo']['name']);
                $File_Ext = substr($File_Name, strrpos($File_Name, '.')); //get file extention
                $Random_Number = rand(0, 9999); //Random number to be added to name.
                $NewFileName = $Random_Number . $File_Ext; //new file name
                // jika user upload lebih dari 1, maka gagal upload

                $move = move_uploaded_file($_FILES['user_photo']['tmp_name'], $UploadDirectory . $NewFileName);
                if ($move) {
                    $data = Array(
                        'photo_plg' => $NewFileName,
                        'nm_plg' => $fullname,
                        'email_plg' => $email,
                        'tlp_plg' => $phone,
                        'alamat' => $address,
                        'kota' => $kota,
                        'kode_pos' => $kode_pos,
                    );
                    $update = $this->core_base->updatedata('pelanggan', $data, Array('id_plg' => $getsession_id));
                    if ($update) {
                        redirect(base_url() . 'dashboard/profile/ubah_profile?=info=success');
                    }
                }
            } else {
                $data = Array(
                    'nm_plg' => $fullname,
                    'email_plg' => $email,
                    'tlp_plg' => $phone,
                    'alamat' => $address,
                    'kota' => $kota,
                    'kode_pos' => $kode_pos,
                );
                $update = $this->core_base->updatedata('pelanggan', $data, Array('id_plg' => $getsession_id));
                if ($update) {
                    redirect(base_url() . 'dashboard/profile/ubah_profile?=info=success');
                }
            }
        } else if ($params == 'ubah_password') {
            $password_lama = sha1(md5($this->input->post('password_lama')));
            $password_baru = sha1(md5($this->input->post('password_baru')));
            $konfirmasi_password = sha1(md5($this->input->post('konfirmasi_password')));

            // Cek password lama salah si user_id
            $cek_pass_lama = $this->core_base->countdata('pelanggan', Array('pass_plg' => $password_lama, 'id_plg' => $getsession_id));

            // Jika di count tidak sama atau data kurang lebih dari 1
            if ($cek_pass_lama < 1) {
                $status = 'password_tak_sama';
                $msg = 'Password lama anda tidak valid';
            } else { // Jika datanya lebih dari 0
                // cek password baru dan konfirmasi password
                if ($password_baru != $konfirmasi_password) {
                    $status = 'password_konfirmasi_tak_sama';
                    $msg = 'Password baru dan konfirmasi pasword tidak sama';
                } else { // jika password baru dan password konfirmasi sama, dia update
                    $data = Array('pass_plg' => $password_baru);
                    $update_password = $this->core_base->updatedata('pelanggan', $data, Array('id_plg' => $getsession_id));
                    if ($update_password) {
                        $status = 'berhasil';
                        $msg = 'Password baru anda berhasil di ubah';
                    }
                }
            }

            echo json_encode(Array('status' => $status, 'msg' => $msg));
        }
    }

}
