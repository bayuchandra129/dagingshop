<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Data Pelanggan Konfirmasi Pembayaran
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= base_url() ?>backend"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li class="active">Data Pelanggan Konfirmasi Pembayaran</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">

                <div class="box-header">
                    <h3 class="box-title">Data Pelanggan Konfirmasi Pembayaran</h3><br/><br/>

                </div><!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th class="text-center">Opsi</th>
                                <th class="text-center">No Pesanan</th>
                                <th class="text-center">Tanggal Pembayaran</th>
                                <th class="text-center">Nama Pemilik Bank</th>
                                <th class="text-center">No. Rekening</th>
                                <th class="text-center">Bank Pelanggan</th>
                                <th class="text-center">Total Bayar</th>
                                <th class="text-center">Bukti Transfer</th>
                                <th class="text-center">Status Pembayaran</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            foreach ($show_data as $val) {
                                ?>
                                <tr>
                                    <td>
                                        <?= $no ?>
                                    </td>
                                    <td style="text-align:center;">     
                                        <a href="<?=base_url()?>pdf/getcetak/invoice/<?=$val['kd_pesanan']?>">
                                            <span title="Cetak Invoice" aria-hidden="true" class="glyphicon glyphicon-print"></span>  &nbsp;&nbsp;&nbsp;
                                        </a>
                                        <a href="<?=base_url()?>backend/edit/pembayaran/<?=$val['kd_pembayaran']?>">
                                            <span title="Edit" aria-hidden="true" class="glyphicon glyphicon-edit"></span>  &nbsp;&nbsp;&nbsp;
                                        </a>
                                    </td>
                                    <td>
                                        <a href="<?= base_url() ?>backend/edit/user_order/<?= $val['kd_pesanan'] ?>"><?= $val['kd_pesanan'] ?></a>
                                    </td>
                                    <td>
                                        <?= $val['tgl_pembayaran'] ?>
                                    </td>
                                    <td>
                                        <?= $val['nm_pembank'] ?>
                                    </td>
                                    <td>
                                        <?= $val['no_rek'] ?>
                                    </td>
                                    <td>
                                        <?= $val['nm_bank'] ?>
                                    </td>
                                    <td>
                                        Rp. <?= (empty($val['total_bayar']) ? 0 : rupiah($val['total_bayar'])) ?>
                                    </td>
                                    <td>
                                        <?php
                                        if ($val['foto_bukti'] == "") {
                                            ?>
                                        <a class="fancybox" data-fancybox-group="gallery" href="<?= base_url() ?>assets/admin/img/not_available.jpg" >
                                            <img width="100" style="border:2px solid #000" src="<?= base_url() ?>assets/admin/img/not_available.jpg"/>
                                            </a>
                                        <?php } else { ?>
                                        <a class="fancybox" data-fancybox-group="gallery"  title="Bukti Transfer No. Pesanan : <?= $val['kd_pesanan'] ?>" href="<?= base_url() ?>repository/konfirmasi_pembayaran/<?= $val['foto_bukti'] ?>" >
                                            <img width="100" style="border:2px solid #000" src="<?= base_url() ?>repository/konfirmasi_pembayaran/<?= $val['foto_bukti'] ?>"/>
                                            </a>
                                        <?php } ?>
                                    </td>
                                    <td>
                                             <?= status_payment($val['status_pembayaran']) ?>
                                    </td>
                                </tr>
                                <?php $no++;
                            } ?>
                        </tbody>
                    </table>
                </div><!-- /.box-body -->

            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->

