<?php
if ($type == 'user') {
    ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Data Customer
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url() ?>backend/panel"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li><a href="<?= base_url() ?>backend/category_product"><i class="fa fa-dashboard"></i>Data Customer</a></li>
            <li class="active">View Customer</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">View Data Master Customer</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <dl class="dl-horizontal">
                            <dt>Nama Lengkap</dt>
                            <dd><?= $show_data['nm_plg'] ?></dd><br/>
                            <dt>Email</dt>
                            <dd><?= $show_data['email_plg'] ?></dd><br/>
                            <dt>Phone</dt>
                            <dd><?= $show_data['tlp_plg'] ?></dd><br/>
                            <dt>Alamat Lengkap</dt>
                            <dd><?= $show_data['alamat'] ?>, <?=$show_data['kota']?></dd><br/>
                            <dt>Tanggal Daftar</dt>
                            <dd><?= $show_data['date_register'] ?></dd><br/>
                        </dl>
                    </div>
                    <div class="box-footer">
                        <a href="<?= base_url() ?>backend/user" class="btn btn-default">Cancel</a>

                    </div>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
<?php } else if ($type == 'product') {
    ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Data Produk
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url() ?>backend/panel"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li><a href="<?= base_url() ?>backend/product"><i class="fa fa-dashboard"></i>Data Produk</a></li>
            <li class="active">View Produk</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">View Data Master Produk</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <dl class="dl-horizontal">
                            <dt>Kode Produk</dt>
                            <dd><?= $show_data[0]['kd_produk'] ?></dd><br/>
                            <dt>Nama Produk</dt>
                            <dd><?= $show_data[0]['nm_produk'] ?></dd><br/>
                            <dt>Stok</dt>
                            <dd><?= $show_data[0]['stok'] ?></dd><br/>
                            <dt>Diskon</dt>
                            <dd><?= $show_data[0]['diskon'] ?></dd><br/>
                            <dt>Kategori</dt>
                            <dd><?= $show_data[0]['nm_kategori'] ?></dd><br/>
                            <dt>Harga</dt>
                            <dd><?= $show_data[0]['harga'] ?></dd><br/>
                            <dt>Gambar Produk</dt>
                            <dd>
                                <?php
                                if ($show_data[0]['gambar_produk'] == "") {
                                    ?>
                                    <a class="fancybox" data-fancybox-group="gallery" href="<?= base_url() ?>assets/admin/img/not_available.jpg" >
                                        <img width="100" style="border:2px solid #000" src="<?= base_url() ?>assets/admin/img/not_available.jpg"/>
                                    </a>
                                <?php } else { ?>
                                    <a class="fancybox" data-fancybox-group="gallery"  title="<?= $show_data[0]['nm_produk'] ?>" href="<?= base_url() ?>repository/product/<?= $show_data[0]['gambar_produk'] ?>" >
                                        <img width="100" style="border:2px solid #000" src="<?= base_url() ?>repository/product/<?= $show_data[0]['gambar_produk'] ?>"/>
                                    </a>
                                <?php } ?>
                            </dd><br/>
                            <dt>Description Produk</dt>
                            <dd><?= $show_data[0]['deskripsi'] ?></dd><br/>
                        </dl>
                    </div>
                    <div class="box-footer">
                        <a href="<?= base_url() ?>backend/product" class="btn btn-default">Cancel</a>

                    </div>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
<?php } else if ($type == 'category_product') {
    ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Data Kategori Produk
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url() ?>backend/panel"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li><a href="<?= base_url() ?>backend/category_product"><i class="fa fa-dashboard"></i>Data Kategori Produk</a></li>
            <li class="active">View Kategori Produk</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">View Data Master Kategori Produk</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <dl class="dl-horizontal">
                            <dt>Image Kategori Produk</dt>
                            <dd>
                                <?php
                                if ($show_data['cproduct_pict'] == "") {
                                    ?>
                                    <a class="fancybox" data-fancybox-group="gallery" href="<?= base_url() ?>assets/admin/img/not_available.jpg" >
                                        <img width="100" style="border:2px solid #000" src="<?= base_url() ?>assets/admin/img/not_available.jpg"/>
                                    </a>
                                <?php } else { ?>
                                    <a class="fancybox" data-fancybox-group="gallery" title="<?= $show_data['cproduct_name'] ?>" href="<?= base_url() ?>repository/category_product/<?= $show_data['cproduct_pict'] ?>" >
                                        <img width="100" style="border:2px solid #000" src="<?= base_url() ?>repository/category_product/<?= $show_data['cproduct_pict'] ?>"/>
                                    </a>
                                <?php } ?>
                            </dd><br/>
                            <dt>Nama Kategori Produk</dt>
                            <dd><?= $show_data['cproduct_name'] ?></dd>
                        </dl>
                    </div>
                    <div class="box-footer">
                        <a href="<?= base_url() ?>backend/category_product" class="btn btn-default">Cancel</a>

                    </div>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
<?php }  else if ($type == 'ongkir') {
    ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Data Ongkir Kirim
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url() ?>backend/panel"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li><a href="<?= base_url() ?>backend/ongkir"><i class="fa fa-dashboard"></i>Data Ongkos Kirim</a></li>
            <li class="active">View Ongkos Kirim</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">View Data Master Ongkos Kirim</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <dl class="dl-horizontal">
                            <dt>Nama Kota</dt>
                            <dd><?= $show_data['nama_kota'] ?></dd>
                        </dl>
                        <dl class="dl-horizontal">
                            <dt>Harga Ongkir</dt>
                            <dd><?= $show_data['harga_ongkir'] ?></dd>
                        </dl>
                    </div>
                    <div class="box-footer">
                        <a href="<?= base_url() ?>backend/ongkir" class="btn btn-default">Cancel</a>

                    </div>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
<?php }  else if ($type == 'kurir') {
    ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Data Kurir
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url() ?>backend/panel"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li><a href="<?= base_url() ?>backend/kurir"><i class="fa fa-dashboard"></i>Data Kurir</a></li>
            <li class="active">View Kurir</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">View Data Master Kurir</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <dl class="dl-horizontal">
                            <dt>Nama </dt>
                            <dd><?= $show_data['nama_kurir'] ?></dd>
                        </dl>
                        <dl class="dl-horizontal">
                            <dt>Alamat </dt>
                            <dd><?= $show_data['alamat_kurir'] ?></dd>
                        </dl>
                        <dl class="dl-horizontal">
                            <dt>No. Telp</dt>
                            <dd><?= $show_data['no_telp_kurir'] ?></dd>
                        </dl>
                    </div>
                    <div class="box-footer">
                        <a href="<?= base_url() ?>backend/kurir" class="btn btn-default">Cancel</a>

                    </div>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
<?php }  else if ($type == 'staff') {
    ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Data Staff
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url() ?>backend/panel"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li><a href="<?= base_url() ?>backend/staff"><i class="fa fa-dashboard"></i>Data Staff</a></li>
            <li class="active">View Staff</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">View Data Staff</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <dl class="dl-horizontal">
                            <dt>Hak Akses</dt>
                            <dd><?= $show_data[0]['privilege_type'] ?></dd><br/>
                            <dt>Nama Staff</dt>
                            <dd><?= $show_data[0]['staff_fullname'] ?></dd><br/>
                            <dt>Username Staff</dt>
                            <dd><?= $show_data[0]['username'] ?></dd><br/>
                            <dt>Email Staff</dt>
                            <dd><?= $show_data[0]['staff_email'] ?></dd><br/>
                            <dt>Photo Staff</dt>
                            <dd>
                                <?php
                                if ($show_data[0]['staff_photo'] == "") {
                                    ?>
                                <a class="fancybox" data-fancybox-group="gallery" href="<?= base_url() ?>assets/admin/img/not_available.jpg" >
                                    <img width="100" style="border:2px solid #000" src="<?= base_url() ?>assets/admin/img/not_available.jpg"/>
                                    </a>
                                <?php } else { ?>
                                <a class="fancybox" data-fancybox-group="gallery" title="<?= $show_data[0]['staff_fullname'] ?>" href="<?= base_url() ?>repository/staff/<?= $show_data[0]['staff_photo'] ?>" >
                                    <img width="100" style="border:2px solid #000" src="<?= base_url() ?>repository/staff/<?= $show_data[0]['staff_photo'] ?>"/>
                                </a>
                                        <?php } ?>    
                            </dd><br/>
                            <dt>Phone Staff</dt>
                            <dd><?= $show_data[0]['staff_phone'] ?></dd><br/>
                            <dt>Address Staff</dt>
                            <dd><?= $show_data[0]['staff_address'] ?></dd><br/>
                            <dt>Gender Staff</dt>
                            <dd><?= stats_gender($show_data[0]['staff_gender']) ?></dd><br/>

                        </dl>
                    </div>
                    <div class="box-footer">
                        <a href="<?= base_url() ?>backend/staff" class="btn btn-default">Cancel</a>
                    </div>
                </div><!-- /.box --><br/>
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Ubah Password</h3>
                    </div><!-- /.box-header -->
                    <form action="<?= base_url() ?>proccess_backend/processupdate/change_password/<?= $show_data[0]['id_staff'] ?>" method="post" enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="form-group">
                                <label>Username : </label>
                                <input type="username" value="<?= $show_data[0]['username'] ?>" readonly="readonly" name="username"  class="form-control" placeholder="Enter password" required="required">
                            </div>
                            <div class="form-group">
                                <label>Password : </label>
                                <input type="password" name="password"  class="form-control" placeholder="Enter password" required="required">
                            </div>
                        </div>
                        <div class="box-footer">
                            <a href="<?= base_url() ?>backend/product" class="btn btn-default">Cancel</a>
                            <button class="btn btn-primary pull-right" type="submit">Edit</button>
                        </div>
                    </form>    
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
<?php } else if ($type == 'settings_banner') {
    ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Data Konfigurasi Banner
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url() ?>backend/panel"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li><a href="<?= base_url() ?>backend/settings_about"><i class="fa fa-dashboard"></i>Data Konfigurasi Banner</a></li>
            <li class="active">View Konfigurasi Banner</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">View Data Konfigurasi Banner</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <dl class="dl-horizontal">

                            <dt>Gambar Banner</dt>
                            <dd>
                                <?php
                                if ($show_data[0]['banner_pict'] == "") {
                                    ?>
                                <a class="fancybox" data-fancybox-group="gallery" href="<?= base_url() ?>assets/admin/img/not_available.jpg" >
                                    <img width="100" style="border:2px solid #000" src="<?= base_url() ?>assets/admin/img/not_available.jpg"/>
                                    </a>
                                <?php } else { ?>
                                <a class="fancybox" data-fancybox-group="gallery" title="<?=$show_data[0]['banner_title']?>" href="<?= base_url() ?>repository/banner/<?= $show_data[0]['banner_pict'] ?>" >
                                    <img width="100" style="border:2px solid #000" src="<?= base_url() ?>repository/banner/<?= $show_data[0]['banner_pict'] ?>"/>
                                    </a>
                                <?php } ?>    
                            </dd><br/>
                            <dt>Title</dt>
                            <dd><?= $show_data[0]['banner_title'] ?></dd><br/>
                            <dt>Description</dt>
                            <dd><?= $show_data[0]['banner_desc'] ?></dd><br/>
                            <dt>Nama Staff</dt>
                            <dd><?= $show_data[0]['staff_fullname'] ?></dd>
                        </dl>
                    </div>
                    <div class="box-footer">
                        <a href="<?= base_url() ?>backend/settings_banner" class="btn btn-default">Cancel</a>
                    </div>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
<?php } else if ($type == 'pengiriman') {
    ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Data Surat Jalan
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url() ?>backend/panel"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li><a href="<?= base_url() ?>backend/settings_terms_and_condition"><i class="fa fa-dashboard"></i>Data Surat Jalan</a></li>
            <li class="active">View Surat Jalan</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">View Data Surat Jalan</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="col-md-6">
                            <dl class="dl-horizontal">
                                <dt>No Surat Jalan</dt>
                                <dd><?= $show_data[0]['kd_sj'] ?></dd><br/>
                                <dt>No Order</dt>
                                <dd><?= $show_data[0]['pesanan_code'] ?></dd><br/>
                                
                                <dt>Tanggal Surat Jalan</dt>
                                <dd><?= formatDates($show_data[0]['tgl_sj']) ?></dd><br/>
                            </dl>
                        </div>
                        <div class="col-md-6">
                            <dl class="dl-horizontal">
                                <dt>Pengiriman Ke : </dt>
                                <dd></dd><br/>
                                <dt>Nama</dt>
                                <dd><?= $show_data[0]['nm_plg'] ?></dd><br/>
                                <dt>Alamat Kirim</dt>
                                <dd><?= $show_data[0]['alamat_kirim'] ?></dd><br/>
                                <dt>No. Handphone</dt>
                                <dd><?= $show_data[0]['tlp_plg'] ?></dd><br/>
                                
                            </dl>
                        </div>
                    </div>
                    <div class="box-footer">
                        <a href="<?= base_url() ?>backend/pengiriman" class="btn btn-default">Cancel</a>
                    </div>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
<?php } else if ($type == 'retur') {
    ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Data Retur
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url() ?>backend/panel"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li><a href="<?= base_url() ?>backend/settings_terms_and_condition"><i class="fa fa-dashboard"></i>Data Retur</a></li>
            <li class="active">View Retur</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">View Data Retur</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="col-md-6">
                            <dl class="dl-horizontal">
                                <dt>No Pesanan</dt>
                                <dd><?= $show_data[0]['kd_pesanan'] ?></dd><br/>
                                <dt>Kepada</dt>
                                <dd><?= $show_data[0]['nm_plg'] ?></dd><br/>
                                
                                <dt>Alamat</dt>
                                <dd><?= $show_data[0]['alamat'] ?></dd><br/>
                                <dt>Telp</dt>
                                <dd><?= $show_data[0]['tlp_plg'] ?></dd><br/>
                                <dt>Email</dt>
                                <dd><?= $show_data[0]['email_plg'] ?></dd><br/>
                                
                                <br/>
                            </dl>
                        </div>
                        <div class="col-md-6">
                            <dl class="dl-horizontal">
                                <dt>Pengiriman Ke : </dt>
                                <dd></dd><br/>
                                <dt>No Retur</dt>
                                <dd><?= $show_data[0]['kd_retur'] ?></dd><br/>
                                <dt>Tanggal Retur</dt>
                                <dd><?= formatDates($show_data[0]['tgl_retur']) ?></dd>
                                
                            </dl>
                        </div>
                        <table id="example1" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th class="text-center">Nama Produk</th>
                                <th class="text-center">Qty</th>
                                <th class="text-center">Keterangan Retru</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php 
                        $no =  1;
                            foreach($pesanan_saya as $val) {
                        ?>
                            <tr>
                                <td>
                                    <?=$no;?>
                                </td>
                                <td>
                                    <?=$val['nama_produk']?>
                                </td>
                                <td>
                                <?=$val['quantity']?>
                                </td>
                                <td>
                                <?=$val['keterangan']?>
                                </td>
                            </tr>
                            <?php $no++; } ?>
                        </tbody>
                    </table>
                    </div>
                    
                    <div class="box-footer">
                        <a href="<?= base_url() ?>backend/pengiriman" class="btn btn-default">Cancel</a>
                    </div>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
<?php } ?>
