<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Data Contact Us
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?=base_url()?>backend"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li class="active">Data Contact Us</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Data Master Contact Us</h3><br/><br/>
<!--                    <div class="col-md-2">
                        <a href="<?=base_url()?>backend/add/ourservice" class="btn btn-block btn-primary">Add Contact Us</a>
                    </div>-->
                </div><!-- /.box-header -->
                <div class="box-body">
                     <table id="example1" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th class="text-center">Opsi</th>
                                <th class="text-center">Nama Staff</th>
                                <th class="text-center">Full Name</th>
                                <th class="text-center">Email</th>
                                <th class="text-center">Subject</th>
                                <th class="text-center">Message</th>
                                <th class="text-center">Date Add</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            $no = 1;
                                foreach($show_data as $val) {
                            ?>
                            <tr>
                                <td>
                                    <?=$no;?>
                                </td>
                                <td class="text-center">
                                    <a href="<?=base_url()?>backend/view/contact/<?=$val['id_contact']?>">
                                        <span title="View" aria-hidden="true" class="glyphicon glyphicon-eye-open"></span> &nbsp;&nbsp;&nbsp;
                                    </a>
                                </td>
                                <td class="text-center">
                                    <?=$val['staff_fullname']?>
                                </td>
                                <td class="text-center">
                                    <?=$val['contact_fullname']?>
                                </td>
                                <td class="text-center">
                                    <?=$val['contact_email']?>
                                </td>
                                <td class="text-center">
                                    <?=$val['contact_subject']?>
                                </td>
                                <td class="text-center">
                                    <?=$val['contact_message']?>
                                </td>
                                <td class="text-center">
                                    <?=formatDate($val['date_add_contact'])?>
                                </td>
                            </tr>
                            <?php $no++; } ?>
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
