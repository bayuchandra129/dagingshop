<?php 
    // data grafik report laporan per bulan
    $data = $this->core_admin->get_data_projek_month();
    if(empty($data))
    {
        $bulan = null;
        $jumlah_bulanan = null;
    } else {
        foreach($data as $data){
            $bulan[] = $data->bulan;
            $jumlah_bulanan[] = $data->jumlah_bulanan;
        }
    }

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Administrator | PT. GLOBAL PRATAMA WIJAYA</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/admin.css">
        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="<?= base_url() ?>assets/admin/bootstrap.3.3.5/css/bootstrap.min.css">
        <!-- Bootstrap Datepicker CSS -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/bootstrap/bootstrap-calender/css/bootstrap-datepicker3.css" media="screen">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?= base_url() ?>assets/admin/font-awesome/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?= base_url() ?>assets/admin/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="<?= base_url() ?>assets/admin/css/skins/_all-skins.min.css">
        <!-- DataTables -->
        <link rel="stylesheet" href="<?= base_url() ?>assets/admin/plugins/datatables/dataTables.bootstrap.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="<?= base_url() ?>assets/admin/plugins/iCheck/flat/blue.css">
        <!-- Morris chart -->
        <link rel="stylesheet" href="<?= base_url() ?>assets/admin/plugins/morris/morris.css">
        <!-- jvectormap -->
        <link rel="stylesheet" href="<?= base_url() ?>assets/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
        <!-- Date Picker -->
        <link rel="stylesheet" href="<?= base_url() ?>assets/admin/plugins/datepicker/datepicker3.css">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="<?= base_url() ?>assets/admin/plugins/daterangepicker/daterangepicker-bs3.css">
        <!-- Select2 -->
        <link rel="stylesheet" href="<?= base_url() ?>assets/admin/plugins/select2/select2.min.css">
        <!-- Fancybox CSS -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/plugins/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />

        
        <!-- bootstrap wysihtml5 - text editor -->
        <link rel="stylesheet" href="<?= base_url() ?>assets/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
        <script src="<?= base_url() ?>assets/js/jquery-1.8.3.min.js"></script>
        <script src="<?= base_url() ?>assets/js/jquery-2.0.3.min.js"></script>
        <!-- Add jQuery library -->
        <!-- Morris.js charts -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        <script src="<?= base_url() ?>assets/admin/plugins/morris/morris.min.js"></script>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript">
            var host = '<?= base_url() ?>';
        </script>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">

            <header class="main-header">
                <!-- Logo -->
                <a href="index2.html" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><b>AL</b></span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b>GLOBAL PRATAMA WIJAYA</b></span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top" role="navigation">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <?php
                            $getsession_photo = $this->session->userdata('photo');
                            if (empty($getsession_photo)) {
                                ?>
                                <img src="<?= base_url() ?>assets/admin/img/avatar5.png" width="160" class="img-circle" alt="User Image">
                            <?php } else { ?>
                                <img src="<?= base_url() ?>repository/staff/<?= $getsession_photo ?>" width="160" class="img-circle" alt="User Image">
                            <?php } ?>
                        </div>
                        <div class="pull-left info">
                            <p>
                                <a style="color:#ffffff" href="<?= base_url() ?>backend/view/staff/<?= $this->session->userdata('id_staff') ?>">
                                    <?= $this->session->userdata('staff_fullname') ?>
                                </a>    
                            </p>
                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <ul class="sidebar-menu">
                        <li class="header">MAIN NAVIGATION</li>
                        <li class="active treeview">
                            <a href="<?= base_url() ?>backend/panel">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span> 
                            </a>
                        </li>
                        <li class="treeview <?=($module == 'category_product' 
                                            || $module == 'product'
                                            || $module == 'ongkir'  
                                            || $module == 'kurir'
                                            || $module == 'user'
                                            ? 'active' : '')?>">
                            <a href="javascript:void(0)">
                                <i class="fa fa-dashboard"></i> <span>Master</span><i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li class="<?=($module == 'category_product' ? 'active' : '')?>"><a href="<?= base_url() ?>backend/category_product"><i class="fa fa-circle-o"></i>Data Kategori</a></li>
                                <li class="<?=($module == 'product' ? 'active' : '')?>"><a href="<?= base_url() ?>backend/product"><i class="fa fa-circle-o"></i>Data Produk</a></li>
                                <li class="<?=($module == 'ongkir' ? 'active' : '')?>"><a href="<?= base_url() ?>backend/ongkir"><i class="fa fa-circle-o"></i>Data Ongkir</a></li>
                                <li class="<?=($module == 'kurir' ? 'active' : '')?>"><a href="<?= base_url() ?>backend/kurir"><i class="fa fa-circle-o"></i>Data Kurir</a></li>
                                <li class="<?=($module == 'user' ? 'active' : '')?>"><a href="<?= base_url() ?>backend/user"><i class="fa fa-circle-o"></i>Data Pelanggan</a></li>
                            </ul>
                        </li>
                        <li class="treeview <?=($module == 'user_order' 
                                            || $module == 'user_konfirmasi_pembayaran'
                                            || $module == 'pengiriman'
                                            || $module == 'retur' ? 'active' : '')?>">
                            <a href="javascript:void(0)">
                                <i class="fa fa-dashboard"></i> <span>Transaksi</span><i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li class="<?=($module == 'user_order'  ? 'active' : '')?>"><a href="<?= base_url() ?>backend/user_order"><i class="fa fa-circle-o"></i>Pemesanan</a></li>
                                <li class="<?=($module == 'user_konfirmasi_pembayaran'  ? 'active' : '')?>"><a href="<?= base_url() ?>backend/user_konfirmasi_pembayaran"><i class="fa fa-circle-o"></i>Pembayaran</a></li>
                                <li class="<?=($module == 'pengiriman'  ? 'active' : '')?>"><a href="<?= base_url() ?>backend/pengiriman"><i class="fa fa-circle-o"></i>Pengiriman</a></li>
                                <!-- <li class="<?=($module == 'retur'  ? 'active' : '')?>"><a href="<?= base_url() ?>backend/retur"><i class="fa fa-dashboard"></i> <span>Retur</span> </a></li> -->
                            </ul>
                        </li>
                        
                        
                        <?php
                        $id_privilege = $this->session->userdata('id_privilege');
                        if ($id_privilege == 1) {
                            ?>
                            <li class="treeview">
                                <a href="javascript:void(0)">
                                    <i class="fa fa-dashboard"></i> <span>Management Staff</span> <i class="fa fa-angle-left pull-right"></i>
                                </a>
                                <ul class="treeview-menu">
                                    <li class="active"><a href="<?= base_url() ?>backend/staff"><i class="fa fa-circle-o"></i>Staff</a></li>
                                    <!--<li class="active"><a href="<?= base_url() ?>backend/staff_login"><i class="fa fa-circle-o"></i>Staff Login</a></li>-->
                                    <li class="active"><a href="<?= base_url() ?>backend/hak_akses"><i class="fa fa-circle-o"></i>Hak Akses</a></li>
                                </ul>
                            </li>
                        <?php } ?>
                        <li class="treeview <?=($module == 'laporan_pesanan' 
                                            || $module == 'laporan_pembayaran'
                                            || $module == 'laporan_penjualan'
                                            // || $module == 'laporan_retur'
                                            || $module == 'laporan_produk_terlaris'
                                             ? 'active' : '')?>">
                            <a href="javascript:void(0)">
                                <i class="fa fa-dashboard"></i> <span>Laporan</span> <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li class="<?=($module == 'laporan_pesanan'  ? 'active' : '')?>"><a href="<?= base_url() ?>backend/laporan_pesanan"><i class="fa fa-circle-o"></i>Laporan Pengiriman</a></li>
                                <li class="<?=($module == 'laporan_pembayaran'  ? 'active' : '')?>"><a href="<?= base_url() ?>backend/laporan_pembayaran"><i class="fa fa-circle-o"></i>Laporan Pembayaran</a></li>
                                <li class="<?=($module == 'laporan_penjualan'  ? 'active' : '')?>"><a href="<?= base_url() ?>backend/laporan_penjualan"><i class="fa fa-circle-o"></i>Laporan Pesanan</a></li>
                                <!-- <li class="<?=($module == 'laporan_retur'  ? 'active' : '')?>"><a href="<?= base_url() ?>backend/laporan_retur"><i class="fa fa-circle-o"></i>Laporan Retur</a></li> -->
                                <li class="<?=($module == 'laporan_produk_terlaris'  ? 'active' : '')?>"><a href="<?= base_url() ?>backend/laporan_produk_terlaris"><i class="fa fa-circle-o"></i>Laporan Produk Terlaris</a></li>
                                <!-- <li class="active"><a href="<?= base_url() ?>backend/contact_complain"><i class="fa fa-circle-o"></i>Laporan Rekapitulasi</a></li> -->
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="<?= base_url() ?>login/prosesLogout">
                                <i class="fa fa-dashboard"></i> <span>Logout</span>
                            </a>
                        </li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <?php
                require_once('backend/' . $module . '.php');
                ?>
            </div><!-- /.content-wrapper -->
            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Version</b> 2.3.0
                </div>
                <strong>Copyright &copy; <?=date('Y')?> <a target="_index" href="<?= base_url() ?>">PT. GLOBAL PRATAMA WIJAYA</a>.</strong> All rights reserved.
            </footer>
            <div class="control-sidebar-bg"></div>
        </div><!-- ./wrapper -->

        <script src="<?= base_url() ?>assets/admin/plugins/jQuery/jQuery-2.1.4.min.js"></script>
        <!-- Bootstrap 3.3.5 -->
        <script src="<?= base_url() ?>assets/admin/bootstrap.3.3.5/js/bootstrap.min.js"></script>
        
        <!-- Add fancyBox main JS and CSS files -->
        <script type="text/javascript" src="<?= base_url() ?>assets/bootstrap/bootstrap-calender/js/bootstrap-datepicker.min.js"></script>

        <!-- DataTables -->
        <script src="<?= base_url() ?>assets/admin/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?= base_url() ?>assets/admin/plugins/datatables/dataTables.bootstrap.min.js"></script>

        <script type="text/javascript" src="<?= base_url() ?>assets/admin/plugins/fancybox/jquery.mousewheel-3.0.6.pack.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/admin/plugins/fancybox/jquery.fancybox.js?v=2.1.5"></script>

        <!-- daterangepicker -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
        <script src="<?= base_url() ?>assets/admin/plugins/daterangepicker/daterangepicker.js"></script>
        <script src="<?=base_url()?>assets/js/chart.min.js"></script>
        <!-- datepicker -->
        <script src="<?= base_url() ?>assets/admin/plugins/datepicker/bootstrap-datepicker.js"></script>
        <script src="<?=base_url()?>assets/js/ckeditor/ckeditor.js"></script>

        <!-- FastClick -->
        <script src="<?= base_url() ?>assets/admin/plugins/fastclick/fastclick.min.js"></script>
        <!-- Select2 -->
        <script src="<?= base_url() ?>assets/admin/plugins/select2/select2.full.min.js"></script>
        <!-- AdminLTE App -->
        <script src="<?= base_url() ?>assets/admin/js/app.min.js"></script>
        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <!--<script src="<?= base_url() ?>assets/admin/js/pages/dashboard.js"></script>-->
        <!-- AdminLTE for demo purposes -->
        <script src="<?= base_url() ?>assets/admin/js/demo.js"></script>
        <script src="<?= base_url() ?>assets/js/admin.js"></script>
    </body>
</html>
<script type="text/javascript">
$(document).ready(function () {
   
    
    $(".fancybox").fancybox();
    $("#example1").DataTable();
    $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false
    });
    CKEDITOR.replace('editor1');
    CKEDITOR.replace('editor2');
    $(".select2").select2();

    

});

$(function () {

});
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
</script>

