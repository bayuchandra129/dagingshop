<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Data About Us
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?=base_url()?>backend"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li class="active">Data About Us</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">

            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Data Master About Us</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                     <table id="example1" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th class="text-center">Opsi</th>
                                <th class="text-center">About Name</th>
                                <th class="text-center">Date Add</th>
                                <th class="text-center">Date Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            $no = 1;
                                foreach($show_about as $val) {
                            ?>
                            <tr>
                                <td>
                                    <?=$no;?>
                                </td>
                                <td class="text-center">
                                    <a href="<?=base_url()?>backend/view/aboutus/<?=$val['id']?>">
                                        <span title="View" aria-hidden="true" class="glyphicon glyphicon-eye-open"></span> &nbsp;&nbsp;&nbsp;
                                    </a>
                                    <a href="<?=base_url()?>backend/edit/aboutus/<?=$val['id']?>">
                                        <span title="Edit" aria-hidden="true" class="glyphicon glyphicon-edit"></span> 
                                    </a>
                                </td>
                                <td class="text-center">
                                    <?=$val['name']?>
                                </td>
                                <td class="text-center">
                                    <?=$val['date_add']?>
                                </td>
                                <td class="text-center">
                                    <?=$val['date_edit']?>
                                </td>
                            </tr>
                            <?php $no++; } ?>
                        </tbody>
<!--                        <tfoot>
                            <tr>
                                <th>No</th>
                                <th class="text-center">Opsi</th>
                                <th class="text-center">About Name</th>
                                <th class="text-center">Date Add</th>
                                <th class="text-center">Date Edit</th>
                            </tr>
                        </tfoot>-->
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
