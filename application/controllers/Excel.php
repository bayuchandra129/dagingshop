<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Excel extends ci_controller {

    public function __construct() {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
        $this->load->library('PHPExcel');
    }

    public function getexcel($params = null) {
        $date_form = $this->input->post('from_date');
        $date_to = $this->input->post('to_date');

        if($params == 'laporan_pesanan') {
            $select = $this->core_admin->getrelasi('relasi_pengiriman_detail', null, $date_form, $date_to);
          
     
        } else if ($params == 'laporan_pembayaran') {
            $select = $this->core_admin->getrelasi('relasi_pembayaran', null, $date_form, $date_to);
        } else if($params == 'laporan_penjualan') {
            $select = $this->core_admin->getrelasi('relasi_cetak_laporan_penjualan', null, $date_form, $date_to);
           
        } else if($params == 'laporan_retur') {
            $select = $this->core_admin->getrelasi('relasi_retur', null, $date_form, $date_to);
          
        } else if($params == 'laporan_produk_terlaris') {
            $select = $this->core_admin->getrelasi('relasi_laporan_produk_terlaris', null, $date_form, $date_to);
            
            
        }


        $objPHPExcel    = new PHPExcel();

        if($params == 'laporan_pesanan') {
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
            
            $objPHPExcel->getActiveSheet()->getStyle(1)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle(2)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle(3)->getFont()->setBold(false);
            $objPHPExcel->getActiveSheet()->getStyle(4)->getFont()->setBold(false);
            $objPHPExcel->getActiveSheet()->getStyle(5)->getFont()->setBold(false);
            $objPHPExcel->getActiveSheet()->getStyle(6)->getFont()->setBold(false);
            $objPHPExcel->getActiveSheet()->getStyle(7)->getFont()->setBold(false);
            $objPHPExcel->getActiveSheet()->getStyle(8)->getFont()->setBold(false);
            $objPHPExcel->getActiveSheet()->getStyle(8)->getFont()->setBold(false);
            $objPHPExcel->getActiveSheet()->getStyle(8)->getFont()->setBold(false);
            $objPHPExcel->getActiveSheet()->getStyle(8)->getFont()->setBold(false);
            $objPHPExcel->getActiveSheet()->getStyle(8)->getFont()->setBold(false);
        } else if($params == 'laporan_pembayaran') { 
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(40);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(40);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
            
            $objPHPExcel->getActiveSheet()->getStyle(1)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle(2)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle(3)->getFont()->setBold(false);
            $objPHPExcel->getActiveSheet()->getStyle(4)->getFont()->setBold(false);
            $objPHPExcel->getActiveSheet()->getStyle(5)->getFont()->setBold(false);
            $objPHPExcel->getActiveSheet()->getStyle(6)->getFont()->setBold(false);
            $objPHPExcel->getActiveSheet()->getStyle(7)->getFont()->setBold(false);
        } else if($params == 'laporan_penjualan') { 
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(40);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
            
            $objPHPExcel->getActiveSheet()->getStyle(1)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle(2)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle(3)->getFont()->setBold(false);
            $objPHPExcel->getActiveSheet()->getStyle(4)->getFont()->setBold(false);
            $objPHPExcel->getActiveSheet()->getStyle(5)->getFont()->setBold(false);
            $objPHPExcel->getActiveSheet()->getStyle(6)->getFont()->setBold(false);
            $objPHPExcel->getActiveSheet()->getStyle(7)->getFont()->setBold(false);
            $objPHPExcel->getActiveSheet()->getStyle(8)->getFont()->setBold(false);
        }  else if($params == 'laporan_retur') { 
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
            
            $objPHPExcel->getActiveSheet()->getStyle(1)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle(2)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle(3)->getFont()->setBold(false);
            $objPHPExcel->getActiveSheet()->getStyle(4)->getFont()->setBold(false);
            $objPHPExcel->getActiveSheet()->getStyle(5)->getFont()->setBold(false);
        } else if($params == 'laporan_produk_terlaris') { 
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(55);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
            
            $objPHPExcel->getActiveSheet()->getStyle(1)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle(2)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle(3)->getFont()->setBold(false);
            $objPHPExcel->getActiveSheet()->getStyle(4)->getFont()->setBold(false);
        }
        
        $header = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ),
            'font' => array(
                'bold' => true,
                'color' => array('rgb' => 'FF0000'),
                'name' => 'Verdana'
            )
        );
        $objPHPExcel->getActiveSheet()->getStyle("A1:D2")
                ->applyFromArray($header)
                ->getFont()->setSize(16);
        $objPHPExcel->getActiveSheet()->mergeCells('A1:D2');
        
        if($params == 'laporan_pesanan') {
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Laporan Data Pengiriman')
                ->setCellValue('A3', 'No.')
                ->setCellValue('B3', 'Tanggal SJ')
                ->setCellValue('C3', 'No. Pesanan')
                ->setCellValue('D3', 'Nama Pelanggan')
                ->setCellValue('E3', 'Alamat Kirim')
                ->setCellValue('F3', 'Kode Surat Jalan')
                ->setCellValue('G3', 'Nama Kurir')
                ->setCellValue('H3', 'Nama Produk')
                ->setCellValue('I3', 'Jumlah')
                ->setCellValue('J3', 'Harga Satuan')
                ->setCellValue('K3', 'Total Harga');
        } else if($params == 'laporan_pembayaran') { 
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Laporan Data Pembayaran')
                ->setCellValue('A3', 'No.')
                ->setCellValue('B3', 'No. Pesanan')
                ->setCellValue('C3', 'Nama Pelanggan')
                ->setCellValue('D3', 'Tanggal Pembayaran')
                ->setCellValue('E3', 'Nama Pemilik Bank')
                ->setCellValue('F3', 'No. Rekening')
                ->setCellValue('G3', 'Nama Bank')
                ->setCellValue('H3', 'Total Bayar');
        }  else if($params == 'laporan_penjualan') { 
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Laporan Data Pesanan')
                ->setCellValue('A3', 'No.')
                ->setCellValue('B3', 'No. Pesanan')
                ->setCellValue('C3', 'Nama Pelanggan')
                ->setCellValue('D3', 'Tanggal Pesan')
                ->setCellValue('E3', 'Nama Barang')
                ->setCellValue('F3', 'Jumlah Beli')
                ->setCellValue('G3', 'Harga Satuan')
                ->setCellValue('H3', 'Total')
                ->setCellValue('I3', 'Status Pesan');
        } else if($params == 'laporan_retur') { 
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Laporan Data Retur')
                ->setCellValue('A3', 'No.')
                ->setCellValue('B3', 'No. Pesanan')
                ->setCellValue('C3', 'No. Pelanggan')
                ->setCellValue('D3', 'Nama Produk')
                ->setCellValue('E3', 'Qty')
                ->setCellValue('F3', 'Keterangan')
                ->setCellValue('G3', 'Tanggal Retur');
        } else if($params == 'laporan_produk_terlaris') { 
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Laporan Data Produk Terlaris')
                ->setCellValue('A3', 'No.')
                ->setCellValue('B3', 'Kode Produk')
                ->setCellValue('C3', 'Nama Produk')
                ->setCellValue('D3', 'Total Ter-Jual');
        }

        $ex = $objPHPExcel->setActiveSheetIndex(0);
        $no = 1;
        $counter = 4;
        $grand_total = 0;
        foreach ($select as $row):
            if($params == 'laporan_pesanan') {
                $grand_total += $row['qty'] * $row['harga_jual'];
                $ex->setCellValue('A'.$counter, $no++);
                $ex->setCellValue('B'.$counter, formatDateNumerik($row['tgl_sj']));
                $ex->setCellValue('C'.$counter, $row['kd_pesanan']);
                $ex->setCellValue('D'.$counter, $row['nm_plg']);
                $ex->setCellValue('E'.$counter, $row['alamat_kirim']);
                $ex->setCellValue('F'.$counter, $row['kd_sj']);
                $ex->setCellValue('G'.$counter, $row['nama_kurir']);
                $ex->setCellValue('H'.$counter, $row['nm_produk']);
                $ex->setCellValue('I'.$counter, $row['qty']);
                $ex->setCellValue('J'.$counter, "Rp. ".rupiah($row['harga_jual']));
                $ex->setCellValue('K'.$counter, "Rp. ".rupiah($row['qty'] * $row['harga_jual']));
            } else if($params == 'laporan_pembayaran') {
                $grand_total += $row['total_bayar'];
                $ex->setCellValue('A'.$counter, $no++);
                $ex->setCellValue('B'.$counter, $row['kd_pesanan']);
                $ex->setCellValue('C'.$counter, $row['nm_plg']);
                $ex->setCellValue('D'.$counter, formatDateMonthNumerik($row['tgl_pembayaran']));
                $ex->setCellValue('E'.$counter, $row['nm_pembank']);
                $ex->setCellValue('F'.$counter, $row['no_rek']);
                $ex->setCellValue('G'.$counter, $row['nm_bank']);
                $ex->setCellValue('H'.$counter, "Rp ".rupiah($row['total_bayar']));
            } else if($params == 'laporan_penjualan') {
                $grand_total += $row['qty'] * $row['harga_jual'];
                $ex->setCellValue('A'.$counter, $no++);
                $ex->setCellValue('B'.$counter, $row['kd_pesanan']);
                $ex->setCellValue('C'.$counter, $row['nm_plg']);
                $ex->setCellValue('D'.$counter, formatDateMonthNumerik($row['tgl_pesanan']));
                $ex->setCellValue('E'.$counter, $row['nm_produk']);
                $ex->setCellValue('F'.$counter, $row['qty']);
                $ex->setCellValue('G'.$counter, "Rp ".rupiah($row['harga_jual']));
                $ex->setCellValue('H'.$counter, "Rp ".rupiah($row['qty'] * $row['harga_jual']));
                $ex->setCellValue('I'.$counter, status_pesanan($row['status_pesanan']));
            } else if($params == 'laporan_retur') {
                $ex->setCellValue('A'.$counter, $no++);
                $ex->setCellValue('B'.$counter, $row['kd_pesanan']);
                $ex->setCellValue('C'.$counter, $row['nm_plg']);
                $ex->setCellValue('D'.$counter, $row['nama_produk']);
                $ex->setCellValue('E'.$counter, $row['quantity']);
                $ex->setCellValue('F'.$counter, $row['keterangan']);
                $ex->setCellValue('G'.$counter, formatDateMonthNumerik($row['tgl_retur']));
            } else if($params == 'laporan_produk_terlaris') {
                $ex->setCellValue('A'.$counter, $no++);
                $ex->setCellValue('B'.$counter, $row['kd_produk']);
                $ex->setCellValue('C'.$counter, $row['nm_produk']);
                $ex->setCellValue('D'.$counter, $row['total_jual']);
            }
            
            $counter = $counter+1;
        endforeach;

        if($params == 'laporan_pesanan') {
            $ex->setCellValue("H$counter", "Grand Total");
            $ex->setCellValue("I$counter", "Rp ".rupiah($grand_total));
        } else if($params == 'laporan_pembayaran') {
            $ex->setCellValue("F$counter", "Grand Total");
            $ex->setCellValue("G$counter", "Rp ".rupiah($grand_total));
        } else if($params == 'laporan_penjualan') {
            $ex->setCellValue("F$counter", "Grand Total");
            $ex->setCellValue("G$counter", "Rp ".rupiah($grand_total));
        }
        
        
        $objPHPExcel->getProperties()->setCreator("Ismo Broto")
            ->setLastModifiedBy("Ismo Broto")
            ->setTitle("Export PHPExcel Test Document")
            ->setSubject("Export PHPExcel Test Document")
            ->setDescription("Test doc for Office 2007 XLSX, generated by PHPExcel.")
            ->setKeywords("office 2007 openxml php")
            ->setCategory("PHPExcel");
        $objPHPExcel->getActiveSheet()->setTitle('Data Laporan');
        
        $objWriter  = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        header('Last-Modified:'. gmdate("D, d M Y H:i:s").'GMT');
        header('Chace-Control: no-store, no-cache, must-revalation');
        header('Chace-Control: post-check=0, pre-check=0', FALSE);
        header('Pragma: no-cache');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Export_Data'.$params.'_'. date('Ymd') .'.xlsx"');
        
        $objWriter->save('php://output');
    }

}
?>