<br/><br/>
<div id="content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="big-title text-center">
                    <h1>FAQ</h1>
                </div>
                <div style="font-size:12px;">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        Bagaimana cara melakukan order di toko Online ini?
                        </a>
                    </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">
                       Untuk order Anda dapat melakukan dari halaman website, dengan memilih produk terlebih dahulu. Selanjutnya adalah proses ordering dan diakhiri dengan Proses Checkout.
                       </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwo">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        Bagaimana metode pembayaran di toko online ini?
                        </a>
                    </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                    <div class="panel-body">
                    Pembayaran dapat anda lakukan dengan melakukan transfer ke nomor rekening yang kami tunjuk, selanjutnya konfirmasikan pembayaran anda kepada kami untuk kami validasi
                    </div>
                    </div>
                </div>
                </div>
                </div>
                
            </div>
        </div>
    </div>         
</div>
