<br/><br/>
<section id="register">
    <div id="content">
        <div class="container">
            <div class="page-content">
                <div class="row">
                    <!-- left form -->
                    <div class="col-sm-8">
                        <form enctype="multipart/form-data" id="form-registerid" method="POST" class="p-form" autocomplete="off">
                            
                            <h3>REGISTER</h3>
                            <p>Silahkan mengisi form berikut untuk mendaftar sebagai member</p>
                            <hr>
                            <?php
                            if (!empty($this->cart->contents())) {
                                ?>
                            <input type="hidden" id="ongkir_val" value="<?= $this->cart->total() ?>"/>
                            <h4>Total Pembayaran</h4>
                            <hr>
                            <label for="nama">Ongkos Kirim</label>
                            <label id="txt_ongkos_kirim">
                                <b> Rp. 0</b>
                            </label>
                            <label for="nama">Total Order</label>
                            <label>
                                <b> <a href="<?= base_url() ?>product/cart">Rp. <?= $this->cart->format_number($this->cart->total()) ?> </a></b>
                            </label>
                            <label for="nama">Grand Total</label>
                            <label id="txt_grand_total">
                                <b>Rp. <?= $this->cart->format_number($this->cart->total()) ?></b>
                            </label>
                            <div style="clear:both"></div><br/>
                            <h4>Tujuan Pengiriman</h4>
                            <hr>
                            <label for="nama">Kota</label>
                            <select class="select-form" id="ongkos_kirim" required="" name="ongkos_kirim">
                            <option value="">--Pilih Kota --</option>
                            <?php 
                                foreach($ongkir as $val_ongkir) {
                            ?>
                            <option value="<?=$val_ongkir['id_ongkir']?>"><?=$val_ongkir['nama_kota']?></option>
                            <?php } ?>
                            </select>
                           <label for="nama">Alamat Lengkap Pengiriman</label>
                            <input type="text" value="" required="" id="alamat_pengiriman" placeholder="Masukkan Alamat Lengkap Pengiriman" name="alamat_pengiriman" autocomplete="off">
                            <label for="nama">Catatan</label>
                            <input type="text" value="" required="" id="catatan" placeholder="Masukkan Catatan" name="catatan" autocomplete="off"> <br/>
                            <div style="clear:both"></div><br/>
                            <?php } ?>
                           <h4>Data Diri</h4>
                            <hr>
                            <label for="nama">Nama Lengkap</label>
                            <input type="text" value="" required="" id="nama" placeholder="Masukkan Nama Lengkap Anda" name="nama" autocomplete="off">
                            <label for="alamat-email">Alamat Email</label>
                            <input type="email" required="required" value="" id="alamat_email" name="alamat_email" placeholder="Masukkan Alamat Email Lengkap Anda">
                            <label for="password">Password</label>
                            <input type="password" required="required" id="password" name="password" autocomplete="off" placeholder="Masukkan Password">
                            <label for="nomor-hp">Nomor Handphone</label>
                            <input type="text" onkeypress="return isNumber(event);" required="" value="" id="nomor_hp" name="nomor_hp" placeholder="Masukkan Nomor Handphone Anda" autocomplete="off">
                            <label for="alamat-domisili">Alamat Sekarang</label>
                            <input type="text" required="required" value="" id="alamat_domisili" name="alamat_domisili" autocomplete="off" placeholder="Masukkan Alamat Lengkap Anda">
                            <label for="kota">Provinsi</label>                              
                            <select type="text" required="required" class="select-form"  name="kota" autocomplete="off">
                            <option value="">-- Pilih Provinsi</option>
                                <option value="Bali">Bali</option>
                                <option value="Bangka Belitung">Bangka Belitung</option>
                                <option value="Banten">Banten</option>
                                <option value="Bengkulu">Bengkulu</option>
                                <option value="DI Yogyakarta">DI Yogyakarta</option>
                                <option value="DKI Jakarta">DKI Jakarta</option>
                                <option value="Gorontalo">Gorontalo</option>
                                <option value="Jambi">Jambi</option>
                                <option value="Jawa Barat">Jawa Barat</option>
                                <option value="Jawa Tengah">Jawa Tengah</option>
                                <option value="Jawa Timur">Jawa Timur</option>
                                <option value="Kalimantan Barat">Kalimantan Barat</option>
                                <option value="Kalimantan Selatan">Kalimantan Selatan</option>
                                <option value="Kalimantan Tengah">Kalimantan Tengah</option>
                                <option value="Kalimantan Timur">Kalimantan Timur</option>
                                <option value="Kalimantan Utara">Kalimantan Utara</option>
                                <option value="Kepulauan Riau">Kepulauan Riau</option>
                                <option value="Lampung">Lampung</option>
                                <option value="Maluku">Maluku</option>
                                <option value="Maluku Utara">Maluku Utara</option>
                                <option value="Nanggroe Aceh Darussalam">Nanggroe Aceh Darussalam (NAD)</option>
                                <option value="Nusa Tenggara Barat">Nusa Tenggara Barat (NTB)</option>
                                <option value="Nusa Tenggara Timur">Nusa Tenggara Timur (NTT)</option>
                                <option value="Papua">Papua</option>
                                <option value="Papua Barat">Papua Barat</option>
                                <option value="Riau">Riau</option>
                                <option value="Sulawesi Barat">Sulawesi Barat</option>
                                <option value="Sulawesi Selatan">Sulawesi Selatan</option>
                                <option value="Sulawesi Tengah">Sulawesi Tengah</option>
                                <option value="Sulawesi Tenggara">Sulawesi Tenggara</option>
                                <option value="Sulawesi Utara">Sulawesi Utara</option>
                                <option value="Sumatera Barat">Sumatera Barat</option>
                                <option value="Sumatera Selatan">Sumatera Selatan</option>
                                <option value="Sumatera Utara">Sumatera Utara</option>
                            </select>
                            <label for="kodepos">Kode Pos</label>                              
                            <input type="text" required="required"  placeholder="Masukkan Kode Pos"  id="kodepos" name="kodepos" autocomplete="off">
                            
                            <input type="submit" id="SubmitRegisterId" class="btn-submit daftar" value="DAFTAR">
                        </form>
                    </div>
                    <!-- rigth form -->
                    <div class="col-sm-4">
                        <div class="tabs-section tabs-section-mobile">

                            <!-- Nav Tabs -->
                            <ul class="nav nav-tabs" data-tabs="tabs">
                                <li class="active"><a href="#tab-1" data-toggle="tab"><i class="fa fa-desktop"></i>Login</a></li>
                                <li><a href="#tab-2" data-toggle="tab"><i class="fa fa-leaf"></i>Lupa Password</a></li>
                            </ul>

                            <!-- Tab panels -->
                            <div class="tab-content tab-content-height">
                                <div class="tab-pane fade in active" id="tab-1">
                                <form id="form-loginid" method="POST" class="p-form form-register" autocomplete="off">
                                    <h3>SIGN IN</h3>
                                    <p>Masuk jika anda sudah menjadi member</p>
                                    <hr>
                                    <input type="email" required="" value="" placeholder="Email" id="user_email" name="user_email">
                                    <input type="password" required="" placeholder="Kata Sandi" id="user_password" name="user_password" autocomplete="off">
                                    <input type="submit" id="SubmitLoginId" class="btn-submit" value="MASUK">
                                </form>
                                </div>
                                <div class="tab-pane fade in" id="tab-2">
                                    <form id="form-forgotid" method="POST" class="p-form form-register" autocomplete="off">
                                        <h3>Forgot Password</h3>
                                        <p>Masukan email anda jika lupa password</p>
                                        <hr>
                                        <input type="email" required="" value="" placeholder="Email" id="user_email" name="user_email">
                                        <input type="password" required="" placeholder="Kata Sandi Baru" id="user_password" name="password_new" autocomplete="off">
                                        <input type="submit" id="SubmitForgotId" class="btn-submit" value="MASUK">
                                    </form>
                                </div>
                            </div>
                        </div> 
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>