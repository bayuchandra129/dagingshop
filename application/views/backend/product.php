<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Data Produk
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?=base_url()?>backend"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li class="active">Data Produk</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Data Master Produk</h3><br/><br/>
                    <div class="col-md-2">
                        <a href="<?=base_url()?>backend/add/product" class="btn btn-block btn-primary">Tambah Produk</a>
                    </div>
                    <div class="pull-right">
                        <!-- <form action="<?= base_url() ?>excel/getexcel/product" id="formReport_<?=$module?>" method="post">
                            <span>Sort By : </span>
                            FROM &nbsp;&nbsp; <input type="text" required="required" name="tanggal1" class="datepicker form-sortby" placeholder="From Tanggal" value="" id="tanggal"  autocomplete="off"> 
                            &nbsp;TO &nbsp;&nbsp; <input type="text" required="required" name="tanggal2" class="datepicker form-sortby" placeholder="To Tanggal" value="" id="tanggal" autocomplete="off">
                            <input type="submit" class="btn btn-primary" value="Export Excel"/>
                        </form> -->
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                     <table id="example1" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th class="text-center">Opsi</th>
                                <th class="text-center">Kode Produk</th>
                                <th class="text-center">Kategori</th>
                                <th class="text-center">Nama Produk</th>
                                <th class="text-center">Stok</th>
                                <th class="text-center">Diskon (%)</th>
                                <th class="text-center">Harga</th>
                                <th class="text-center">Picture</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            $no = 1;
                                foreach($show_data as $val) {
                            ?>
                            <tr>
                                <td>
                                    <?=$no;?>
                                </td>
                                <td class="text-center">
                                    <a href="<?=base_url()?>backend/view/product/<?=$val['kd_produk']?>">
                                        <span title="View" aria-hidden="true" class="glyphicon glyphicon-eye-open"></span> &nbsp;&nbsp;&nbsp;
                                    </a>
                                    <a href="<?=base_url()?>backend/edit/product/<?=$val['kd_produk']?>">
                                        <span title="Edit" aria-hidden="true" class="glyphicon glyphicon-edit"></span>  &nbsp;&nbsp;&nbsp;
                                    </a>
                                    <a onclick="return confirm('Apakah anda yakin ingin menghapus data ini ? ')" href="<?=base_url()?>proccess_backend/processremove/product/<?=$val['kd_produk']?>">
                                        <span title="Remove" aria-hidden="true" class="fa fa-trash"></span> 
                                    </a>
                                </td>
                                <td class="text-center">
                                    <?=$val['kd_produk']?>
                                </td>
                               <td class="text-center">
                                    <?=$val['nama_kategori']?> 
                                </td>
                                <td class="text-center">
                                    <?=$val['nm_produk']?>
                                </td>
                                <td class="text-center">
                                    <?=$val['stok']?>
                                </td>
                                <td class="text-center">
                                    <?=$val['diskon']?>
                                </td>
                                <td class="text-center">
                                    <?=$val['harga']?>
                                </td>
                                <td class="text-center">
                                    <?php 
                                        if($val['gambar_produk'] == "") {
                                    ?>
                                    <a class="fancybox" data-fancybox-group="gallery" href="<?= base_url() ?>assets/admin/img/not_available.jpg" >
                                    <img width="100" style="border:2px solid #000" src="<?=base_url()?>assets/admin/img/not_available.jpg"/>
                                    </a>
                                    <?php } else { ?>
                                    <a class="fancybox" data-fancybox-group="gallery"  title="<?=$val['nm_produk']?>" href="<?=base_url()?>repository/product/<?=$val['gambar_produk']?>" >
                                    <img width="100" style="border:2px solid #000" src="<?=base_url()?>repository/product/<?=$val['gambar_produk']?>"/>
                                    </a>
                                    <?php } ?>
                                </td>
                            </tr>
                            <?php $no++; } ?>
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
