<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');

Class Ajax extends CI_Controller {

    public function __construct() {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
    }

    public function ongkos_kirim() {
        $id_ongkir = $this->input->post('id_ongkir');

        $data['ongkir'] = $this->core_base->getdata('ongkir', Array('id_ongkir' => $id_ongkir));

        if($data['ongkir']['harga_ongkir'] != 0) {
            $ongkos_kirim = $data['ongkir']['harga_ongkir'];
        } else {
            $ongkos_kirim = 0;
        }

        echo  json_encode(Array('ongkos_kirim'=> $ongkos_kirim));
    }
}
