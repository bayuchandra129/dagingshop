<br/><br/>
<!-- Start Content -->
<div id="content">
    <div class="container">

        <div class="row">

            <div class="col-md-8">
                <!-- Classic Heading -->
                <h4 class="classic-title"><span>Konfirmasi Pembayaran</span></h4>


                    <form enctype="multipart/form-data" role="form" class="contact-form form-confirmid" id="contact-form" method="post" autocomplete="off">
                        <div class="form-group">
                            <div class="controls">
                                <input type="text" placeholder="Kode Pesanan" id="kode_pesanan" class="kode_pesanan" name="kode_pesanan">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="controls">
                                <input type="text" class="email tgl_bayar datepicker" id="tgl_bayar"  placeholder="Tanggal Bayar" name="tgl_bayar">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="controls">
                                <input type="text" class="requiredField nama_pembank" id="nama_pembank" placeholder="Nama Pemilik Bank" name="nama_pembank">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="controls">
                                <input type="text" class="requiredField no_rekening" id="no_rekening" onKeypress="return isNumber(event)" placeholder="No. Rekening" name="no_rek">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="controls">
                                <input type="text" class="requiredField nm_bank" id="nm_bank" placeholder="Nama Bank" name="nm_bank">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="controls">
                                <input type="text" class="requiredField total_bayar" id="total_bayar" onKeypress="return isNumber(event)" placeholder="Total Bayar" name="total_bayar">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="controls">
                            <label><b>Foto Bukti (Only Image)</b></label><br/>
                                <input type="file" class="requiredField foto_bukti" id="foto_bukti" name="foto_bukti">
                            </div>
                        </div>

                        <div class="form-group">
                            <center>
                                <div class="opsi-info">
                                </div>
                            </center>
                        </div>
                        <button type="submit" style="float: right" id="form-confirmid" class="btn-system btn-large pull-right">SEND</button>
                    </form>
               
                <!-- End Contact Form Help -->

            </div>
            <div class="m-contact-both" ></div>
            <div class="col-md-4">

                <!-- Classic Heading -->
                <h4 class="classic-title"><span>Information</span></h4>

                <!-- Some Info -->
                <p>PT. GLOBAL PRATAMA WIJAYA</p>

                <!-- Divider -->
                <div class="hr1" style="margin-bottom:10px;"></div>

                <!-- Info - Icons List -->
                <ul class="icons-list">
                    <li><i class="fa fa-globe">  </i> <strong>Address:</strong> Jl. Agung Barat 30, Blok B-36A No. 54A, RT.2/RW.10, Sunter Agung, Tj. Priok, Kota Jkt Utara, Daerah Khusus Ibukota Jakarta 14350</li>
                    <li><i class="fa fa-envelope-o"></i> <strong>Email:</strong> info@globalpratamawijaya.com</li>
                    <li><i class="fa fa-mobile"></i> <strong>Phone:</strong> +62 21 6409169</li>
                </ul>

                <!-- Divider -->
                <div class="hr1" style="margin-bottom:15px;"></div>

            </div>

        </div>

    </div>
</div><br/>
<!-- End content -->