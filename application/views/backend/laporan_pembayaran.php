<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Data Laporan Pembayaran
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= base_url() ?>backend"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li class="active">Data Laporan Pembayaran</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Data Laporan Pembayaran</h3><br/><br/>
                    <div class="pull-right">
                    <form class="form-inline" method="post" action="<?= base_url() ?>excel/getexcel/laporan_pembayaran" autocomplete="off">
                        <div class="form-group">
                            <label for="exampleInputName2">Dari Tanggal</label>
                            <input type="text" name="from_date" require class="form-control datepicker"  placeholder="Dari Tanggal">
                        </div> &nbsp;&nbsp;&nbsp;
                        <div class="form-group">
                            <label for="exampleInputEmail2">Sampai Tanggal</label>
                            <input type="text" name="to_date" require class="form-control datepicker"  placeholder="Sampai Tanggal">
                        </div> &nbsp;&nbsp;&nbsp;
                    <button type="submit" class="btn btn-success pull-right">EXPORT LAPORAN</button> 
                    </form>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-header">

                </div><!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th class="text-center">No Pesanan</th>
                                <th class="text-center">Nama Pelanggan</th>
                                <th class="text-center">Tanggal Pembayaran</th>
                                <th class="text-center">Nama Pemilik Bank</th>
                                <th class="text-center">No. Rekening</th>
                                <th class="text-center">Nama Bank</th>
                                <th class="text-center">Bukti Transfer</th>
                                <th class="text-center">Total Bayar</th>
                                <!-- <th class="text-center">Status Pembayaran</th> -->
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            $grand_total = 0;
                            foreach ($show_data as $val) {
                                
                                $grand_total += $val['total_bayar'];
                            ?>
                                <tr>
                                    <td>
                                        <?= $no ?>
                                    </td>
                                    <td>
                                        <a href="<?= base_url() ?>backend/edit/user_order/<?= $val['kd_pesanan'] ?>"><?= $val['kd_pesanan'] ?></a>
                                    </td>
                                    <td>
                                        <?= $val['nm_plg'] ?>
                                    </td>
                                    <td>
                                        <?= formatDateMonthNumerik($val['tgl_pembayaran']) ?>
                                    </td>
                                    <td>
                                        <?= $val['nm_pembank'] ?>
                                    </td>
                                    <td>
                                        <?= $val['no_rek'] ?>
                                    </td>
                                    <td>
                                        <?= $val['nm_bank'] ?>
                                    </td>
                                    <td>
                                        <?php
                                        if ($val['foto_bukti'] == "") {
                                            ?>
                                        <a class="fancybox" data-fancybox-group="gallery" href="<?= base_url() ?>assets/admin/img/not_available.jpg" >
                                            <img width="100" style="border:2px solid #000" src="<?= base_url() ?>assets/admin/img/not_available.jpg"/>
                                            </a>
                                        <?php } else { ?>
                                        <a class="fancybox" data-fancybox-group="gallery"  title="Bukti Transfer No. Pesanan : <?= $val['kd_pesanan'] ?>" href="<?= base_url() ?>repository/konfirmasi_pembayaran/<?= $val['foto_bukti'] ?>" >
                                            <img width="100" style="border:2px solid #000" src="<?= base_url() ?>repository/konfirmasi_pembayaran/<?= $val['foto_bukti'] ?>"/>
                                            </a>
                                        <?php } ?>
                                    </td>
                                    <td>
                                        Rp. <?= (empty($val['total_bayar']) ? 0 : rupiah($val['total_bayar'])) ?>
                                    </td>
                                   
                                    <!-- <td>
                                             <?= status_payment($val['status_pembayaran']) ?>
                                    </td> -->
                                </tr>
                                <?php $no++;
                            } ?>
                            
                        </tbody>
                        <tfoot>
                        <tr>
                                <td colspan="8">
                                    <b>Grand Total</b>
                                </td>
                                <td>
                                Rp. 
                                <?=rupiah($grand_total);?>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div><!-- /.box-body -->

            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->

