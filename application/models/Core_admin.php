<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
Class Core_admin extends CI_Model {
    public function __construct() {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
    }
    
    function ceklogin($email = null, $password = null) { 
        $sql = $this->db->query("SELECT A.*
                                 FROM admin A
                                 WHERE A.email_admin='".$email."' && A.pass_admin='".$password."'
                                ");
        
        $query = $sql->result_array();
        return $query;
    }

    function get_data_projek_month(){
        $query = $this->db->query("SELECT MONTH(tgl_pesanan) AS bulan, COUNT(*) AS jumlah_bulanan
                                FROM pesanan
                                GROUP BY MONTH(tgl_pesanan)
                                ORDER BY MONTH(tgl_pesanan)");
          
        if($query->num_rows() > 0){
            foreach($query->result() as $data){
                $hasil[] = $data;
            }
            return $hasil;
        }
    }
    
    function getrelasi($params, $where = null, $between1 = null, $between2 = null) {
       if ($params == 'relasi_purchase') {
           if(!empty($where)) {
               $where2 = " WHERE A.kd_pesanan='".$where."'";
           }
           $query = "SELECT * FROM pesanan A 
                   LEFT JOIN detil_pesanan B 
                   ON A.kd_pesanan = B.kd_pesanan 
                   LEFT JOIN product C
                   ON B.id_product = C.id_product
                   ";
           $query .= $where2;
       } else if($params == 'user_order') {
           if(!empty($where)) {
               $where = " WHERE B.kd_pesanan='".$where."' ";
           }
           $query = "SELECT B.kd_pesanan as pesanan_code, A.*, B.*, C.*, D.*, E.* FROM pelanggan A
                    LEFT JOIN pesanan B
                    ON A.id_plg = B.kd_plg
                    LEFT JOIN detil_pesanan C
                    ON B.kd_pesanan = C.kd_pesanan
                    LEFT JOIN ongkir D
                    ON B.ongkir_id = D.id_ongkir
                    LEFT JOIN surat_jalan E
                    ON B.kd_pesanan = E.kd_pesanan
                    ";
            $query .= $where;
           $query .= " GROUP BY B.kd_pesanan ORDER BY B.tgl_pesanan DESC "; 
       } else if($params == 'user_order_admin') {
        if(!empty($where)) {
            $where = " WHERE A.kd_pesanan='".$where."' ";
        }
        $query = "SELECT A.kd_pesanan as pesanan_code, A.*, B.*, C.* FROM pesanan A
                        LEFT JOIN pelanggan B
                        ON A.kd_plg = B.id_plg
                        LEFT JOIN detil_pesanan C
                        ON A.kd_pesanan = C.kd_pesanan
                 ";
         $query .= $where;
        $query .= " GROUP BY A.kd_pesanan ORDER BY A.tgl_pesanan DESC "; 
    }  else if($params == 'relasi_staff') {
           if(!empty($where)) {
               $where = " WHERE A.id_staff='".$where."'";
           }
           $query = "SELECT A.*, B.* FROM staff A 
                     LEFT JOIN privilege B 
                     ON A.id_privilege = B.id_privilege ";
            $query .= $where;
           $query .= " ORDER BY A.id_privilege ASC";
       } else if ($params == 'relasi_produk') {
           if(!empty($where)) {
               $where = " WHERE A.kd_produk='".$where."'";
           }
           $query = "SELECT A.*,  A.date_add as date_add_product, A.date_edit as date_edit_product,
                     B.*, B.nm_kategori as nama_kategori FROM produk A
                     LEFT JOIN kategori B 
                     ON A.kd_kategori = B.kd_kategori
                     ";
           $query .= $where;
           $query .= " ORDER BY date_add_product DESC";
       } else if($params == 'relasi_pengiriman') {
           if(!empty($where)) {
               $where = " WHERE A.kd_sj='".$where."'";
           }
           $query = "SELECT A.*, B.*, B.kd_pesanan as pesanan_code,C.*, D.*, E.*,G.* FROM surat_jalan A 
                   LEFT JOIN pesanan B 
                   ON A.kd_pesanan = B.kd_pesanan
                   LEFT JOIN pelanggan C
                    ON B.kd_plg = C.id_plg
                    LEFT JOIN detil_pesanan D
                    ON B.kd_pesanan = D.kd_pesanan
                    LEFT JOIN kurir E
                    ON A.kurir_id = E.id_kurir
                    LEFT JOIN ongkir G
                    ON B.ongkir_id = G.id_ongkir
                   ";
            $query .= $where;
            $query .= " GROUP BY A.kd_pesanan";
           $query .= " ORDER BY sj_date_add DESC";
       } else if($params == 'relasi_pengiriman_detail') {
            if(!empty($where)) {
                $where = " WHERE A.kd_sj='".$where."'";
            } else if(!empty($between1) && !empty($between2))  {
                $where = " WHERE A.tgl_sj between DATE('".$between1."') AND DATE('".$between2."')";
            } else {
                $where = " ";
            }
            // if(!empty($where)) {
            //     $where = " WHERE A.kd_sj='".$where."'";
            // }
            $query = "SELECT *, A.kd_pesanan AS kd_pesanan_detail FROM surat_jalan A 
            LEFT JOIN PESANAN B 
            ON A.kd_pesanan = B.kd_pesanan
            LEFT JOIN detil_pesanan C
            ON B.kd_pesanan =  C.kd_pesanan 
            LEFT JOIN pelanggan D 
            ON B.kd_plg = D.id_plg
            LEFT JOIN produk E
            ON C.kd_produk = E.kd_produk
            LEFT JOIN kurir F 
            ON A.kurir_id = F.id_kurir
                    ";
            $query .= $where;
            
            $query .= " ORDER BY tgl_sj DESC";
        }  else if ($params == 'relasi_setting_banner') {
           if(!empty($where)) {
               $where = " WHERE A.id_banner='".$where."'";
           }
           $query = "SELECT A.*, B.* FROM settings_banner A
                     LEFT JOIN staff B
                     ON A.id_staff = B.id_staff
                   ";
            $query .= $where;
           $query .= " ORDER BY id_banner ASC";
       } else if ($params == 'relasi_setting_web_footer') {
           if(!empty($where)) {
               $where = " WHERE A.id_settings_wf='".$where."'";
           } 
           
           $query = "SELECT A.*, A.date_edit as date_edit_settings_wf, B.*
                    FROM settings_web_footer A
                     LEFT JOIN staff B
                     ON A.id_staff = B.id_staff
                   ";
            $query .= $where;
           $query .= " ORDER BY id_settings_wf DESC";
       } else if ($params == 'report_sales') {
           $query = "SELECT COUNT(*) as tot, A.tgl_pesanan FROM pesanan A WHERE status_pesanan=3";
           $query .= " GROUP BY MONTH(A.tgl_pesanan) ORDER BY MONTH(A.tgl_pesanan) ASC";
       } else if ($params == 'report_sales_between') {
           $query = "SELECT COUNT(*) as tot, A.purchase_date FROM purchase A WHERE A.purchase_status=3 && A.purchase_date BETWEEN DATE('".$between1."') AND DATE('".$between2."')";
           $query .= " GROUP BY A.purchase_date ORDER BY MONTH(A.purchase_date) ASC";
       } else if($params == 'user_confirmation_payment') {
           $query = "SELECT no_order, confirmation_purpose_bank, confirmation_customer_bank,
                      confirmation_a_n, confirmation_method_transfer, confirmation_ammount, 
                      confirmation_tgl_transfer, confirmation_payment_evidence, purchase_status
                      FROM purchase
                      ORDER BY confirmation_tgl_transfer DESC
                     ";
       } else if($params == 'relasi_laporan_pesanan') {
            if(!empty($where)) {
                $where = " WHERE status_pesanan=4";
            } else if(!empty($between1) && !empty($between2))  {
                $where = " WHERE status_pesanan=4 AND A.tgl_pesanan between DATE('".$between1."') AND DATE('".$between2."')";
            } else {
                 $where = " WHERE status_pesanan=4";
            }

            $query = "SELECT * FROM pesanan A 
                      LEFT JOIN pelanggan B 
                      ON A.kd_plg = B.id_plg
                     ";
            $query .= $where;
            $query .= " ORDER BY tgl_pesanan DESC";

       } else if($params == 'relasi_laporan_detail_pesanan') {
            if(!empty($where)) {
                $where = " WHERE status_pesanan=4";
            } else if(!empty($between1) && !empty($between2))  {
                $where = " WHERE status_pesanan=4 AND B.tgl_pesanan between DATE('".$between1."') AND DATE('".$between2."')";
            } else {
                 $where = " WHERE status_pesanan=4";
            }

            $query = "SELECT *, A.kd_pesanan AS kd_pesanan_detail FROM detil_pesanan A 
                      LEFT JOIN pesanan B 
                      ON A.kd_pesanan = B.kd_pesanan
                      LEFT JOIN pelanggan C
                      ON B.kd_plg = C.id_plg
                      LEFT JOIN produk D
                      ON A.kd_produk = D.kd_produk
                      LEFT JOIN surat_jalan E
                      ON E.kd_pesanan = E.kd_pesanan
                      LEFT JOIN kurir F
                      ON E.kurir_id = F.id_kurir
                     ";
            $query .= $where;
            // $query .= " GROUP BY B.kd_pesanan";
            $query .= " ORDER BY B.tgl_pesanan DESC";

       } else if($params == 'relasi_laporan_penjualan') {
            $query = "SELECT * FROM pesanan A 
                        LEFT JOIN pelanggan B ON A.kd_plg = B.id_plg 
                        LEFT JOIN detil_pesanan C ON A.kd_pesanan = C.kd_pesanan 
                        LEFT JOIN produk D ON C.kd_produk = D.kd_produk
                        WHERE status_pesanan=4
                        
                     ";
            $query .= "  ORDER BY tgl_pesanan DESC";
       } else if($params == 'relasi_laporan_pembayaran') {
            $query = "SELECT * FROM pembayaran A 
                        LEFT JOIN pesanan B ON A.kd_pesanan = B.kd_pesanan 
                        LEFT JOIN pelanggan C ON B.kd_plg = C.id_plg
                        WHERE status_pembayaran=1
                     ";
            $query .= "  ORDER BY tgl_pembayaran DESC";
       } else if($params == 'relasi_cetak_laporan_penjualan') {
            if(!empty($between1) && !empty($between2))  {
            $where .= "  WHERE status_pesanan=4 && A.tgl_pesanan between DATE('".$between1."') AND DATE('".$between2."')";
            } else {
                $where .= " WHERE status_pesanan=4";
            }    
            $query = "SELECT * FROM pesanan A 
                        LEFT JOIN pelanggan B ON A.kd_plg = B.id_plg 
                        LEFT JOIN detil_pesanan C ON A.kd_pesanan = C.kd_pesanan 
                        LEFT JOIN produk D ON C.kd_produk = D.kd_produk
                       
                        
                    ";
            $query .= $where;
            $query .= "  ORDER BY tgl_pesanan DESC";
        }  else if($params == 'relasi_laporan_produk_terlaris') {
             if(!empty($between1) && !empty($between2))  {
            $where .= "  WHERE status_pesanan=4 && A.tgl_pesanan between DATE('".$between1."') AND DATE('".$between2."')";
            } else {
                $where .= " WHERE status_pesanan=4";
            }    

            $query = "SELECT C.kd_produk, nm_produk, SUM(qty) AS total_jual FROM pesanan A 
                        LEFT JOIN detil_pesanan B 
                        ON  A.kd_pesanan = B.kd_pesanan
                        LEFT JOIN produk C  
                        ON B.kd_produk = C.kd_produk
                     ";
            $query .= $where;
            $query .= " GROUP BY kd_produk";
            $query .= " ORDER BY qty DESC";

       } else if ($params == 'relasi_cetak_suratjalan') {
            if(!empty($where)) {
                $where = " WHERE B.kd_pesanan='".$where."' ";
            }
            $query = "SELECT B.kd_pesanan as pesanan_code, A.*, B.*, C.*, D.* FROM pelanggan A
                    LEFT JOIN pesanan B
                    ON A.id_plg = B.kd_plg
                    LEFT JOIN detil_pesanan C
                    ON B.kd_pesanan = C.kd_pesanan
                    LEFT JOIN produk D
                    ON C.kd_produk = D.kd_produk
                    ";
            $query .= $where;
            $query .= " ORDER BY B.tgl_pesanan DESC "; 
       } else if($params == 'relasi_pembayaran') {
            if(!empty($between1) && !empty($between2))  {
                $where = "  WHERE status_pembayaran=1 AND A.tgl_pembayaran between DATE('".$between1."') AND DATE('".$between2."')";
            } else {
                $where = null;
            }    
            $query = "SELECT * FROM pembayaran A 
                        LEFT JOIN pesanan B ON A.kd_pesanan = B.kd_pesanan 
                        LEFT JOIN pelanggan C ON B.kd_plg = C.id_plg";
            $query .= $where;
            $query .= "  ORDER BY A.tgl_pembayaran DESC";
       } else if($params == 'relasi_retur') {
        if(!empty($between1) && !empty($between2))  {
            $where .= "  WHERE A.tgl_retur between DATE('".$between1."') AND DATE('".$between2."')";
        } else {
            $where .= null;
        }   

        $query = "SELECT * FROM retur A
                    LEFT JOIN pesanan B
                    ON A.kd_pesanan = B.kd_pesanan
                    LEFT JOIN pelanggan C
                    ON B.kd_plg = C.id_plg
                    LEFT JOIN detil_retur D 
                    ON A.kd_retur = D.kd_retur
                    ";
        $query .= $where;
        $query .= "  ORDER BY A.tgl_retur DESC";
       
        } else if($params == 'cetak_retur') {
            if(!empty($where)) {
                $where = " WHERE A.kd_retur='".$where."' ";
            }
            $query = "SELECT * FROM retur A 
                    LEFT JOIN pesanan B 
                    ON a.kd_pesanan = B.kd_pesanan
                    LEFT JOIN pelanggan C 
                    ON B.kd_plg = C.id_plg
                    LEFT JOIN detil_pesanan D ON B.kd_pesanan = D.kd_pesanan
                    LEFT JOIN produk E ON D.kd_produk = E.kd_produk
                     ";
             $query .= $where;
            $query .= " GROUP BY A.kd_retur ORDER BY A.tgl_retur DESC"; 
        } else if($params == 'cetak_detil_retur') {
            if(!empty($where)) {
                $where = " WHERE A.kd_retur='".$where."' ";
            }
            $query = "SELECT * FROM detil_retur A
                     ";
             $query .= $where;
            $query .= " ORDER BY A.nama_produk DESC"; 
        } 
       
       $sql = $this->db->query($query);
       $query = $sql->result_array();
       return $query;         
    }


    public function kodeproduk(){
        $this->db->select('RIGHT(produk.kd_produk,2) as kode_produk', FALSE);
        $this->db->order_by('kode_produk','DESC');    
        $this->db->limit(1);    
        $query = $this->db->get('produk');  //cek dulu apakah ada sudah ada kode di tabel.    
        if($query->num_rows() <> 0){      
            //cek kode jika telah tersedia    
            $data = $query->row();      
            $kode = intval($data->kode_produk) + 1; 
        }
        else{      
            $kode = 1;  //cek jika kode belum terdapat pada table
        }
            $random=create_random(4); 
            $batas = str_pad($kode, 4, "0", STR_PAD_LEFT);    
            $kodetampil = "P"."AL-".$random."-".$batas;  //format kode
            return $kodetampil;  
    }

    public function kodesuratjalan(){
        $this->db->select('RIGHT(surat_jalan.kd_sj,2) as kode_sj', FALSE);
        $this->db->order_by('kd_sj','DESC');    
        $this->db->limit(1);    
        $query = $this->db->get('surat_jalan');  //cek dulu apakah ada sudah ada kode di tabel.    
        if($query->num_rows() <> 0){      
            //cek kode jika telah tersedia    
            $data = $query->row();      
            $kode = intval($data->kode_sj) + 1; 
        }
        else{      
            $kode = 1;  //cek jika kode belum terdapat pada table
        }
            $random=create_random(4); 
            $batas = str_pad($kode, 4, "0", STR_PAD_LEFT);    
            $kodetampil = "SJ"."AL-".$random."-".$batas;  //format kode
            return $kodetampil;  
    }
    
    function getdata($table = "", $where = "", $order = "", $limit = "") {
        if ($order != "") {
            $this->db->order_by($order[0], $order[1]);
        }

        if ($limit != "") {
            $this->db->limit($limit[1], $limit[0]);
        }

        if ($where == "") {
            $query = $this->db->get($table);
        } else {
            $query = $this->db->get_where($table, $where);
        }

        $result = $query->result_array();
        if ($order == "") {
            if (count($result) > 0) {
                $result = $result[0];
            } else {
                $result = null;
            }
        }
        return $result;
    }

    function countdata($table="", $where="") {
        if ($where != "") {
            $this->db->where($where);
        }
        $count = $this->db->from($table)->count_all_results();
        return $count;
    }

    function updatedata($table = "", $data = "", $where = "") {
        if ($where != "") {
            $this->db->where($where);
        }
        return $this->db->update($table, $data);
    }

    function insertdata($table = "", $data = "") {
        return $this->db->insert($table, $data);
    }

    function removedata($table = "", $where = "") {
        return $this->db->delete($table, $where);
    }
    
}

/* End of file base.php */
/* Location: ./application/models/core_admin.php */