<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Data Laporan Pesanan
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?=base_url()?>backend"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li class="active">Data Laporan Pesanan</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Data Laporan Pesanan</h3><br/><br/>
                    <div class="pull-right">
                    <form class="form-inline" method="post" action="<?= base_url() ?>excel/getexcel/laporan_penjualan" autocomplete="off">
                        <div class="form-group">
                            <label for="exampleInputName2">Dari Tanggal</label>
                            <input type="text" name="from_date" require class="form-control datepicker"  placeholder="Dari Tanggal">
                        </div> &nbsp;&nbsp;&nbsp;
                        <div class="form-group">
                            <label for="exampleInputEmail2">Sampai Tanggal</label>
                            <input type="text" name="to_date" require class="form-control datepicker"  placeholder="Sampai Tanggal">
                        </div> &nbsp;&nbsp;&nbsp;
                    <button type="submit" class="btn btn-success pull-right">EXPORT LAPORAN</button> 
                    </form>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                     <table id="example1" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th class="text-center">No. Pesanan</th>
                                <th class="text-center">Nama Pelanggan</th>
                                <th class="text-center">Tanggal Pesan</th>
                                <th class="text-center">Nama Barang</th>
                                <th class="text-center">Jumlah Beli</th>
                                <th class="text-center">Harga Satuan</th>
                                <th class="text-center">Total</th>
                                <th class="text-center">Status Pesanan</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            $no = 1;
                            $total_jual = 0;
                            $grand_total = 0;
                                foreach($show_data as $val) {
                                    $total_jual = $val['qty'] * $val['harga_jual'];
                                   
                                    $grand_total += $total_jual;
                            ?>
                            <tr>
                                <td>
                                    <?=$no;?>
                                </td>
                                <td style="text-align:center;">                        
                                    <a href="<?=base_url()?>backend/edit/user_order/<?=$val['kd_pesanan']?>"><?=$val['kd_pesanan']?></a>
                                </td>
                                <td class="text-center">
                                    <?=$val['nm_plg']?>
                                </td>
                                <td class="text-center">
                                    <?=formatDateMonthNumerik($val['tgl_pesanan'])?>
                                </td>
                                <td class="text-center">
                                    <?=$val['nm_produk']?>
                                </td>
                                <td class="text-center">
                                    <?=$val['qty']?>
                                </td>
                                <td class="text-center">
                                    <?=$val['harga_jual']?>
                                </td>
                                <td class="text-center">
                                    <?=convert_to_rupiah($total_jual)?>
                                </td>
                                <td class="text-center">
                                <?php
                                         if($val['status_pesanan'] == 0) {
                                             $status = 'alert-danger';
                                         } else if($val['status_pesanan'] == 1) {
                                             $status = 'alert-warning';
                                         } else if($val['status_pesanan'] == 2) {
                                             $status = 'alert-info';
                                         } else if($val['status_pesanan'] == 3) {
                                            $status = 'alert-info'; 
                                         } else if($val['status_pesanan'] == 4) {
                                            $status = 'alert-success'; 
                                         } else if($val['status_pesanan'] == 5) {
                                            $status = 'alert-danger'; 
                                         }
                                    ?>
                                     <div style="border-radius:2px;" class="<?=$status?>">
                                    <?=status_pesanan($val['status_pesanan'])?>
                                    </div>
                                </td>
                                
                            </tr>
                            <?php $no++; } ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td class="text-right" colspan="6">
                                    <b>Grand Total</b>
                                </td>
                                <td class="text-center">
                                Rp. 
                                <?=rupiah($grand_total);?>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
