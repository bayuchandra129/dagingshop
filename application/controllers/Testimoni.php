<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');
Class Testimoni extends CI_Controller {
    public function __construct() {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
    }
    
    public function index($params = null) {
        $data['show_portofolio'] = $this->core_base->getrelasi('relasi_testimonial');
        $data['show_portofolio_product'] = $this->core_base->getrelasi('relasi_testimonial', 1);
        $data['show_portofolio_package_service'] = $this->core_base->getrelasi('relasi_testimonial', 2);
        $data['product_new'] = $this->core_base->getdata('product', null, Array('date_add','DESC'), Array(0, 5));
         $data['show_contact'] = $this->core_base->getdata('settings_contact', Array('id_settings_contact' => 1));
        $data['params'] = $params;
        $data['menu'] = 'menu';
        $data['module'] = 'testimony';
        $this->load->view('template', $data);
    }
    
    public function proccessTestimoni() {
        $getsession_logged = $this->session->userdata('logged_in');
        $id_user = $this->session->userdata('id');
        $testimoni = $this->input->post('testimoni');
        $id_product = $this->input->post('id_product');
        $id_cproduct = $this->input->post('id_cproduct');
        
        if(empty($getsession_logged)) {
            $status = 'notlogin';
            $msg = 'Harap, Login terlebih dahulu untuk melakukan testimoni';
        } else {
            $data = Array(
                'id_user' => $id_user,
                'id_product' => $id_product,
                'id_c_testimonial' => 1,
                'testimonial_message' => $testimoni,
                'testimonial_status' => 0,
                'date_add' => date('Y-m-d H:i:s')
            );
            
            $insert = $this->core_base->insertdata('testimonial', $data);
            if($insert) {
                $status = 'success';
                $msg = 'Terimakasih, sudah memberikan testimonial terhadap produk kami :)';
            }
        }
        
        echo json_encode(Array('status' => $status, 'msg' => $msg));
    }
}