<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * RajaOngkir API Key
 * Silakan daftar akun di RajaOngkir.com untuk mendapatkan API Key
 * http://rajaongkir.com/akun/daftar
 */
$config['rajaongkir_api_key'] = "6429a8f2026999b8b7d27ddd36ecbe00";

/**
 * RajaOngkir account type: starter, basic or pro
 * http://rajaongkir.com/dokumentasi#akun-ringkasan
 * 
 */
$config['rajaongkir_account_type'] = "pro";
