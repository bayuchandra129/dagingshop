<section>
    <div class="content-cart">
        <div id="content">

            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h3 class="title">MEMBER AREA<i class="fa fa-caret-right"></i><span>PESANAN BELANJA</span></h3>
                        <div class="welcome">
                            <span class="hello">Halo, <?= $this->session->userdata('fullname'); ?><i></i></span>
                            <span class="userid"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!-- left form -->
                    <div class="col-xs-2 col-sm-2 col-md-3 ">
                        <?php require_once($sidebar_dashboard . '.php'); ?>
                    </div>
                    <div class="col-xs-10  col-sm-10 col-md-9 content-wrap">
                        <!-- Profil -->
                        <div class="profil">
                            
                            <h3 class="profil-title">RETUR PRODUK</h3><br/>
                             <div class="content-error">
                               <?php 
                                    if(@$_GET['info'] == 'not_valid_noorder') {
                               ?>
                                 <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <strong>Warning!</strong> No Pesanan yang anda masukan tidak valid.
                                </div>
                                <?php } else if(@$_GET['info'] == 'success') { ?>
                                    <div class="alert alert-success alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <strong>Success!</strong> Terimakasih sudah konfirmasi, informasi retur anda kami segara proses!.
                                    </div>
                                <?php } else if(empty(@$_GET['info']) ||  @$_GET['info'] != null) {   ?>

                                    <?php } ?>
                            </div>   
                            <div class="profil-wrap">
                            <form action="<?=base_url()?>dashboard/proccessRetur" role="form" class="contact-form add_name" enctype="multipart/form-data" id="contact-form" method="post" autocomplete="off">
                                <div class="form-group">
                                    <div class="controls">
                                        <input type="text" placeholder="Kode Pesanan" id="kode_pesanan" class="kode_pesanan" name="kode_pesanan">
                                    </div>
                                </div>
                                <div class="table-responsive">  
                                    <table class="table table-bordered" id="dynamic_field">  
                                        <tr>  
                                            <th class="text-center">Produk</th>  
                                            <th class="text-center">Jumlah</th>
                                            <!-- <th class="text-center">Harga Satuan</th>
                                            <th class="text-center">Diskon</th> -->
                                            <th class="text-center">Keterangan</th>
                                            <th class="text-center">Opsi More</th>  
                                        </tr>  
                                        <tr>  
                                            <td>
                                            <input type="text" name="product[]" placeholder="Nama Produk"  class="form-control name_list" required="" /> 
                                            </td>  
                                            <td><input type="text" name="quantity[]" maxlength="3" placeholder="Jumlah" onkeypress="return isNumber(event);" class="form-control name_list numbersOnly" required="" /></td>  
                                            <td>
                                            <input type="text" name="keterangan[]" placeholder="Keterangan Retur" class="form-control name_list" required="" /> 
                                            </td>
                                            <!-- <td></td>  
                                            <td></td>  
                                            <td></td>   -->
                                            <td><center><button type="button" name="add" id="add" class="btn btn-success">Add More</button></center></td>  
                                        </tr>  
                                    </table>  
                                </div>
                                <div class="form-group">
                                    <div class="controls">
                                    <label><b>Foto Retur (Only Image)</b></label><br>
                                        <input type="file" class="requiredField foto_retur" id="foto_bukti" name="foto_bukti">
                                    </div>
                                </div>
                                <button type="submit" onClick="return confirm('Apakah data retur Anda sudah benar?')" style="float: right" id="submit" class="btn-system btn-large pull-right form-returids">SEND</button>
                            </form>
                            </div> <br/><br/>

                        </div>						
                        </div>
                </div>

            </div>
        </div>
    </div>    
</section>

